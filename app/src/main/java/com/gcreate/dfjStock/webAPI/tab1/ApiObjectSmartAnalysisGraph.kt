package com.gcreate.dfjStock.webAPI.tab1

class ApiObjectSmartAnalysisGraph {
    /**
     * kind : stock#MetadataDate
     * items : [{"metaValue":"https://dfj.com.tw/stock_analysis/stock_trend_state/2020-12-23",
     * "createDate":"2020-12-23","metaKey":"path_stock_trend_state"}]
     */
    var kind: String? = null
    var items: List<ItemsBean>? = null

    class ItemsBean {
        /**
         * metaValue : https://dfj.com.tw/stock_analysis/stock_trend_state/2020-12-23
         * createDate : 2020-12-23
         * metaKey : path_stock_trend_state
         */
        var metaValue: String? = null
        var createDate: String? = null
        var metaKey: String? = null
        var stockId = 0
    }

    /**
     * success : {"code":200,
     * "message":"stock not found."}
     */
    var success: SuccessBean? = null

    class SuccessBean {
        /**
         * code : 200
         * message : stock not found.
         */
        var code = 0
        var message: String? = null
    }
}