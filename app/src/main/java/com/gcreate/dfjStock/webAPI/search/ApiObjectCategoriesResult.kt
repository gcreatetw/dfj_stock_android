package com.gcreate.dfjStock.webAPI.search

import com.gcreate.dfjStock.webAPI.all.States


data class ApiObjectCategoriesResult(
    val `data`: MutableList<CategoryData>,
    val message: String,
    val status: String,
)

data class CategoryData(
    val description: String,
    val id: Int,
    val price: Double,
    val priceChange: Double,
    val priceChangePercent: Double,
    val states: States,
    val stockExchangeCode: String,
    val symbol: String,
)

