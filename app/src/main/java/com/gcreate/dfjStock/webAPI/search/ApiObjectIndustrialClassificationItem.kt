package com.gcreate.dfjStock.webAPI.search

class ApiObjectIndustrialClassificationItem {
    /**
     * kind : stock#IndustrialClassificationItems
     * items : [{"description":"水泥工業","id":1,"marketId":3}]
     */
    var kind: String? = null
    var items: List<ItemsBean>? = null

    class ItemsBean {
        /**
         * description : 水泥工業
         * id : 1
         * marketId : 3
         */
        var description: String? = null
        var id = 0
        var marketId = 0
    }

    /**
     * success : {"code":200,
     * "message":"stock not found."}
     */
    var success: SuccessBean? = null

    class SuccessBean {
        /**
         * code : 200
         * message : stock not found.
         */
        var code = 0
        var message: String? = null
    }
}