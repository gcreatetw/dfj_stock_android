package com.gcreate.dfjStock.webAPI.tab1

data class ApiObjectStockStateImage(
    val `data`: StockStateImageData,
    val message: String,
    val status: String,
)

data class StockStateImageData(
    val id: Int,
    val url: String,
)