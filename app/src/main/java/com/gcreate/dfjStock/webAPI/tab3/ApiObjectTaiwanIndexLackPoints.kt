package com.gcreate.dfjStock.webAPI.tab3

data class ApiObjectTaiwanIndexLackPoints(
    val `data`: MutableList<TaiwanIndexLackPointsData>,
    val message: Any,
    val status: String,
)

data class TaiwanIndexLackPointsData(
    val items: MutableList<TaiwanIndexLackPointsDataItem>,
    val name: String,
)

data class TaiwanIndexLackPointsDataItem(
    val index: Int,
    val kDatetime: String,
    val price: Double,
)