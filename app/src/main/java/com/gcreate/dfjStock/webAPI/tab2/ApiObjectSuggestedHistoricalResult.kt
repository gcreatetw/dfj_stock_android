package com.gcreate.dfjStock.webAPI.tab2


import com.gcreate.dfjStock.webAPI.all.States

data class ApiObjectSuggestedHistoricalResult(
    val `data`: MutableList<SuggestedHistoricalResultData>,
    val message: String,
    val status: String,
)

data class SuggestedHistoricalResultData(
    val date: String,
    val listInfos: MutableList<Infos>,
)

data class Infos(
    val description: String,
    val id: Int,
    val price: Double,
    val priceChange: Double,
    val priceChangePercent: Double,
    val states: States,
    val stockExchangeCode: String,
    val symbol: String,
)

