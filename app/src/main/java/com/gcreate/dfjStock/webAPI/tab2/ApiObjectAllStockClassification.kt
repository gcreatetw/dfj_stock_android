package com.gcreate.dfjStock.webAPI.tab2

import java.io.Serializable

class ApiObjectAllStockClassification : Serializable {
    /**
     * kind : stock#StockClassification
     * items : [
     * {"id":1,"code":"tpe","description":"台灣證券交易所",
     * "securitiesClassificationItems":[{
     * "id":1,"description":"指數",
     * "marketItems":[{"id":1,"description":"上市","industrialClassificationItems":[]},{"id":2,"description":"上櫃","industrialClassificationItems":[]}]},{"id":2,"description":"股票","marketItems":[{"id":3,"description":"上市","industrialClassificationItems":[{"id":1,"description":"水泥工業"},{"id":2,"description":"食品工業"},{"id":3,"description":"塑膠工業"},{"id":4,"description":"紡織纖維"},{"id":5,"description":"電機機械"},{"id":6,"description":"電器電纜"},{"id":7,"description":"玻璃陶瓷"},{"id":8,"description":"造紙工業"},{"id":9,"description":"鋼鐵工業"},{"id":10,"description":"橡膠工業"},{"id":11,"description":"汽車工業"},{"id":12,"description":"電子工業"},{"id":13,"description":"建材工業"},{"id":14,"description":"航運工業"},{"id":15,"description":"觀光事業"},{"id":16,"description":"金融保險"},{"id":17,"description":"貿易百貨"},{"id":18,"description":"其他"},{"id":19,"description":"化學工業"},{"id":20,"description":"生技醫療"},{"id":21,"description":"油電燃氣"},{"id":22,"description":"半導體業"},{"id":23,"description":"電腦週邊"},{"id":24,"description":"光電"},{"id":25,"description":"通信網路"},{"id":26,"description":"電子零件"},{"id":27,"description":"電子通路"},{"id":28,"description":"資訊服務"},{"id":29,"description":"其它電子"}]},{"id":4,"description":"上櫃","industrialClassificationItems":[{"id":30,"description":"食品工業"},{"id":31,"description":"塑膠工業"},{"id":32,"description":"紡織纖維"},{"id":33,"description":"電機機械"},{"id":34,"description":"電器電纜"},{"id":35,"description":"玻璃陶瓷"},{"id":36,"description":"鋼鐵工業"},{"id":37,"description":"橡膠工業"},{"id":38,"description":"電子工業"},{"id":39,"description":"建材工業"},{"id":40,"description":"航運工業"},{"id":41,"description":"觀光事業"},{"id":42,"description":"金融保險"},{"id":43,"description":"貿易百貨"},{"id":44,"description":"其他"},{"id":45,"description":"化學工業"},{"id":46,"description":"生技醫療"},{"id":47,"description":"油電燃氣"},{"id":48,"description":"半導體業"},{"id":49,"description":"電腦週邊"},{"id":50,"description":"光電"},{"id":51,"description":"通信網路"},{"id":52,"description":"電子零件"},{"id":53,"description":"電子通路"},{"id":54,"description":"資訊服務"},{"id":55,"description":"其它電子"},{"id":56,"description":"文創產業"}]}]},{"id":3,"description":"ETF","marketItems":[{"id":5,"description":"上市","industrialClassificationItems":[]}]},{"id":4,"description":"國際金融","marketItems":[]}]}]
     */
    var kind: String? = null
    var items: MutableList<ItemsBean>? = null

    class ItemsBean {
        /**
         * id : 1
         * code : tpe
         * description : 台灣證券交易所
         * securitiesClassificationItems : [{"id":1,"description":"指數","marketItems":[{"id":1,"description":"上市","industrialClassificationItems":[]},{"id":2,"description":"上櫃","industrialClassificationItems":[]}]},{"id":2,"description":"股票","marketItems":[{"id":3,"description":"上市","industrialClassificationItems":[{"id":1,"description":"水泥工業"},{"id":2,"description":"食品工業"},{"id":3,"description":"塑膠工業"},{"id":4,"description":"紡織纖維"},{"id":5,"description":"電機機械"},{"id":6,"description":"電器電纜"},{"id":7,"description":"玻璃陶瓷"},{"id":8,"description":"造紙工業"},{"id":9,"description":"鋼鐵工業"},{"id":10,"description":"橡膠工業"},{"id":11,"description":"汽車工業"},{"id":12,"description":"電子工業"},{"id":13,"description":"建材工業"},{"id":14,"description":"航運工業"},{"id":15,"description":"觀光事業"},{"id":16,"description":"金融保險"},{"id":17,"description":"貿易百貨"},{"id":18,"description":"其他"},{"id":19,"description":"化學工業"},{"id":20,"description":"生技醫療"},{"id":21,"description":"油電燃氣"},{"id":22,"description":"半導體業"},{"id":23,"description":"電腦週邊"},{"id":24,"description":"光電"},{"id":25,"description":"通信網路"},{"id":26,"description":"電子零件"},{"id":27,"description":"電子通路"},{"id":28,"description":"資訊服務"},{"id":29,"description":"其它電子"}]},{"id":4,"description":"上櫃","industrialClassificationItems":[{"id":30,"description":"食品工業"},{"id":31,"description":"塑膠工業"},{"id":32,"description":"紡織纖維"},{"id":33,"description":"電機機械"},{"id":34,"description":"電器電纜"},{"id":35,"description":"玻璃陶瓷"},{"id":36,"description":"鋼鐵工業"},{"id":37,"description":"橡膠工業"},{"id":38,"description":"電子工業"},{"id":39,"description":"建材工業"},{"id":40,"description":"航運工業"},{"id":41,"description":"觀光事業"},{"id":42,"description":"金融保險"},{"id":43,"description":"貿易百貨"},{"id":44,"description":"其他"},{"id":45,"description":"化學工業"},{"id":46,"description":"生技醫療"},{"id":47,"description":"油電燃氣"},{"id":48,"description":"半導體業"},{"id":49,"description":"電腦週邊"},{"id":50,"description":"光電"},{"id":51,"description":"通信網路"},{"id":52,"description":"電子零件"},{"id":53,"description":"電子通路"},{"id":54,"description":"資訊服務"},{"id":55,"description":"其它電子"},{"id":56,"description":"文創產業"}]}]},{"id":3,"description":"ETF","marketItems":[{"id":5,"description":"上市","industrialClassificationItems":[]}]},{"id":4,"description":"國際金融","marketItems":[]}]
         */
        var id = 0
        var code: String? = null
        var description: String? = null
        var securitiesClassificationItems: MutableList<SecuritiesClassificationItemsBean>? = null

        class SecuritiesClassificationItemsBean {
            /**
             * id : 1
             * description : 指數
             * marketItems : [{"id":1,"description":"上市","industrialClassificationItems":[]},{"id":2,"description":"上櫃","industrialClassificationItems":[]}]
             */
            var id = 0
            var description: String? = null
            var marketItems: MutableList<MarketItemsBean>? = null

            class MarketItemsBean {
                /**
                 * id : 1
                 * description : 上市
                 * industrialClassificationItems : []
                 */
                var id = 0
                var description: String? = null
                var industrialClassificationItems: MutableList<IndustrialClassificationItemsBeans>? = null

                class IndustrialClassificationItemsBeans {
                    /**
                     * id : 1
                     * description : 上市
                     * industrialClassificationItems : []
                     */
                    var id = 0
                    var description: String? = null
                }
            }
        }
    }
}