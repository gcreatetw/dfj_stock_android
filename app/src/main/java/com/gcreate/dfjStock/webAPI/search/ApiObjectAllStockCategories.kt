package com.gcreate.dfjStock.webAPI.search


data class ApiObjectAllStockCategories(
    val `data`: MutableList<AllStockCategoriesData>,
    val message: String,
    val status: String,
)

data class AllStockCategoriesData(
    val code: String,
    val description: String,
    val id: Int,
    val market_categories: MutableList<MarketCategory>,
)

data class MarketCategory(
    val description: String,
    val id: Int,
    val industry_categories: MutableList<IndustryCategory>,
)

data class IndustryCategory(
    val description: String,
    val id: Int,
)