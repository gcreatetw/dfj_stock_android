package com.gcreate.dfjStock.webAPI.tab1

data class ApiObjectSmartAnalysisTrendLine(
    val `data`: TrendlinesData,
    val message: String,
    val status: String,
)

data class TrendlinesData(
    val day1: MutableList<Day1>,
    val day20: MutableList<Day20>,
    val day5: MutableList<Day5>,
    val min30: MutableList<Min30>,
)

data class Day1(
    val items: MutableList<Item>,
    val name: String,
)

data class Day20(
    val items: MutableList<Item>,
    val name: String,
)

data class Day5(
    val items: MutableList<Item>,
    val name: String,
)

data class Min30(
    val items: MutableList<Item>,
    val name: String,
)

data class Item(
    val a: Double,
    val b: Double,
)
