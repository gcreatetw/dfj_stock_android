package com.gcreate.dfjStock.webAPI.tab2

import com.gcreate.dfjStock.webAPI.all.States


data class ApiObjectSuggestedConditionResult(
    val `data`: MutableList<SuggestedConditionResultData>,
    val message: String,
    val status: String
)

data class SuggestedConditionResultData(
    val description: String,
    val id: Int,
    val price: Double,
    val priceChange: Double,
    val priceChangePercent: Double,
    val states: States,
    val stockExchangeCode: String,
    val symbol: String
)

