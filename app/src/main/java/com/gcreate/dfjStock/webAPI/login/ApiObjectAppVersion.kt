package com.gcreate.dfjStock.webAPI.login

data class ApiObjectAppVersion(
    val Android_version: List<String>,
    val IOS_version: List<String>
)