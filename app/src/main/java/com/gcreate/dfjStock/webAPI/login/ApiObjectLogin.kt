package com.gcreate.dfjStock.webAPI.login

data class ApiObjectLogin(
    val `data`: LoginData,
    val message: String,
    val success: Boolean,
    val user: User
)

data class LoginData(
    val email: String,
    val is_vip: Boolean,
    val name: String,
    val phone: String,
    val role_name: String,
    val stock_group: MutableList<StockGroup>,
    val user_id: Int,
    val vip_date: String,
)

data class StockGroup(
    val contain: MutableList<Int>,
    val group_id: Int,
    var group_name: String,
)

data class User(
    val error_data: MutableList<String>,
    val errors: Errors
)

data class Errors(
    val invalid_username: MutableList<String>
)