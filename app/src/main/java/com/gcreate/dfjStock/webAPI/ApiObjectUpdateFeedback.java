package com.gcreate.dfjStock.webAPI;

public class ApiObjectUpdateFeedback {

    /**
     * success : true
     * message : Update successed
     */

    private boolean success;
    private String message;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
