package com.gcreate.dfjStock.webAPI;

import androidx.annotation.NonNull;

import com.gcreate.dfjStock.webAPI.search.ApiObjectAllStockCategories;
import com.gcreate.dfjStock.webAPI.search.ApiObjectCategoriesResult;
import com.gcreate.dfjStock.webAPI.search.ApiObjectSearchResult;
import com.gcreate.dfjStock.webAPI.tab1.ApiObjectSmartAnalysisTrendLine;
import com.gcreate.dfjStock.webAPI.tab1.ApiObjectSmartAnalysisTrendLine0;
import com.gcreate.dfjStock.webAPI.tab1.ApiObjectStockAnalysisText;
import com.gcreate.dfjStock.webAPI.tab1.ApiObjectStockDFJTrendLine;
import com.gcreate.dfjStock.webAPI.tab1.ApiObjectStockHistoricalData;
import com.gcreate.dfjStock.webAPI.tab1.ApiObjectStockStateImage;
import com.gcreate.dfjStock.webAPI.tab2.ApiObjectSuggestedConditionItems;
import com.gcreate.dfjStock.webAPI.tab2.ApiObjectSuggestedConditionResult;
import com.gcreate.dfjStock.webAPI.tab2.ApiObjectSuggestedHistoricalResult;
import com.gcreate.dfjStock.webAPI.tab3.ApiObjectTaiwanIndexLackPoints;
import com.gcreate.dfjStock.webAPI.tab3.ApiObjectTaiwanIndexText;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface dfjAPI {

    String PATH = "dfj.com.tw:30443/stock-analysis";
    String DFJ_SERVER = "https://" + PATH + "/api/";


    // 智慧圖形 smart-analysis-trendLines
    @Headers({"Content-Type: application/json"})
    @GET("stock-trendline/smart-analysis-trendlines/")
    Call<ApiObjectSmartAnalysisTrendLine0> GetSmartAnalysisTrendLine(@Query("stockId") int stockId, @Query("lineId") int lineId);

    //------------------------------ NEW API--------------------------------------------------------------------------------------

    /**
     * check
     * 取得某最愛股票群組的股票 list
     */
    @Headers({"Content-Type: application/json;charset=utf-8"})
    @GET("stock-app/categories/result/")
    Call<ApiObjectCategoriesResult> getOneFavGroupStockItems(@Query("ids") String stockIDs);

    /**
     * check
     * API 2  取得全部股票
     */
    @Headers({"Content-Type: application/json;charset=utf-8"})
    @GET("stock-app/search/")
    Call<ApiObjectSearchResult> getAllStocks();


    /**
     * check
     * API 2  Search 單一股票
     *
     * @param queryText 股票代號或名稱
     */
    @Headers({"Content-Type: application/json;charset=utf-8"})
    @GET("stock-app/search/")
    Call<ApiObjectSearchResult> getSearchStock(@Query("text") String queryText);


    /**
     * check
     * API 8  篩選分類結果
     *
     * @param stockExchangeId    篩選分類一
     * @param marketCategoryId   篩選分類二
     * @param industryCategoryId 篩選分類三
     */
    @Headers({"Content-Type: application/json"})
    @GET("stock-app/categories/result/")
    Call<ApiObjectCategoriesResult> getCategoriesResult(
            @Query("stockExchangeId") int stockExchangeId,
            @Query("marketCategoryId") int marketCategoryId,
            @Query("industryCategoryId") int industryCategoryId);


    /**
     * check
     * API 9  股票詳細資訊
     *
     * @param stockId      stockId
     * @param timeInterval K線時間區間 <BR/>
     *                     • "min30" - 極短線  • "day1" - 短線<BR/>
     *                     • "day5" - 中線     • "day20" - 長線<BR/>
     *                     台指欠點<BR/>
     *                     • "min1" - 一分鐘   • "day1" - 一日
     * @param kNumber      要多少跟 K
     */
    @Headers({"Content-Type: application/json"})
    @GET("historical-data/")
    @NonNull
    Call<ApiObjectStockHistoricalData> getStockHistoricalData(
            @Query("stockId") int stockId,
            @Query("timeInterval") String timeInterval,
            @Query("kNumber") int kNumber);


    /**
     * check
     * API 10  股票智慧分析圖
     */
    @Headers({"Content-Type: application/json"})
    @GET("stock-app/stock/image/state/{id}/")
    @NonNull
    Call<ApiObjectStockStateImage> getStockStateImage(@Path("id") int id);


    /**
     * check
     * API 11  智慧分析表格文字
     */
    @Headers({"Content-Type: application/json"})
    @GET("stock-app/stock-analysis/text/{id}/")
    @NonNull
    Call<ApiObjectStockAnalysisText> getSmartAnalysisText(@Path("id") int stockId);

    /**
     * check
     * API 12 智慧圖形趨勢線
     */
    @Headers({"Content-Type: application/json"})
    @GET("stock-app/time-interval/trendlines/?names=uptrendlines-1-level-1&names=downtrendlines-1-level-1")
    Call<ApiObjectSmartAnalysisTrendLine> getSmartAnalysisTrendLine(@Query("stockId") int stockId);


    /**
     * check
     * API 14 DFJ獨家分類
     */
    @Headers({"Content-Type: application/json"})
    @GET("stock-app/suggested-conditions/")
    Call<ApiObjectSuggestedConditionItems> getDfjSuggestedCondition();


    /**
     * check
     * API 15 DFJ 獨家分類結果- 無篩選條件
     * 今日選股 suggested-condition Result
     */
    @Headers({"Content-Type: application/json"})
    @GET("stock-app/suggested-condition/{id}/result/")
    Call<ApiObjectSuggestedConditionResult> getDfjSuggestedConditionResult(
            @Path("id") int DfjSuggestedConditionID);


    /**
     * check
     * API 15 DFJ 獨家分類結果
     * 今日選股 suggested-condition Result
     */
    @Headers({"Content-Type: application/json"})
    @GET("stock-app/suggested-condition/{id}/result/")
    Call<ApiObjectSuggestedConditionResult> getDfjSuggestedConditionResult(
            @Path("id") int DfjSuggestedConditionID, @Query("stockExchangeCode") String stockExchangeCode,
            @Query("marketCategoryId") int marketCategoryId, @Query("industryCategoryId") int industryCategoryId);


    /**
     * check
     * API 16 當日選股  - 無篩選條件
     */
    @Headers({"Content-Type: application/json"})
    @GET("stock-app/suggested-condition/{id}/results/")
    Call<ApiObjectSuggestedHistoricalResult> getDfjSuggestedHistoricalResult(
            @Path("id") int DfjSuggestedConditionID, @Query("startDate") String startDate, @Query("endDate") String endDate);


    /**
     * check
     * API 16-1 當日選股  - 有篩選條件
     */
    @Headers({"Content-Type: application/json"})
    @GET("stock-app/suggested-condition/{id}/results/")
    Call<ApiObjectSuggestedHistoricalResult> getDfjSuggestedHistoricalResult(
            @Path("id") int DfjSuggestedConditionID, @Query("startDate") String startDate,
            @Query("endDate") String endDate, @Query("stockExchangeCode") String stockExchangeCode,
            @Query("marketCategoryId") int marketCategoryId, @Query("industryCategoryId") int industryCategoryId);


    /**
     * check
     * API 19 趨勢線
     */
    @Headers({"Content-Type: application/json"})
    @GET("stock-app/trendlines/?names=uptrendlines&names=downtrendlines")
    Call<ApiObjectStockDFJTrendLine> getAllStockDFJTrendLine(
            @Query("stockId") Integer stockId, @Query("timeInterval") String timeInterval,
            @Query("startDatetime") String startDatetime, @Query("endDatetime") String endDatetime,
            @Query("level") Integer level);


    /**
     * check
     * API 20 股票分類
     */
    @Headers({"Content-Type: application/json"})
    @GET("stock-app/categories/")
    Call<ApiObjectAllStockCategories> getAllStockCategories();


    /**
     * check
     * API 21 當日新增  - 無篩選條件
     */
    @Headers({"Content-Type: application/json"})
    @GET("stock-app/suggested-condition/{id}/new-results/")
    Call<ApiObjectSuggestedHistoricalResult> getDfjSuggestedHistoricalResult2(
            @Path("id") int DfjSuggestedConditionID, @Query("startDate") String startDate, @Query("endDate") String endDate);


    /**
     * check
     * API 21-1 當日新增  - 有篩選條件
     */
    @Headers({"Content-Type: application/json"})
    @GET("stock-app/suggested-condition/{id}/new-results/")
    Call<ApiObjectSuggestedHistoricalResult> getDfjSuggestedHistoricalResult2(
            @Path("id") int DfjSuggestedConditionID, @Query("startDate") String startDate,
            @Query("endDate") String endDate, @Query("stockExchangeCode") String stockExchangeCode,
            @Query("marketCategoryId") int marketCategoryId, @Query("industryCategoryId") int industryCategoryId);


    /**
     * check
     * API 22  台指欠點表格文字
     */
    @Headers({"Content-Type: application/json"})
    @GET("stock-app/lack-point-analysis/text/")
    Call<ApiObjectTaiwanIndexText> getTaiwanIndexText();


    /**
     * check
     * API 23 台指欠點
     */
    @Headers({"Content-Type: application/json"})
    @GET("stock-app/lack-points/")
    Call<ApiObjectTaiwanIndexLackPoints> getTaiwanIndexLackPointsData(
            @Query("timeInterval") String timeInterval,
            @Query("startDatetime") String startIdx,
            @Query("endDatetime") String endIdx);

}
