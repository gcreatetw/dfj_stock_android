package com.gcreate.dfjStock.webAPI.search

class ApiObjectSecuritiesClassificationItem {
    /**
     * kind : stock#SecuritiesClassificationItems
     * items : [{"description":"指數","id":1,"exchangeId":1}]
     */
    var kind: String? = null
    var items: List<ItemsBean>? = null

    class ItemsBean {
        /**
         * description : 指數
         * id : 1
         * exchangeId : 1
         */
        var description: String? = null
        var id = 0
        var exchangeId = 0
    }

    /**
     * success : {"code":200,
     * "message":"stock not found."}
     */
    var success: SuccessBean? = null

    class SuccessBean {
        /**
         * code : 200
         * message : stock not found.
         */
        var code = 0
        var message: String? = null
    }
}