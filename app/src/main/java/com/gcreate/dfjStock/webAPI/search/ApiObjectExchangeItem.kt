package com.gcreate.dfjStock.webAPI.search

class ApiObjectExchangeItem {
    /**
     * kind : stock#ExchangeItems
     * items : [{
     * "description":"台灣證券交易所",
     * "id":1,
     * "code":"tpe"}]
     */
    var kind: String? = null
    var items: List<ItemsBean>? = null

    class ItemsBean {
        /**
         * description : 台灣證券交易所
         * id : 1
         * code : tpe
         */
        var description: String? = null
        var id = 0
        var code: String? = null
    }

    /**
     * success : {"code":200,
     * "message":"stock not found."}
     */
    var success: SuccessBean? = null

    class SuccessBean {
        /**
         * code : 200
         * message : stock not found.
         */
        var code = 0
        var message: String? = null
    }
}