package com.gcreate.dfjStock.webAPI.tab3

data class ApiObjectTaiwanIndexText(
    val data: TaiwanIndexTextData,
    val message: String,
    val status: String,
)

data class TaiwanIndexTextData(
    val create_date: String,
    val meta_key: String,
    val meta_value: MutableList<MetaValue>,
    val stock_id: Int,
)

data class MetaValue(
    val continueMessage: String,
    val direaction: String,
    val distance: Double,
    val inTheDayMessage: String,
    val kDatetime: String,
    val offTheDayMessage: String,
    val price: Double,
)