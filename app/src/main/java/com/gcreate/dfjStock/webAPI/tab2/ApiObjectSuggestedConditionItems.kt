package com.gcreate.dfjStock.webAPI.tab2

data class ApiObjectSuggestedConditionItems(
    val `data`: MutableList<SuggestedConditionsData>,
    val message: String,
    val status: String
)

data class SuggestedConditionsData(
    val description: String,
    val id: Int
)