package com.gcreate.dfjStock.webAPI;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ApiObjectStock_Detail {


    private String kind;
    private List<ItemsBean> items;

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public List<ItemsBean> getItems() {
        return items;
    }

    public void setItems(List<ItemsBean> items) {
        this.items = items;
    }

    public static class ItemsBean {


        private TrendlineDataBean trendline_data;
        private int id;
        private String exchange_code;
        private String symbol;
        private String description;
        private String securities_classification_description;
        private String market_description;
        private Object industrial_classification_description;
        private int state;

        public TrendlineDataBean getTrendline_data() {
            return trendline_data;
        }

        public void setTrendline_data(TrendlineDataBean trendline_data) {
            this.trendline_data = trendline_data;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getExchange_code() {
            return exchange_code;
        }

        public void setExchange_code(String exchange_code) {
            this.exchange_code = exchange_code;
        }

        public String getSymbol() {
            return symbol;
        }

        public void setSymbol(String symbol) {
            this.symbol = symbol;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getSecurities_classification_description() {
            return securities_classification_description;
        }

        public void setSecurities_classification_description(String securities_classification_description) {
            this.securities_classification_description = securities_classification_description;
        }

        public String getMarket_description() {
            return market_description;
        }

        public void setMarket_description(String market_description) {
            this.market_description = market_description;
        }

        public Object getIndustrial_classification_description() {
            return industrial_classification_description;
        }

        public void setIndustrial_classification_description(Object industrial_classification_description) {
            this.industrial_classification_description = industrial_classification_description;
        }

        public int getState() {
            return state;
        }

        public void setState(int state) {
            this.state = state;
        }

        public static class TrendlineDataBean {


            @SerializedName("30min")
            private _$30minBean _$30min;
            @SerializedName("1day")
            private _$1dayBean _$1day;
            @SerializedName("5day")
            private _$5dayBean _$5day;
            @SerializedName("20day")
            private _$20dayBean _$20day;

            public _$30minBean get_$30min() {
                return _$30min;
            }

            public void set_$30min(_$30minBean _$30min) {
                this._$30min = _$30min;
            }

            public _$1dayBean get_$1day() {
                return _$1day;
            }

            public void set_$1day(_$1dayBean _$1day) {
                this._$1day = _$1day;
            }

            public _$5dayBean get_$5day() {
                return _$5day;
            }

            public void set_$5day(_$5dayBean _$5day) {
                this._$5day = _$5day;
            }

            public _$20dayBean get_$20day() {
                return _$20day;
            }

            public void set_$20day(_$20dayBean _$20day) {
                this._$20day = _$20day;
            }

            public static class _$30minBean {
                /**
                 * trendline_change_day : 7
                 * trendline_now_state : 0
                 * trendline_before_state : 2
                 */

                private int trendline_change_day;
                private int trendline_now_state;
                private int trendline_before_state;

                public int getTrendline_change_day() {
                    return trendline_change_day;
                }

                public void setTrendline_change_day(int trendline_change_day) {
                    this.trendline_change_day = trendline_change_day;
                }

                public int getTrendline_now_state() {
                    return trendline_now_state;
                }

                public void setTrendline_now_state(int trendline_now_state) {
                    this.trendline_now_state = trendline_now_state;
                }

                public int getTrendline_before_state() {
                    return trendline_before_state;
                }

                public void setTrendline_before_state(int trendline_before_state) {
                    this.trendline_before_state = trendline_before_state;
                }
            }

            public static class _$1dayBean {
                /**
                 * trendline_change_day : 1
                 * trendline_now_state : 2
                 * trendline_before_state : 1
                 */

                private int trendline_change_day;
                private int trendline_now_state;
                private int trendline_before_state;

                public int getTrendline_change_day() {
                    return trendline_change_day;
                }

                public void setTrendline_change_day(int trendline_change_day) {
                    this.trendline_change_day = trendline_change_day;
                }

                public int getTrendline_now_state() {
                    return trendline_now_state;
                }

                public void setTrendline_now_state(int trendline_now_state) {
                    this.trendline_now_state = trendline_now_state;
                }

                public int getTrendline_before_state() {
                    return trendline_before_state;
                }

                public void setTrendline_before_state(int trendline_before_state) {
                    this.trendline_before_state = trendline_before_state;
                }
            }

            public static class _$5dayBean {
                /**
                 * trendline_change_day : 1
                 * trendline_now_state : 2
                 * trendline_before_state : 1
                 */

                private int trendline_change_day;
                private int trendline_now_state;
                private int trendline_before_state;

                public int getTrendline_change_day() {
                    return trendline_change_day;
                }

                public void setTrendline_change_day(int trendline_change_day) {
                    this.trendline_change_day = trendline_change_day;
                }

                public int getTrendline_now_state() {
                    return trendline_now_state;
                }

                public void setTrendline_now_state(int trendline_now_state) {
                    this.trendline_now_state = trendline_now_state;
                }

                public int getTrendline_before_state() {
                    return trendline_before_state;
                }

                public void setTrendline_before_state(int trendline_before_state) {
                    this.trendline_before_state = trendline_before_state;
                }
            }

            public static class _$20dayBean {
                /**
                 * trendline_change_day : 83
                 * trendline_now_state : 1
                 * trendline_before_state : 2
                 */

                private int trendline_change_day;
                private int trendline_now_state;
                private int trendline_before_state;

                public int getTrendline_change_day() {
                    return trendline_change_day;
                }

                public void setTrendline_change_day(int trendline_change_day) {
                    this.trendline_change_day = trendline_change_day;
                }

                public int getTrendline_now_state() {
                    return trendline_now_state;
                }

                public void setTrendline_now_state(int trendline_now_state) {
                    this.trendline_now_state = trendline_now_state;
                }

                public int getTrendline_before_state() {
                    return trendline_before_state;
                }

                public void setTrendline_before_state(int trendline_before_state) {
                    this.trendline_before_state = trendline_before_state;
                }
            }
        }
    }
}
