package com.gcreate.dfjStock.webAPI.tab3

class ApiObjectTaiwanIndexArticle {
    /**
     * kind : stock#MetadataDate
     * items : [{"metaValue":"fitx1 text_stock_lack_of_point_analysis.","stockId":2012,"createDate":"2021-01-11","metaKey":"text_stock_lack_of_point_analysis"}]
     */
    var kind: String? = null
    var items: List<ItemsBean>? = null

    class ItemsBean {
        /**
         * metaValue : fitx1 text_stock_lack_of_point_analysis.
         * stockId : 2012
         * createDate : 2021-01-11
         * metaKey : text_stock_lack_of_point_analysis
         */
        var metaValue: String? = null
        var stockId = 0
        var createDate: String? = null
        var metaKey: String? = null
    }

    /**
     * success : {"code":200,"message":"metadata on date not found."}
     */
    var success: SuccessBean? = null

    class SuccessBean {
        /**
         * code : 200
         * message : metadata on date not found.
         */
        var code = 0
        var message: String? = null
    }

    /**
     * error : {"code":"http_status_code","message":"message"}
     */
    var error: ErrorBean? = null

    class ErrorBean {
        /**
         * code : http_status_code
         * message : message
         */
        var code: String? = null
        var message: String? = null
    }
}