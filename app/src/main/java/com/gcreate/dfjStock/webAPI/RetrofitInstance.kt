package com.gcreate.dfjStock.webAPI

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class RetrofitInstance {

    companion object {

        //  old server
        private var DFJ_PATH: String? = "dfj.com.tw:30443/stock-analysis"
        private var DFJ_SERVER = "https://$DFJ_PATH/api/"

        private var dfjApiInstance: dfjAPI? = null

        fun getDfjApiInstance(): dfjAPI? {
            if (dfjApiInstance == null) {
                val interceptor = HttpLoggingInterceptor()
                interceptor.level = HttpLoggingInterceptor.Level.BODY
                val client = OkHttpClient.Builder().addInterceptor(interceptor).addInterceptor { chain ->
                    val builder = chain.request().newBuilder()
                    val build = builder.build()
                    chain.proceed(build)
                }.connectTimeout(60 , TimeUnit.SECONDS).readTimeout(60 , TimeUnit.SECONDS).build()
                dfjApiInstance = Retrofit.Builder().baseUrl(DFJ_SERVER).addConverterFactory(GsonConverterFactory.create()).client(client).build()
                    .create(dfjAPI::class.java)
            }
            return dfjApiInstance
        }

        //  DFJ new server
        private var DFJ_SERVER2 = "https://api.dfj.com.tw/stock-analysis/"
        private var dfjApiInstance2: dfjAPI? = null

        fun getDfjApiInstance2(): dfjAPI {
            if (dfjApiInstance2 == null) {
                val interceptor = HttpLoggingInterceptor()
                interceptor.level = HttpLoggingInterceptor.Level.BODY
                val client = OkHttpClient.Builder().addInterceptor(interceptor).addInterceptor { chain ->
                    val builder = chain.request().newBuilder()
                    val build = builder.build()
                    chain.proceed(build)
                }.connectTimeout(60 , TimeUnit.SECONDS).readTimeout(60 , TimeUnit.SECONDS).build()
                dfjApiInstance2 = Retrofit.Builder().baseUrl(DFJ_SERVER2).addConverterFactory(GsonConverterFactory.create()).client(client).build()
                    .create(dfjAPI::class.java)
            }
            return dfjApiInstance2!!
        }


        //  GC server
        var GC_PATH: String? = "www.dfj.com.tw/wp-json/"
        var GC_SERVER = "https://" + GC_PATH + "dfj_api/"

        private var gcAPIInstance: gcAPI? = null

        fun getGCApiInstance(): gcAPI? {
            if (gcAPIInstance == null) {
                val interceptor = HttpLoggingInterceptor()
                interceptor.level = HttpLoggingInterceptor.Level.BODY
                val client = OkHttpClient.Builder().addInterceptor(interceptor).addInterceptor { chain ->
                    val builder = chain.request().newBuilder()
                    val build = builder.build()
                    chain.proceed(build)
                }.connectTimeout(60 , TimeUnit.SECONDS).readTimeout(60 , TimeUnit.SECONDS).build()
                gcAPIInstance = Retrofit.Builder().baseUrl(GC_SERVER).addConverterFactory(GsonConverterFactory.create()).client(client).build()
                    .create(gcAPI::class.java)
            }
            return gcAPIInstance
        }



    }
}