package com.gcreate.dfjStock.webAPI;

import com.gcreate.dfjStock.webAPI.login.ApiObjectAppVersion;
import com.gcreate.dfjStock.webAPI.login.ApiObjectLogin;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;


public interface gcAPI {


    String PATH = "www.dfj.com.tw/wp-json/";
    String GC_SERVER = "https://" + PATH + "dfj_api/";


    //------------------     GET ----------------------
    //  取得APP Version
    @Headers({"Content-Type: application/json"})
    @GET("app_vertion")
    Call<ApiObjectAppVersion> getAppVersion();

    //--------------- POST  ---------------------------
    //  取得會員資料
    @Headers({"Content-Type: application/json"})
    @POST("user?/")
    Call<ApiObjectLogin> GetLoginFeedback(@Query("username") String account, @Query("password") String password);

    @Headers({"Content-Type: application/json"})
    @POST("update_group_1")
    Call<ApiObjectUpdateFeedback> updateSingleGroup(@Body JsonObject jsonObject);

    @Headers({"Content-Type: application/json"})
    @POST("update_group_2")
    Call<ApiObjectUpdateFeedback> updateAllGroup(@Body JsonObject jsonObject);

    @Headers({"Content-Type: application/json"})
    @POST("change_name")
    Call<ApiObjectUpdateUserName> updateUserName(@Body JsonObject jsonObject);


}
