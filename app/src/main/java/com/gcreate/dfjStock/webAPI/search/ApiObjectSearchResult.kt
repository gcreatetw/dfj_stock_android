package com.gcreate.dfjStock.webAPI.search

data class ApiObjectSearchResult(
    val status: String,
    val message: String?,
    val data: MutableList<SearchData>?
)

data class SearchData(
    val state: Int,
    val description: String,
    val symbol: String,
    val industryCategoryDescription: String,
    val industryCategoryId: Int,
    val marketCategoryDescription: String,
    val marketCategoryId: Int,
    val stockExchangeCode: String,
    val id: Int
)