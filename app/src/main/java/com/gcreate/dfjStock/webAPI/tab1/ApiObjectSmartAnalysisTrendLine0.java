package com.gcreate.dfjStock.webAPI.tab1;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ApiObjectSmartAnalysisTrendLine0 {

    private String kind;
    private ItemsBean items;

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public ItemsBean getItems() {
        return items;
    }

    public void setItems(ItemsBean items) {
        this.items = items;
    }

    public static class ItemsBean {
        @SerializedName("30min")
        private List<_$30minBean> _$30min;
        @SerializedName("1day")
        private List<_$1dayBean> _$1day;
        @SerializedName("5day")
        private List<_$5dayBean> _$5day;
        @SerializedName("20day")
        private List<_$20dayBean> _$20day;

        public List<_$30minBean> get_$30min() {
            return _$30min;
        }

        public void set_$30min(List<_$30minBean> _$30min) {
            this._$30min = _$30min;
        }

        public List<_$1dayBean> get_$1day() {
            return _$1day;
        }

        public void set_$1day(List<_$1dayBean> _$1day) {
            this._$1day = _$1day;
        }

        public List<_$5dayBean> get_$5day() {
            return _$5day;
        }

        public void set_$5day(List<_$5dayBean> _$5day) {
            this._$5day = _$5day;
        }

        public List<_$20dayBean> get_$20day() {
            return _$20day;
        }

        public void set_$20day(List<_$20dayBean> _$20day) {
            this._$20day = _$20day;
        }

        public static class _$30minBean {
            /**
             * repeatLevel : 1
             * a : -0.50121009349823
             * b : 3787.041015625
             */

            private int repeatLevel;
            private double a;
            private double b;

            public int getRepeatLevel() {
                return repeatLevel;
            }

            public void setRepeatLevel(int repeatLevel) {
                this.repeatLevel = repeatLevel;
            }

            public double getA() {
                return a;
            }


            public void setA(double a) {
                this.a = a;
            }

            public double getB() {
                return b;
            }

            public void setB(double b) {
                this.b = b;
            }
        }

        public static class _$1dayBean {
            private Object repeatLevel;
            private double a;
            private double b;

            public Object getRepeatLevel() {
                return repeatLevel;
            }

            public void setRepeatLevel(Object repeatLevel) {
                this.repeatLevel = repeatLevel;
            }

            public double getA() {
                return a;
            }

            public void setA(double a) {
                this.a = a;
            }

            public double getB() {
                return b;
            }

            public void setB(double b) {
                this.b = b;
            }
        }

        public static class _$5dayBean {
            private Object repeatLevel;
            private double a;
            private double b;

            public Object getRepeatLevel() {
                return repeatLevel;
            }

            public void setRepeatLevel(Object repeatLevel) {
                this.repeatLevel = repeatLevel;
            }

            public double getA() {
                return a;
            }

            public void setA(double a) {
                this.a = a;
            }

            public double getB() {
                return b;
            }

            public void setB(double b) {
                this.b = b;
            }
        }

        public static class _$20dayBean {
            private Object repeatLevel;
            private double a;
            private double b;

            public Object getRepeatLevel() {
                return repeatLevel;
            }

            public void setRepeatLevel(Object repeatLevel) {
                this.repeatLevel = repeatLevel;
            }

            public double getA() {
                return a;
            }

            public void setA(double a) {
                this.a = a;
            }

            public double getB() {
                return b;
            }

            public void setB(double b) {
                this.b = b;
            }
        }
    }
}
