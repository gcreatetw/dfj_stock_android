package com.gcreate.dfjStock.webAPI.tab3

import com.google.gson.annotations.SerializedName
import java.util.ArrayList

class ApiObjectTaiwanIndexHorizontalData {
    /**
     * kind : stock#LackOfPointIdxs
     * items : [{"excessState":1,"index":97921,"dayIdx":5338,"startIdx":97909,"endIdx":97936,"price":12144,"lackPrice":47,"coveringIdx":97560,"coveringState":0,"continuanceIdx":97560,"continuanceState":0,"continuancePrice":-1,"excessIdx":98762}]
     */
    var kind: String? = null
    var items: List<ItemsBean> = ArrayList()

    class ItemsBean {
        /**
         * excessState : 1
         * index : 97921
         * dayIdx : 5338
         * startIdx : 97909
         * endIdx : 97936
         * price : 12144.0
         * lackPrice : 47.0
         * coveringIdx : 97560
         * coveringState : 0
         * continuanceIdx : 97560
         * continuanceState : 0
         * continuancePrice : -1.0
         * excessIdx : 98762
         */
        var excessState = 0
        var index = 0
        var dayIdx = 0
        var startIdx = 0
        var endIdx = 0
        var price = 0.0
        var lackPrice = 0.0
        var coveringIdx = 0
        var coveringState = 0
        var continuanceIdx = 0
        var continuanceState = 0
        var continuancePrice = 0.0
        var excessIdx = 0
    }

    /**
     * success : {"code":200,"message":"lack of point not found."}
     */
    @SerializedName("success")
    var success: SuccessBean? = null

    class SuccessBean {
        /**
         * code : 200
         * message : lack of point not found.
         */
        var code = 0
        var message: String? = null
    }

    /**
     * error : {"code":"http_status_code","message":"message"}
     */
    @SerializedName("error")
    var error: ErrorBean? = null

    class ErrorBean {
        /**
         * code : http_status_code
         * message : message
         */
        @SerializedName("code")
        var code = 0

        @SerializedName("message")
        var message: String? = null
    }
}