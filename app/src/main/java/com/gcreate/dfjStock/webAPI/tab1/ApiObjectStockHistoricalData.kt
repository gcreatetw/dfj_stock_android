package com.gcreate.dfjStock.webAPI.tab1

import com.gcreate.dfjStock.webAPI.all.KData

data class ApiObjectStockHistoricalData(
    val `data`: MutableList<KData>,
    val message: String,
    val status: String
)

