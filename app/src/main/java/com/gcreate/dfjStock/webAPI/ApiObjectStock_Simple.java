package com.gcreate.dfjStock.webAPI;

import java.util.List;

public class ApiObjectStock_Simple {

    /**
     * kind : string
     * items : [{
     * "id":0,
     * "exchangeId":"int",
     * "exchangeCode":"string",
     * "symbol":"string",
     * "description":"string"}]
     */

    private String kind;
    private List<ItemsBean> items;

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public List<ItemsBean> getItems() {
        return items;
    }

    public void setItems(List<ItemsBean> items) {
        this.items = items;
    }

    public static class ItemsBean {
        /**
         * id : 0
         * exchangeId : int
         * exchangeCode : string
         * symbol : string
         * description : string
         */

        private int id;
        private int exchangeId;
        private String exchangeCode;
        private String symbol;
        private String description;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getExchangeId() {
            return exchangeId;
        }

        public void setExchangeId(int exchangeId) {
            this.exchangeId = exchangeId;
        }

        public String getExchangeCode() {
            return exchangeCode;
        }

        public void setExchangeCode(String exchangeCode) {
            this.exchangeCode = exchangeCode;
        }

        public String getSymbol() {
            return symbol;
        }

        public void setSymbol(String symbol) {
            this.symbol = symbol;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }

    /**
     * success : {"code":200,"message":"stock not found."}
     */
    private SuccessBean success;
    public SuccessBean getSuccess() {
        return success;
    }

    public void setSuccess(SuccessBean success) {
        this.success = success;
    }

    public static class SuccessBean {
        /**
         * code : 200
         * message : stock not found.
         */

        private int code;
        private String message;

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}
