package com.gcreate.dfjStock.webAPI.tab1

class ApiObjectStockAnalysisText(
    val data: StockAnalysisData,
    val message: String,
    val status: String
)

data class StockAnalysisData(
    val createDate: String,
    val metaKey: String,
    val metaValue: MutableList<StockAnalysisMetaValue>,
    val stockId: Int
)

data class StockAnalysisMetaValue(
    val pressureAngle: Double,
    val pressureDistancePercent: Double,
    val pressureNumber: Int,
    val pressurePrice: Double,
    val supportAngle: Double,
    val supportDistancePercent: Double,
    val supportNumber: Int,
    val supportPrice: Double,
    val timeInterval: String,
    val trendlineBeforeState: String,
    val trendlineNowState: String,
    val trendlineStateChangeDay: Int
)