package com.gcreate.dfjStock.webAPI.tab1

data class ApiObjectStockDFJTrendLine(
    val `data`: MutableList<TrendlineData>,
    val message: String,
    val status: String,
)

data class TrendlineData(
    val items: MutableList<MutableList<TrendlinesDataItem>>,
    val name: String,
)

data class TrendlinesDataItem(
    val index: Int,
    val kDatetime: String,
    val price: Double,
)