package com.gcreate.dfjStock.webAPI.tab2

class ApiObjectDfjSuggestedCondition {
    /**
     * kind : stock#SuggestedCondtionItems
     * items : [{"description":"全訊號漲勢股","id":1},
     * {"description":"短中長漲勢股","id":2},
     * {"description":"全訊號跌勢股","id":3},
     * {"description":"短中長跌勢股","id":4}}]
     */
    var kind: String? = null
    var items: MutableList<ItemsBean>? = null

    class ItemsBean {
        /**
         * description : 全訊號漲勢股
         * id : 1
         */
        var description: String? = null
        var id = 0
    }
}