package com.gcreate.dfjStock.webAPI.search

class ApiObjectMarketItem {
    /**
     * kind : stock#MarketItems
     * items : [{"description":"上市","id":1,"securitiesClassificationId":1},{"description":"上櫃","id":2,"securitiesClassificationId":1}]
     */
    var kind: String? = null
    var items: List<ItemsBean>? = null

    class ItemsBean {
        /**
         * description : 上市
         * id : 1
         * securitiesClassificationId : 1
         */
        var description: String? = null
        var id = 0
        var securitiesClassificationId = 0
    }

    /**
     * success : {"code":200,
     * "message":"stock not found."}
     */
    var success: String? = null

    class SuccessBean {
        /**
         * code : 200
         * message : stock not found.
         */
        var code = 0
        var message: String? = null
    }
}