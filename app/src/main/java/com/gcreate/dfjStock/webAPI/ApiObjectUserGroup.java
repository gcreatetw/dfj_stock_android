package com.gcreate.dfjStock.webAPI;

import java.util.List;

public class ApiObjectUserGroup {
    /**
     * status : true
     * userFavGroup : [{"Group":" 群組一","favStock":[0,1,2,3]},{"Group":"\u201c群組一\u201d","favStock":[0,1,2,3]}]
     */

    private boolean status;
    private List<UserFavGroupBean> userFavGroup;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public List<UserFavGroupBean> getUserFavGroup() {
        return userFavGroup;
    }

    public void setUserFavGroup(List<UserFavGroupBean> userFavGroup) {
        this.userFavGroup = userFavGroup;
    }

    public static class UserFavGroupBean {
        /**
         * Group : “群組一”
         * favStock : [0,1,2,3]
         */

        private String Group;
        private List<Integer> favStock;

        public String getGroup() {
            return Group;
        }

        public void setGroup(String Group) {
            this.Group = Group;
        }

        public List<Integer> getFavStock() {
            return favStock;
        }

        public void setFavStock(List<Integer> favStock) {
            this.favStock = favStock;
        }
    }
}
