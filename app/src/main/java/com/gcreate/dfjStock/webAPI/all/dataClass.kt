package com.gcreate.dfjStock.webAPI.all

data class States(
    val day1: Int,
    val day20: Int,
    val day5: Int,
    val min30: Int,
)

data class KData(
    val close: Double,
    val high: Double,
    val index: Int,
    val kDatetime: String,
    val low: Double,
    val `open`: Double,
    val volume: Int,
)