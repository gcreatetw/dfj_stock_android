package com.gcreate.dfjStock.Listener;

public interface UpdateNameListener {
    void notifyUserName(String userName);
}
