package com.gcreate.dfjStock.Listener;

public interface InterruptCallControlListener {
    void notifyViewCancelCall();
}
