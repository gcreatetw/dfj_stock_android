package com.gcreate.dfjStock.Listener;

public interface ViewControlListener {
    void notifyViewUpdate(Boolean isShow);
}
