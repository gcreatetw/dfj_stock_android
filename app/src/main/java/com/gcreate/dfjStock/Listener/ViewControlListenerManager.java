package com.gcreate.dfjStock.Listener;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;


/**
 * Android用觀察者模式代替廣播通知刷新界面
 * Reference : https://blog.csdn.net/csm_qz/article/details/46461651
 */

public class ViewControlListenerManager {
    /**
     * 單例模式
     */
    public static ViewControlListenerManager listenerManager;

    /**
     * 注册的接口集合，發送廣播的時候都能收到
     */
    private List<ViewControlListener> iListenerList = new CopyOnWriteArrayList<>();

    /**
     * 獲得單例對象對象
     */
    public static ViewControlListenerManager getInstance() {
        if (listenerManager == null) {
            listenerManager = new ViewControlListenerManager();
        }
        return listenerManager;
    }

    /**
     * 注册監聽
     */
    public void registerListener(ViewControlListener iListener) {
        iListenerList.add(iListener);
    }

    /**
     * 註銷監聽
     */
    public void unRegisterListener(ViewControlListener iListener) {
        if (iListenerList.contains(iListener)) {
            iListenerList.remove(iListener);
        }
    }

    /**
     * 發送廣播
     */
    public void sendBroadCast(Boolean isShow) {
        for (ViewControlListener updateViewListener : iListenerList) {
            updateViewListener.notifyViewUpdate(isShow);

        }
    }


}

