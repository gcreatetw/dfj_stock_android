package com.gcreate.dfjStock.Listener;

public interface KViewOrientationListener {
    void notifyChangeParam(String orientation);
}
