package com.gcreate.dfjStock.StorageDataController

import android.content.Context
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.gcreate.dfjStock.global
import com.gcreate.dfjStock.model.ObjectStockFavItem
import com.gcreate.dfjStock.model.ObjectStockGroup
import com.gcreate.dfjStock.view.LoginPageActivity
import com.gcreate.dfjStock.webAPI.ApiObjectUpdateFeedback
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

object StockGroupMaintain {
    // 所有股票群組列表 ( 群組名 + 列表陣列 )
    private var favStockGroupList: MutableList<ObjectStockGroup>? = null

    // API 回傳群組item 資料 ( id,name. 長、中、短線..etc )
    var groupItem: List<ObjectStockFavItem>? = null

    //--------------------------------Stock Group Shared Preferences Data Control-----------------------------------
    //------------------------------------------- init -------------------------------------------
    fun initFavStockGroupList(context: Context) {
        if (favStockGroupList == null) {
            favStockGroupList = mutableListOf()
        }
        favStockGroupList!!.add(0, ObjectStockGroup("群組一", ArrayList()))
        favStockGroupList!!.add(1, ObjectStockGroup("群組二", ArrayList()))
        favStockGroupList!!.add(2, ObjectStockGroup("群組三", ArrayList()))
        favStockGroupList!!.add(3, ObjectStockGroup("群組四", ArrayList()))
        favStockGroupList!!.add(4, ObjectStockGroup("群組五", ArrayList()))
        favStockGroupList!!.add(5, ObjectStockGroup("群組六", ArrayList()))
        favStockGroupList!!.add(6, ObjectStockGroup("群組七", ArrayList()))
        favStockGroupList!!.add(7, ObjectStockGroup("群組八", ArrayList()))
        favStockGroupList!!.add(8, ObjectStockGroup("群組九", ArrayList()))
        favStockGroupList!!.add(9, ObjectStockGroup("群組十", ArrayList()))
        val editor = (context as AppCompatActivity).getPreferences(Context.MODE_PRIVATE).edit()
        editor.putString("FavStockGroupList", Gson().toJson(favStockGroupList))
        editor.apply()
    }

    //------------------------------------------- Get  取得全部群組名稱-------------------------------------------
    fun getFavStockGroupList(context: Context) {
        val sp = (context as AppCompatActivity).getPreferences(Context.MODE_PRIVATE)
        val ListJson = sp.getString("FavStockGroupList", "Empty Array")
        if (!ListJson!!.isEmpty()) {
            val gson = Gson()
            favStockGroupList = gson.fromJson(ListJson, object : TypeToken<List<ObjectStockGroup?>?>() {}.type)
        }
    }

    //------------------------------------------- Save  -------------------------------------------
    fun saveFavStockGroupList(context: Context) {
        if (favStockGroupList != null) {
            val editor = (context as AppCompatActivity).getPreferences(Context.MODE_PRIVATE).edit()
            editor.putString("FavStockGroupList", Gson().toJson(favStockGroupList))
            editor.apply()
        }
    }

    //------------------------------------------- Update Group Name  -------------------------------------------
    fun updateFavStockGroupList(pos: Int, GroupName: String?) {
        if (favStockGroupList != null && favStockGroupList!!.size > pos) {
            LoginPageActivity.objectLoginFeedback.data.stock_group[pos].group_name = GroupName!!
            updateSingleGroupName(pos)
        }
    }

    //------------------------------------------- Add  -------------------------------------------
    fun addFavStockGroupItem(context: Context, objectStockGroupItem: ObjectStockGroup) {
        if (favStockGroupList != null) {
            favStockGroupList!!.add(objectStockGroupItem)
        }
        saveFavStockGroupList(context)
        getFavStockGroupList(context)
    }

    //------------------------------------------- Delete  -------------------------------------------
    fun deleteFavStockGroupItem(context: Context, pos: Int) {
        if (favStockGroupList != null && favStockGroupList!!.size > pos) {
            favStockGroupList!!.removeAt(pos)
            saveFavStockGroupList(context)
            getFavStockGroupList(context)
        }
    }

    //------------------------------------------- Get  取得全部群組名稱-------------------------------------------
    fun getFavGroupDetailItem(context: Context, position: Int): List<ObjectStockFavItem>? {
        val sp = (context as AppCompatActivity).getPreferences(Context.MODE_PRIVATE)
        val ListJson = sp.getString("Group$position", "")
        if (!ListJson!!.isEmpty()) {
            val gson = Gson()
            groupItem = gson.fromJson(ListJson, object : TypeToken<List<ObjectStockFavItem?>?>() {}.type)
        } else {
            groupItem = ArrayList()
        }
        return groupItem
    }

    //------------------------------------------- Save  -------------------------------------------
    fun saveGroupItem(context: Context, pos: Int) {
        if (groupItem != null) {
            val editor = (context as AppCompatActivity).getPreferences(Context.MODE_PRIVATE).edit()
            editor.putString("FavStockGroupListItem$pos", Gson().toJson(groupItem))
            editor.apply()
        }
    }

    //------------------------------------------- Add single stock to one group  -------------------------------------------
    fun addStockToGroup(favGroupPosition: Int, stockID: Int?) {
        if (favStockGroupList != null && favStockGroupList!!.size > favGroupPosition) {
            //  某股票群組的項目列表

            LoginPageActivity.objectLoginFeedback.data.stock_group[favGroupPosition].contain.add(stockID!!)
            Log.d("ben","datalist = ${LoginPageActivity.objectLoginFeedback.data.stock_group[favGroupPosition].contain.toList()}")
            updateSingleGroup(favGroupPosition)
        }
    }

    //------------------------------------------- Insert single stock to one group  -------------------------------------------
    fun insertStockToGroup(whichStockGroup: Int, originRemovePosition: Int, stockID: Int?) {
        if (favStockGroupList != null && favStockGroupList!!.size > whichStockGroup) {
            //  某股票群組的項目列表
            LoginPageActivity.objectLoginFeedback.data.stock_group[whichStockGroup].contain.add(originRemovePosition, stockID!!)
            updateSingleGroup(whichStockGroup)
        }
    }

    //------------------------------------------- Remove single stock to one group  -------------------------------------------
    fun removeStockFromOneGroup(favGroupPosition: Int, removeItemPosition: Int) {
        if (favStockGroupList != null && favStockGroupList!!.size > favGroupPosition) {
            //  某股票群組的項目列表
            LoginPageActivity.objectLoginFeedback.data.stock_group[favGroupPosition].contain.removeAt(removeItemPosition)
            updateSingleGroup(favGroupPosition)
        }
    }

    //------------------------------------------- get single fav group stock  list  -------------------------------------------
    fun getFavGroupItemList(position: Int): List<String>? {
        return favStockGroupList!![position].groupItemList
    }

    private fun updateSingleGroup(whichStockGroup: Int) {
        /*  JSON Body format
         {
           "user_id": 73,
           "group_id": 1,
          "group_name": "群組一",
          "contain": [ 339 ]
         }
         */
        val paramObject = JsonObject()
        paramObject.addProperty("user_id", LoginPageActivity.objectLoginFeedback.data.user_id)
        paramObject.addProperty("group_id", whichStockGroup + 1)
        val jsonArray = JsonArray()
        for (i in LoginPageActivity.objectLoginFeedback.data.stock_group[whichStockGroup].contain.indices) {
            jsonArray.add(LoginPageActivity.objectLoginFeedback.data.stock_group[whichStockGroup].contain[i])
        }
        paramObject.add("contain", jsonArray)
        val call = global.gcAPI.updateSingleGroup(paramObject)
        call.enqueue(object : Callback<ApiObjectUpdateFeedback?> {
            override fun onResponse(call: Call<ApiObjectUpdateFeedback?>, response: Response<ApiObjectUpdateFeedback?>) {
                val objectUpdateFeedback = response.body()
            }

            override fun onFailure(call: Call<ApiObjectUpdateFeedback?>, t: Throwable) {}
        })
    }

    private fun updateSingleGroupName(whichStockGroup: Int) {
        /*  JSON Body format
         {
           "user_id": 73,
           "group_id": 1,
          "group_name": "群組一",
          "contain": [ 339 ]
         }
         */
        val paramObject = JsonObject()
        paramObject.addProperty("user_id", LoginPageActivity.objectLoginFeedback.data.user_id)
        paramObject.addProperty("group_id", whichStockGroup + 1)
        paramObject.addProperty("group_name", LoginPageActivity.objectLoginFeedback.data.stock_group[whichStockGroup].group_name)
        val call = global.gcAPI.updateSingleGroup(paramObject)
        call.enqueue(object : Callback<ApiObjectUpdateFeedback?> {
            override fun onResponse(call: Call<ApiObjectUpdateFeedback?>, response: Response<ApiObjectUpdateFeedback?>) {
                val objectUpdateFeedback = response.body()
            }

            override fun onFailure(call: Call<ApiObjectUpdateFeedback?>, t: Throwable) {}
        })
    }
}