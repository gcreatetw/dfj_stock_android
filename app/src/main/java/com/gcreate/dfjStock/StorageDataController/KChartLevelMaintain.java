package com.gcreate.dfjStock.StorageDataController;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.appcompat.app.AppCompatActivity;

import static android.content.Context.MODE_PRIVATE;

public class KChartLevelMaintain {
    public static void setKChartTrendLineLevel(Context context, int trendLineValue) {
        SharedPreferences.Editor editor = ((AppCompatActivity) context).getPreferences(MODE_PRIVATE).edit();
        editor.putInt("KChartTrendLineLevel", trendLineValue);
        editor.apply();
        editor.commit();
    }

    public static int getKChartTrendLineLevel(Context context) {
        return ((AppCompatActivity) context).getPreferences(MODE_PRIVATE).getInt("KChartTrendLineLevel", 1);
    }
}
