package com.gcreate.dfjStock.StorageDataController;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.appcompat.app.AppCompatActivity;

import com.gcreate.dfjStock.model.ObjectKNumModel;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class KNumGroupMaintain {

    public static List<ObjectKNumModel> kNumModelsList;     // 所有股票群組列表 ( 群組名 + 列表陣列 )

    //------------------------------------------- init -------------------------------------------
    public static void initKNumGroupList(Context context) {
        kNumModelsList = new ArrayList<>();
        kNumModelsList.add(0, new ObjectKNumModel("長線", 100));
        kNumModelsList.add(1, new ObjectKNumModel("中線", 100));
        kNumModelsList.add(2, new ObjectKNumModel("短線", 100));
        kNumModelsList.add(3, new ObjectKNumModel("極短線", 100));
        kNumModelsList.add(4, new ObjectKNumModel("欠點一分", 100));
        kNumModelsList.add(5, new ObjectKNumModel("欠點一日", 100));

        SharedPreferences.Editor editor = ((AppCompatActivity) context).getPreferences(MODE_PRIVATE).edit();
        editor.putString("kNumModels", new Gson().toJson(kNumModelsList));
        editor.apply();
        editor.commit();
    }

    //------------------------------------------- Get  取得kNum 群組名稱-------------------------------------------
    public static List<ObjectKNumModel> getKNumModelsList(Context context) {
        SharedPreferences sp = ((AppCompatActivity) context).getPreferences(MODE_PRIVATE);
        String ListJson = sp.getString("kNumModels", "Empty Array");

        if (!ListJson.isEmpty()) {
            Gson gson = new Gson();
            kNumModelsList = gson.fromJson(ListJson, new TypeToken<List<ObjectKNumModel>>() {
            }.getType());
        }
        return kNumModelsList;
    }

    //------------------------------------------- Save  -------------------------------------------
    public static void saveKNumModelsList(Context context) {
        if (kNumModelsList != null) {
            SharedPreferences.Editor editor = ((AppCompatActivity) context).getPreferences(MODE_PRIVATE).edit();
            editor.putString("kNumModels", new Gson().toJson(kNumModelsList));
            editor.apply();
            editor.commit();
        }
    }

    //------------------------------------------- Update  -------------------------------------------
    public static void updateKNumModelsList(Context context, int pos, String GroupName, int KNumCount) {
        if (kNumModelsList != null && kNumModelsList.size() > pos) {
            //  某股票群組的項目列表
            kNumModelsList.remove(pos);
            kNumModelsList.add(pos, new ObjectKNumModel(GroupName, KNumCount));
            saveKNumModelsList(context);
            getKNumModelsList(context);
        }
    }

    //------------------------------------------- Get  取得 KNum Value -------------------------------------------
    public static int getKNumValue(Context context) {
        return ((AppCompatActivity) context).getPreferences(MODE_PRIVATE).getInt("KNum", 100);
    }

}
