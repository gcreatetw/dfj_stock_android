package com.gcreate.dfjStock.StorageDataController;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class MemberMaintain {


    public static List<String> ap;     //   會員帳密

    //------------------------------------------- init -------------------------------------------
    public static void initAccountPassword(Context context) {
        ap = new ArrayList<>();
        ap.add(0, "");  //  帳號
        ap.add(1, "");  //  密碼

        SharedPreferences.Editor editor = ((AppCompatActivity) context).getPreferences(MODE_PRIVATE).edit();
        editor.putString("AccountPassword", new Gson().toJson(ap));
        editor.apply();
        editor.commit();
    }


    //------------------------------------------- Get  取得帳密-------------------------------------------
    public static void getAccountPassword(Context context) {
        SharedPreferences sp = ((AppCompatActivity) context).getPreferences(MODE_PRIVATE);
        String ListJson = sp.getString("AccountPassword", "Empty Array");

        if (!ListJson.isEmpty()) {
            Gson gson = new Gson();
            ap = gson.fromJson(ListJson, new TypeToken<List<String>>() {
            }.getType());
        }
    }

    //------------------------------------------- Save  -------------------------------------------
    public static void saveAccountPassword(Context context) {
        if (ap != null) {
            SharedPreferences.Editor editor = ((AppCompatActivity) context).getPreferences(MODE_PRIVATE).edit();
            editor.putString("AccountPassword", new Gson().toJson(ap));
            editor.apply();
            editor.commit();
        }
    }


    //------------------------------------------- Update   -------------------------------------------
    public static void updateAccountPassword(Context context, String account, String password) {
        if (ap != null) {
            ap.removeAll(ap);
            ap.add(0, account);
            ap.add(1, password);
            saveAccountPassword(context);
        }
    }

}
