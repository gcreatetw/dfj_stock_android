package com.gcreate.dfjStock.adapter.tab4

import android.annotation.SuppressLint
import android.graphics.PorterDuff
import android.os.Bundle
import android.util.Log
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.gcreate.dfjStock.R
import com.gcreate.dfjStock.view.dialog.DeleteStockGroupItemDialogFragment
import com.gcreate.dfjStock.view.dialog.EditStockGroupItemDialogFragment
import com.gcreate.dfjStock.webAPI.login.StockGroup
import kotlinx.android.synthetic.main.card_group_maintain_item.view.*

class StockGroupMaintainAdapter(data: MutableList<StockGroup>) :
    BaseQuickAdapter<StockGroup, BaseViewHolder>(R.layout.card_group_maintain_item, data) {

    private var isEditSelected = false
    private var isDeletedSelected = false

    private val args = Bundle()

    companion object {
        private var editSelectedPostion = 0
        private var deleteSelectedPostion = 0
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun convert(holder: BaseViewHolder, item: StockGroup) {

        // Set selected color
        if (!isEditSelected && !isDeletedSelected) {
            holder.itemView.ibtn_edit.setColorFilter(ContextCompat.getColor(holder.itemView.context, R.color.WhiteFFFFFF), PorterDuff.Mode.MULTIPLY)
            holder.itemView.ibtn_delete.setColorFilter(ContextCompat.getColor(holder.itemView.context, R.color.WhiteFFFFFF), PorterDuff.Mode.MULTIPLY)
        }
        if (isEditSelected && !isDeletedSelected) {
            if (getItemPosition(item) == editSelectedPostion) {
                holder.itemView.ibtn_edit.setColorFilter(ContextCompat.getColor(holder.itemView.context, R.color.RedDC0000), PorterDuff.Mode.MULTIPLY)
                holder.itemView.ibtn_delete.setColorFilter(ContextCompat.getColor(holder.itemView.context, R.color.WhiteFFFFFF),
                    PorterDuff.Mode.MULTIPLY)
            } else {
                holder.itemView.ibtn_edit.setColorFilter(ContextCompat.getColor(holder.itemView.context, R.color.WhiteFFFFFF),
                    PorterDuff.Mode.MULTIPLY)
                holder.itemView.ibtn_delete.setColorFilter(ContextCompat.getColor(holder.itemView.context, R.color.WhiteFFFFFF),
                    PorterDuff.Mode.MULTIPLY)
            }
        }
        if (isDeletedSelected && !isEditSelected) {
            if (getItemPosition(item) == deleteSelectedPostion) {
                holder.itemView.ibtn_delete.setColorFilter(ContextCompat.getColor(holder.itemView.context, R.color.RedDC0000),
                    PorterDuff.Mode.MULTIPLY)
                holder.itemView.ibtn_edit.setColorFilter(ContextCompat.getColor(holder.itemView.context, R.color.WhiteFFFFFF),
                    PorterDuff.Mode.MULTIPLY)
            } else {
                holder.itemView.ibtn_edit.setColorFilter(ContextCompat.getColor(holder.itemView.context, R.color.WhiteFFFFFF),
                    PorterDuff.Mode.MULTIPLY)
                holder.itemView.ibtn_delete.setColorFilter(ContextCompat.getColor(holder.itemView.context, R.color.WhiteFFFFFF),
                    PorterDuff.Mode.MULTIPLY)
            }
        }

        holder.setText(R.id.tv_StockGroupItemName, item.group_name)

        holder.itemView.ibtn_edit.setOnClickListener {
            isEditSelected = true
            isDeletedSelected = false
            editSelectedPostion = getItemPosition(item)
            args.putInt("GroupIndex", editSelectedPostion)
            args.putString("GroupName", item.group_name)

            val editStockGroupItemName = EditStockGroupItemDialogFragment()
            editStockGroupItemName.arguments = args
            editStockGroupItemName.show((holder.itemView.context as FragmentActivity).supportFragmentManager, "simple dialog")
            notifyDataSetChanged()
        }

        holder.itemView.ibtn_delete.setOnClickListener {
            isEditSelected = false
            isDeletedSelected = true
            deleteSelectedPostion = getItemPosition(item)

            val deleteGroupItem = DeleteStockGroupItemDialogFragment()
            deleteGroupItem.arguments = args
            deleteGroupItem.show((holder.itemView.context as FragmentActivity).supportFragmentManager, "simple dialog")
            notifyDataSetChanged()
        }
    }
}

