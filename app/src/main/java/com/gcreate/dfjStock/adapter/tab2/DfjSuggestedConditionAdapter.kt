package com.gcreate.dfjStock.adapter.tab2

import com.chad.library.adapter.base.BaseQuickAdapter
import com.gcreate.dfjStock.R
import com.gcreate.dfjStock.adapter.DataBindBaseViewHolder

import com.gcreate.dfjStock.webAPI.tab2.ApiObjectSuggestedConditionItems.*
import com.gcreate.dfjStock.webAPI.tab2.SuggestedConditionsData

class DfjSuggestedConditionAdapter(data: MutableList<SuggestedConditionsData>) :
    BaseQuickAdapter<SuggestedConditionsData, DataBindBaseViewHolder>(R.layout.card_dfj_category_item, data) {

    override fun convert(holder: DataBindBaseViewHolder, item: SuggestedConditionsData) {
        holder.setText(R.id.tv_dfj_categoryTitle, item.description)
        holder.setText(R.id.tv_dfj_categorySubTitle, "")
    }
}