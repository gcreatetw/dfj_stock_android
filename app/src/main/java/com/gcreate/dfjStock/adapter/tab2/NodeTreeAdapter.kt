package com.gcreate.dfjStock.adapter.tab2

import androidx.navigation.NavController
import com.chad.library.adapter.base.BaseNodeAdapter
import com.chad.library.adapter.base.entity.node.BaseNode
import com.gcreate.dfjStock.adapter.tab2.expandView.FirstNode
import com.gcreate.dfjStock.adapter.tab2.expandView.FirstProvider
import com.gcreate.dfjStock.adapter.tab2.expandView.SecondNode
import com.gcreate.dfjStock.adapter.tab2.expandView.SecondProvider


class NodeTreeAdapter(navController: NavController) : BaseNodeAdapter() {

    companion object {
        const val EXPAND_COLLAPSE_PAYLOAD = 110
    }

    init {
        addNodeProvider(FirstProvider())
        addNodeProvider(SecondProvider(navController))
    }

    override fun getItemType(data: List<BaseNode>, position: Int): Int {
        val node = data[position]
        if (node is FirstNode) {
            return 1
        } else if (node is SecondNode) {
            return 2
        }

        return -1
    }
}