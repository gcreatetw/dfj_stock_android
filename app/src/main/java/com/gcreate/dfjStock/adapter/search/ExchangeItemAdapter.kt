package com.gcreate.dfjStock.adapter.search

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.gcreate.dfjStock.R
import com.gcreate.dfjStock.webAPI.search.AllStockCategoriesData

class ExchangeItemAdapter(data: MutableList<AllStockCategoriesData>) :
    BaseQuickAdapter<AllStockCategoriesData, BaseViewHolder>(R.layout.card_verticallist_item, data) {

    override fun convert(holder: BaseViewHolder, item: AllStockCategoriesData) {
        holder.setText(R.id.tv_listItemName, item.description)
    }

}