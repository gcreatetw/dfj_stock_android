package com.gcreate.dfjStock.adapter.tab1

import android.app.Dialog
import android.graphics.Color
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.gcreate.dfjStock.Listener.ListenerManager
import com.gcreate.dfjStock.R
import com.gcreate.dfjStock.webAPI.login.StockGroup
import kotlinx.android.synthetic.main.card_stock_group_item.view.*

class OptionalStockSelectGroupAdapter(private val dialog: Dialog, data: MutableList<StockGroup>) :
    BaseQuickAdapter<StockGroup, BaseViewHolder>(R.layout.card_stock_group_item, data) {

    companion object {
        var position = 0
    }

    override fun convert(holder: BaseViewHolder, item: StockGroup) {
        if (getItemPosition(item) == position) {
            holder.itemView.cardView_favStockGroup.setCardBackgroundColor(Color.parseColor("#DC0000"))
            holder.itemView.tv_favStockGroupName.setTextColor(Color.parseColor("#FFFFFF"))
        } else {
            holder.itemView.cardView_favStockGroup.setCardBackgroundColor(Color.parseColor("#FFFFFF"))
            holder.itemView.tv_favStockGroupName.setTextColor(Color.parseColor("#222222"))
        }

        holder.setText(R.id.tv_favStockGroupName, item.group_name)

        holder.itemView.setOnClickListener {
            position = getItemPosition(item)
            ListenerManager.getInstance().sendBroadCast(getItemPosition(item))
            dialog.dismiss()
        }
    }

}
