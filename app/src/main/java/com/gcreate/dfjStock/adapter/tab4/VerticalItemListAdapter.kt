package com.gcreate.dfjStock.adapter.tab4

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.gcreate.dfjStock.R

class VerticalItemListAdapter(data: MutableList<String>) : BaseQuickAdapter<String, BaseViewHolder>(R.layout.card_verticallist_item, data) {

    override fun convert(holder: BaseViewHolder, item: String) {
        holder.setText(R.id.tv_listItemName, item)
    }
}