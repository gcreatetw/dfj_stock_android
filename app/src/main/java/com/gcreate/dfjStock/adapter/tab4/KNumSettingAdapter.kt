package com.gcreate.dfjStock.adapter.tab4

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.FragmentActivity
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.gcreate.dfjStock.R
import com.gcreate.dfjStock.model.ObjectKNumModel
import com.gcreate.dfjStock.view.dialog.ModifyKNum2DialogFragment
import kotlinx.android.synthetic.main.card_knum_item.view.*

class KNumSettingAdapter(data: MutableList<ObjectKNumModel>) :
    BaseQuickAdapter<ObjectKNumModel, BaseViewHolder>(R.layout.card_knum_item, data) {

    override fun convert(holder: BaseViewHolder, item: ObjectKNumModel) {
        holder.setText(R.id.tv_KNum_Type, item.kNumType)
        holder.setText(R.id.tv_KNum_Value, item.kNumValue.toString())

        holder.itemView.tv_KNum_Value.setOnClickListener {

            val modifyKNumDialogFragment = ModifyKNum2DialogFragment()
            val bundle = Bundle()
            bundle.putInt("KNumListPosition", getItemPosition(item))
            modifyKNumDialogFragment.arguments = bundle
            modifyKNumDialogFragment.show( (holder.itemView.context as FragmentActivity).supportFragmentManager, "simple dialog")
        }
    }
}

