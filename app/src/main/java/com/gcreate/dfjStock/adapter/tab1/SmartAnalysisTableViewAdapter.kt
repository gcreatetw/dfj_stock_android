package com.gcreate.dfjStock.adapter.tab1

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.evrencoskun.tableview.adapter.AbstractTableAdapter
import com.evrencoskun.tableview.adapter.recyclerview.holder.AbstractViewHolder
import com.gcreate.dfjStock.R
import com.gcreate.dfjStock.model.tableView.Cell
import com.gcreate.dfjStock.model.tableView.ColumnHeader
import com.gcreate.dfjStock.model.tableView.RowHeader

class SmartAnalysisTableViewAdapter(private val stockSymbol: String) : AbstractTableAdapter<ColumnHeader?, RowHeader?, Cell?>() {

    /**
     * This is sample CellViewHolder class
     * This viewHolder must be extended from AbstractViewHolder class instead of RecyclerView.ViewHolder.
     */
    internal class DataCellViewHolder(itemView: View) : AbstractViewHolder(itemView) {
        val cellContainer: LinearLayout = itemView.findViewById(R.id.cell_container)
        val cellTextview: TextView = itemView.findViewById(R.id.cell_data)
    }

    /**
     * This is where you create your custom Cell ViewHolder. This method is called when Cell
     * RecyclerView of the TableView needs a new RecyclerView.ViewHolder of the given type to
     * represent an item.
     *
     * @param viewType : This value comes from #getCellItemViewType method to support different type
     * of viewHolder as a Cell item.
     * @see .getCellItemViewType
     */
    override fun onCreateCellViewHolder(parent: ViewGroup, viewType: Int): AbstractViewHolder {
        // Get cell xml layout
        val layout = LayoutInflater.from(parent.context)
            .inflate(R.layout.table_view_cell_layout, parent, false)
        // Create a Custom ViewHolder for a Cell item.
        return DataCellViewHolder(layout)
    }

    /**
     * That is where you set Cell View Model data to your custom Cell ViewHolder. This method is
     * Called by Cell RecyclerView of the TableView to display the data at the specified position.
     * This method gives you everything you need about a cell item.
     *
     * @param holder         : This is one of your cell ViewHolders that was created on
     * ```onCreateCellViewHolder``` method. In this example, we have created
     * "MyCellViewHolder" holder.
     * @param cellItemModel  : This is the cell view model located on this X and Y position. In this
     * example, the model class is "Cell".
     * @param columnPosition : This is the X (Column) position of the cell item.
     * @param rowPosition    : This is the Y (Row) position of the cell item.
     * @see .onCreateCellViewHolder
     */
    override fun onBindCellViewHolder(holder: AbstractViewHolder, cellItemModel: Cell?, columnPosition: Int, rowPosition: Int) {

        // Get the holder to update cell item text
        val viewHolder = holder as DataCellViewHolder
        viewHolder.cellTextview.text = cellItemModel!!.data

        when (cellItemModel.data) {
            "漲" -> viewHolder.cellTextview.setTextColor(ContextCompat.getColor(holder.itemView.context, R.color.RedDC0000))
            "跌" -> viewHolder.cellTextview.setTextColor(ContextCompat.getColor(holder.itemView.context, R.color.Green00E600))
            "整理" -> viewHolder.cellTextview.setTextColor(ContextCompat.getColor(holder.itemView.context, R.color.YellowFED804))
            else -> viewHolder.cellTextview.setTextColor(ContextCompat.getColor(holder.itemView.context, R.color.WhiteFFFFFF))
        }

        // If your TableView should have auto resize for cells & columns.
        // Then you should consider the below lines. Otherwise, you can ignore them.

        // It is necessary to remeasure itself.
        viewHolder.cellContainer.layoutParams.width = LinearLayout.LayoutParams.WRAP_CONTENT
        viewHolder.cellTextview.requestLayout()
    }

    //========================================== ColumnHeader =====================================================================
    /**
     * This is sample ColumnHeaderViewHolder class.
     * This viewHolder must be extended from AbstractViewHolder class instead of RecyclerView.ViewHolder.
     */
    internal class ColumnHeaderCellViewHolder(itemView: View) : AbstractViewHolder(itemView) {
        val columnHeaderContainer: LinearLayout = itemView.findViewById(R.id.column_header_container)
        val columnHeaderTextview: TextView = itemView.findViewById(R.id.column_header_textView)
    }

    /**
     * This is where you create your custom Column Header ViewHolder. This method is called when
     * Column Header RecyclerView of the TableView needs a new RecyclerView.ViewHolder of the given
     * type to represent an item.
     *
     * @param viewType : This value comes from "getColumnHeaderItemViewType" method to support
     * different type of viewHolder as a Column Header item.
     * @see .getColumnHeaderItemViewType
     */
    override fun onCreateColumnHeaderViewHolder(parent: ViewGroup, viewType: Int): AbstractViewHolder {
        // Get Column Header xml Layout
        val layout = LayoutInflater.from(parent.context).inflate(R.layout.table_view_column_header_layout, parent, false)

        // Create a ColumnHeader ViewHolder
        return ColumnHeaderCellViewHolder(layout)
    }

    /**
     * That is where you set Column Header View Model data to your custom Column Header ViewHolder.
     * This method is Called by ColumnHeader RecyclerView of the TableView to display the data at
     * the specified position. This method gives you everything you need about a column header
     * item.
     *
     * @param holder                : This is one of your column header ViewHolders that was created on
     * ```onCreateColumnHeaderViewHolder``` method. In this example we have created
     * "MyColumnHeaderViewHolder" holder.
     * @param columnHeaderItemModel : This is the column header view model located on this X position. In this
     * example, the model class is "ColumnHeader".
     * @param position              : This is the X (Column) position of the column header item.
     * @see .onCreateColumnHeaderViewHolder
     */
    override fun onBindColumnHeaderViewHolder(holder: AbstractViewHolder, columnHeaderItemModel: ColumnHeader?, position: Int) {

        // Get the holder to update cell item text
        val columnHeaderViewHolder = holder as ColumnHeaderCellViewHolder
        assert(columnHeaderItemModel != null)
        columnHeaderViewHolder.columnHeaderTextview.text = columnHeaderItemModel!!.data.toString()

        // If your TableView should have auto resize for cells & columns.
        // Then you should consider the below lines. Otherwise, you can ignore them.

        // It is necessary to remeasure itself.
        columnHeaderViewHolder.columnHeaderContainer.layoutParams.width = LinearLayout.LayoutParams.WRAP_CONTENT
        columnHeaderViewHolder.columnHeaderTextview.requestLayout()
    }

    //========================================== RowHeader =====================================================================
    /**
     * This is sample RowHeaderViewHolder class.
     * This viewHolder must be extended from AbstractViewHolder class instead of RecyclerView.ViewHolder.
     */
    internal class RowHeaderCellViewHolder(itemView: View) : AbstractViewHolder(itemView) {
        //  val cell_textview: TextView = itemView.findViewById(R.id.cell_data)
        //  val row_header_container: LinearLayout = itemView.findViewById(R.id.row_header_container)
        val rowHeaderTextview: TextView = itemView.findViewById(R.id.row_header_textView)

    }

    /**
     * This is where you create your custom Row Header ViewHolder. This method is called when
     * Row Header RecyclerView of the TableView needs a new RecyclerView.ViewHolder of the given
     * type to represent an item.
     *
     * @param viewType : This value comes from "getRowHeaderItemViewType" method to support
     * different type of viewHolder as a row Header item.
     * @see .getRowHeaderItemViewType
     */
    override fun onCreateRowHeaderViewHolder(parent: ViewGroup, viewType: Int): AbstractViewHolder {
        // Get Row Header xml Layout
        val layout = LayoutInflater.from(parent.context).inflate(R.layout.table_view_row_header_layout, parent, false)

        // Create a Row Header ViewHolder
        return RowHeaderCellViewHolder(layout)
    }

    /**
     * That is where you set Row Header View Model data to your custom Row Header ViewHolder. This
     * method is Called by RowHeader RecyclerView of the TableView to display the data at the
     * specified position. This method gives you everything you need about a row header item.
     *
     * @param holder             : This is one of your row header ViewHolders that was created on
     * ```onCreateRowHeaderViewHolder``` method. In this example, we have created
     * "MyRowHeaderViewHolder" holder.
     * @param rowHeaderItemModel : This is the row header view model located on this Y position. In this
     * example, the model class is "RowHeader".
     * @param position           : This is the Y (row) position of the row header item.
     * @see .onCreateRowHeaderViewHolder
     */
    override fun onBindRowHeaderViewHolder(holder: AbstractViewHolder, rowHeaderItemModel: RowHeader?, position: Int) {

        // Get the holder to update row header item text
        val rowHeaderViewHolder = holder as RowHeaderCellViewHolder
        rowHeaderViewHolder.rowHeaderTextview.text = rowHeaderItemModel!!.data
    }

    //===============================================================================================================
    override fun onCreateCornerView(parent: ViewGroup): View {
        // Get Corner xml layout
        val view = LayoutInflater.from(parent.context).inflate(R.layout.table_view_corner_layout, parent, false)

        // coner set stock symbol
        view.findViewById<TextView>(R.id.tv_corner).text = ""
        //view.findViewById<TextView>(R.id.tv_corner).text = stockSymbol

        return view
    }

    override fun getColumnHeaderItemViewType(columnPosition: Int): Int {
        // The unique ID for this type of column header item
        // If you have different items for Cell View by X (Column) position,
        // then you should fill this method to be able create different
        // type of ColumnViewHolder on "onCreateColumnViewHolder"
        return 0
    }

    override fun getRowHeaderItemViewType(rowPosition: Int): Int {
        // The unique ID for this type of row header item
        // If you have different items for Row Header View by Y (Row) position,
        // then you should fill this method to be able create different
        // type of RowHeaderViewHolder on "onCreateRowHeaderViewHolder"
        return 0
    }

    override fun getCellItemViewType(columnPosition: Int): Int {
        // The unique ID for this type of cell item
        // If you have different items for Cell View by X (Column) position,
        // then you should fill this method to be able create different
        // type of CellViewHolder on "onCreateCellViewHolder"
        return 0
    }
}