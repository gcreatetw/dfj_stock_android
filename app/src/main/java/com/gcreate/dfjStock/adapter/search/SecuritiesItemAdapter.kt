package com.gcreate.dfjStock.adapter.search

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.gcreate.dfjStock.R
import com.gcreate.dfjStock.webAPI.tab2.ApiObjectAllStockClassification

class SecuritiesItemAdapter(data: MutableList<ApiObjectAllStockClassification.ItemsBean.SecuritiesClassificationItemsBean>?) :
    BaseQuickAdapter<ApiObjectAllStockClassification.ItemsBean.SecuritiesClassificationItemsBean, BaseViewHolder>(R.layout.card_verticallist_item,
        data) {

    override fun convert(holder: BaseViewHolder, item: ApiObjectAllStockClassification.ItemsBean.SecuritiesClassificationItemsBean) {
        holder.setText(R.id.tv_listItemName, item.description)
    }

}