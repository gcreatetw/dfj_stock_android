package com.gcreate.dfjStock.adapter.tab1

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.gcreate.dfjStock.R
import kotlinx.android.synthetic.main.card_stock_klinegraph_item.view.*

class StockDetailKIlineGraphTypeAdapter(data: MutableList<String>) :
    BaseQuickAdapter<String, BaseViewHolder>(R.layout.card_stock_klinegraph_item, data) {

    override fun convert(holder: BaseViewHolder, item: String) {

        if (getItemPosition(item) == 0) {
            holder.itemView.tv_stock_DetailCategory.setBackgroundResource(R.drawable.solid_r10_orange_f63e02)
        }
        holder.setText(R.id.tv_stock_DetailCategory, item)
    }

}
