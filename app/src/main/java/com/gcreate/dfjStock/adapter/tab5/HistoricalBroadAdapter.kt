package com.gcreate.dfjStock.adapter.tab5

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.gcreate.dfjStock.R
import com.gcreate.dfjStock.model.ObjectHistoricalBroad

class HistoricalBroadAdapter(private val objectHistoricalBroads: List<ObjectHistoricalBroad>?) :
    RecyclerView.Adapter<HistoricalBroadAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.card_histoical_broad_item, parent, false)
        v.setBackgroundColor(parent.context.resources.getColor(android.R.color.transparent))
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.title.text = objectHistoricalBroads!![position].title
        holder.subTitle.text = objectHistoricalBroads[position].content
        holder.time.text = objectHistoricalBroads[position].time
    }

    override fun getItemCount(): Int {
        return objectHistoricalBroads?.size ?: 0
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title: TextView = itemView.findViewById(R.id.tv_historicalBroadcastTitle)
        val subTitle: TextView = itemView.findViewById(R.id.tv_historicalBroadcastSubTitle)
        val time: TextView = itemView.findViewById(R.id.tv_historicalBroadcastTime)

    }
}