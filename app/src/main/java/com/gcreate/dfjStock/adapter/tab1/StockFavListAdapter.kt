package com.gcreate.dfjStock.adapter.tab1

import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.chad.library.adapter.base.BaseQuickAdapter
import com.gcreate.dfjStock.Listener.ViewControlListenerManager
import com.gcreate.dfjStock.R
import com.gcreate.dfjStock.StorageDataController.StockGroupMaintain
import com.gcreate.dfjStock.adapter.DataBindBaseViewHolder
import com.gcreate.dfjStock.databinding.FragmentOptionalStockBinding
import com.gcreate.dfjStock.view.tab1.OptionalStockFragment
import com.gcreate.dfjStock.webAPI.search.CategoryData
import com.google.android.material.snackbar.Snackbar

class StockFavListAdapter(private val binding: FragmentOptionalStockBinding, data: MutableList<CategoryData>) :
    BaseQuickAdapter<CategoryData, DataBindBaseViewHolder>(R.layout.card_stock_favlist_item, data) {

    //    var listener: View.OnClickListener? = null
    private var deletedNameCardPosition = 0
    private var removePos = 0

    private var deletedNameCard: CategoryData? = null
    private var itemsBeanLists2: MutableList<CategoryData>? = data

    override fun convert(holder: DataBindBaseViewHolder, item: CategoryData) {

        when {
            item.priceChange > 0 -> {
                holder.setTextColor(R.id.tv_stock_closing_price, ContextCompat.getColor(holder.itemView.context, R.color.RedDC0000))
            }
            item.priceChange == 0.0 -> {
                holder.setTextColor(R.id.tv_stock_closing_price, ContextCompat.getColor(holder.itemView.context, R.color.YellowFED804))
            }
            item.priceChange < 0 -> {
                holder.setTextColor(R.id.tv_stock_closing_price, ContextCompat.getColor(holder.itemView.context, R.color.Green00E600))
            }
        }

        holder.setText(R.id.tv_stock_id, item.symbol)
        holder.setText(R.id.tv_stock_name, item.description)
        holder.setText(R.id.tv_stock_closing_price, item.price.toString())

        setLongLineGraph(holder.getView(R.id.tv_stock_LongLine) as TextView, item.states.day20)
        setMiddleLineGraph(holder.getView(R.id.tv_stock_middleLine) as TextView, item.states.day5)
        setShortLineLineGraph(holder.getView(R.id.tv_stock_shortLine) as TextView, item.states.day1)
        setShortestLineGraph(holder.getView(R.id.tv_stock_shortestLine) as TextView, item.states.min30)

    }

    private fun setLongLineGraph(view: TextView, graphStyle: Int) {
        setState(view, graphStyle)
    }

    private fun setMiddleLineGraph(view: TextView, graphStyle: Int) {
        setState(view, graphStyle)
    }

    private fun setShortLineLineGraph(view: TextView, graphStyle: Int) {
        setState(view, graphStyle)
    }

    private fun setShortestLineGraph(view: TextView, graphStyle: Int) {
        setState(view, graphStyle)
    }

    private fun setState(view: TextView, graphStyle: Int) {
        when (graphStyle) {
            0 -> view.setCompoundDrawablesWithIntrinsicBounds(null,
                null,
                null,
                ContextCompat.getDrawable(view.context, R.drawable.icon_triangle_inverted_green))
            1 -> view.setCompoundDrawablesWithIntrinsicBounds(null, null, null, ContextCompat.getDrawable(view.context, R.drawable.icon_triangle_red))
            2 -> view.setCompoundDrawablesWithIntrinsicBounds(null,
                null,
                null,
                ContextCompat.getDrawable(view.context, R.drawable.icon_square_yellow))
        }
    }

    fun refresh(itemsBeanLists: MutableList<CategoryData>?) {
        itemsBeanLists2 = itemsBeanLists
        notifyDataSetChanged()
        ViewControlListenerManager.getInstance().sendBroadCast(itemsBeanLists2!!.size == 0)
    }

    fun deleteItem(position: Int) {
        Log.d("ben","position = $position")
        deletedNameCard = itemsBeanLists2!![position]
        deletedNameCardPosition = position
        notifyItemRemoved(position)
        itemsBeanLists2!!.removeAt(position)
        StockGroupMaintain.removeStockFromOneGroup(OptionalStockFragment.favGroupSelectedPosition, position)
        refresh(itemsBeanLists2)
        showUndoSnackBar(position)
        removePos = position
    }

    private fun showUndoSnackBar(position: Int) {
        val snackbar = Snackbar.make(binding.swipeRefresh, "已刪除", Snackbar.LENGTH_LONG)
        snackbar.setAction("還原") { v: View? -> unDeleteItem(position) }
        snackbar.show()
    }

    private fun unDeleteItem(position: Int) {
        notifyItemInserted(deletedNameCardPosition)
        itemsBeanLists2!!.add(position, deletedNameCard!!)
        StockGroupMaintain.insertStockToGroup(OptionalStockFragment.favGroupSelectedPosition, removePos, deletedNameCard!!.id)
        refresh(itemsBeanLists2)
    }



}