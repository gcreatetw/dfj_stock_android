package com.gcreate.dfjStock.adapter

import android.annotation.SuppressLint
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.gcreate.dfjStock.R
import com.gcreate.dfjStock.webAPI.login.StockGroup

class StockDetailSelectGroupAdapter(private val favStockGroupList: MutableList<StockGroup>?) :
    RecyclerView.Adapter<StockDetailSelectGroupAdapter.ViewHolder>() {

    private var onItemClickListener: OnItemClickListener? = null

    interface OnItemClickListener {
        fun onItemClick(view: View?, position: Int)
    }

    fun setOnItemClickListener(listener: OnItemClickListener?) {
        onItemClickListener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.card_stock_group_item, parent, false)
        return ViewHolder(v)
    }


    override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {
        if (position == selectPosition) {
            holder.itemView.setBackgroundColor(Color.parseColor("#DC0000"))
            holder.groupName.setTextColor(Color.parseColor("#FFFFFF"))
        } else {
            holder.stockGruop.setCardBackgroundColor(Color.parseColor("#FFFFFF"))
            holder.groupName.setTextColor(Color.parseColor("#222222"))
        }
        holder.groupName.text = favStockGroupList!![position].group_name
        holder.itemView.setOnClickListener {
            selectPosition = position
            onItemClickListener!!.onItemClick(holder.itemView, position)
            notifyDataSetChanged()
        }
    }

    override fun getItemCount(): Int {
        return favStockGroupList?.size ?: 0
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val stockGruop: CardView = itemView.findViewById(R.id.cardView_favStockGroup)
        val groupName: TextView = itemView.findViewById(R.id.tv_favStockGroupName)

    }

    companion object {
        private var selectPosition = 0
    }
}