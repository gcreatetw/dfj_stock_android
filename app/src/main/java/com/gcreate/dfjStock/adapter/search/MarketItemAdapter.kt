package com.gcreate.dfjStock.adapter.search

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.gcreate.dfjStock.R
import com.gcreate.dfjStock.webAPI.search.MarketCategory


class MarketItemAdapter(data: MutableList<MarketCategory>?) :
    BaseQuickAdapter<MarketCategory, BaseViewHolder>(R.layout.card_verticallist_item, data) {

    override fun convert(holder: BaseViewHolder, item: MarketCategory) {
        holder.setText(R.id.tv_listItemName, item.description)
    }
}