package com.gcreate.dfjStock.adapter.tab2.expandView

import com.chad.library.adapter.base.entity.node.BaseExpandNode
import com.chad.library.adapter.base.entity.node.BaseNode

/**
 * 第二个节点SecondNode，里面没有子节点了
 */
class SecondNode(val stockId : Int,val stockSymbol: String, val stockName: String, val priceChange: Double,val price : Double,
                 val _20DayState: Int ,val _5DayState: Int,val _1DayState: Int,val _30MinState: Int) : BaseExpandNode() {

    /**
     * 重写此方法，返回子节点
     */
    override val childNode: MutableList<BaseNode>?
        get() = null
}