package com.gcreate.dfjStock.adapter.tab2

import android.widget.TextView
import androidx.core.content.ContextCompat
import com.chad.library.adapter.base.BaseQuickAdapter
import com.gcreate.dfjStock.R
import com.gcreate.dfjStock.adapter.DataBindBaseViewHolder
import com.gcreate.dfjStock.webAPI.tab2.SuggestedConditionResultData

class DfjSuggestedTodayResultAdapter(data: MutableList<SuggestedConditionResultData>) :
    BaseQuickAdapter<SuggestedConditionResultData, DataBindBaseViewHolder>(R.layout.card_stock_favlist_item, data) {

    override fun convert(holder: DataBindBaseViewHolder, item: SuggestedConditionResultData) {

        when {
            item.priceChange > 0 -> {
                holder.setTextColor(R.id.tv_stock_closing_price, ContextCompat.getColor(holder.itemView.context, R.color.RedDC0000))
            }
            item.priceChange == 0.0 -> {
                holder.setTextColor(R.id.tv_stock_closing_price, ContextCompat.getColor(holder.itemView.context, R.color.YellowFED804))
            }
            item.priceChange < 0 -> {
                holder.setTextColor(R.id.tv_stock_closing_price, ContextCompat.getColor(holder.itemView.context, R.color.Green00E600))
            }
        }

        holder.setText(R.id.tv_stock_id, item.symbol)
        holder.setText(R.id.tv_stock_name, item.description)
        holder.setText(R.id.tv_stock_closing_price, item.price.toString())

        setLongLineGraph(holder.getView(R.id.tv_stock_LongLine) as TextView, item.states.day20)
        setMiddleLineGraph(holder.getView(R.id.tv_stock_middleLine) as TextView, item.states.day5)
        setShortLineLineGraph(holder.getView(R.id.tv_stock_shortLine) as TextView, item.states.day1)
        setShortestLineGraph(holder.getView(R.id.tv_stock_shortestLine) as TextView, item.states.min30)
    }

    private fun setLongLineGraph(view: TextView, graphStyle: Int) {
        setState(view, graphStyle)
    }

    private fun setMiddleLineGraph(view: TextView, graphStyle: Int) {
        setState(view, graphStyle)
    }

    private fun setShortLineLineGraph(view: TextView, graphStyle: Int) {
        setState(view, graphStyle)
    }

    private fun setShortestLineGraph(view: TextView, graphStyle: Int) {
        setState(view, graphStyle)
    }

    private fun setState(view: TextView, graphStyle: Int) {
        when (graphStyle) {
            0 -> view.setCompoundDrawablesWithIntrinsicBounds(null,
                null,
                null,
                ContextCompat.getDrawable(view.context, R.drawable.icon_triangle_inverted_green))
            1 -> view.setCompoundDrawablesWithIntrinsicBounds(null, null, null, ContextCompat.getDrawable(view.context, R.drawable.icon_triangle_red))
            2 -> view.setCompoundDrawablesWithIntrinsicBounds(null,
                null,
                null,
                ContextCompat.getDrawable(view.context, R.drawable.icon_square_yellow))
        }
    }
}