package com.gcreate.dfjStock.adapter.tab1

import android.opengl.Visibility
import android.view.View
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.gcreate.dfjStock.R
import kotlinx.android.synthetic.main.card_stock_graph_item.view.*


class StockDetailGraphTypeAdapter(data: MutableList<String>) : BaseQuickAdapter<String, BaseViewHolder>(R.layout.card_stock_graph_item, data) {

    override fun convert(holder: BaseViewHolder, item: String) {

        if(getItemPosition(item) == 0){
            holder.itemView.img_underLine.visibility = View.VISIBLE
        }

        holder.setText(R.id.tv_stock_DetailCategory, item)
    }

}
