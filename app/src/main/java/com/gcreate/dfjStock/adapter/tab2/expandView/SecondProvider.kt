package com.gcreate.dfjStock.adapter.tab2.expandView

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.navigation.NavController
import com.chad.library.adapter.base.entity.node.BaseNode
import com.chad.library.adapter.base.provider.BaseNodeProvider
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.gcreate.dfjStock.R

class SecondProvider(private val navController: NavController) : BaseNodeProvider() {

    override val itemViewType: Int
        get() = 2

    override val layoutId: Int
        get() = R.layout.card_stock_favlist_item

    override fun convert(helper: BaseViewHolder, item: BaseNode) {
        val entity = item as SecondNode
        helper.setText(R.id.tv_stock_id, entity.stockSymbol)
        helper.setText(R.id.tv_stock_name, entity.stockName)

        val priceChange = item.priceChange
        when {
            priceChange > 0 -> helper.setTextColor(R.id.tv_stock_closing_price, ContextCompat.getColor(helper.itemView.context, R.color.RedDC0000))
            priceChange == 0.0 -> helper.setTextColor(R.id.tv_stock_closing_price,
                ContextCompat.getColor(helper.itemView.context, R.color.YellowFED804))
            priceChange < 0 -> helper.setTextColor(R.id.tv_stock_closing_price, ContextCompat.getColor(helper.itemView.context, R.color.Green00E600))
        }

        helper.setText(R.id.tv_stock_closing_price, entity.price.toString())
        setLongLineGraph(helper.getView(R.id.tv_stock_LongLine), item._20DayState)
        setMiddleLineGraph(helper.getView(R.id.tv_stock_middleLine), item._5DayState)
        setShortLineLineGraph(helper.getView(R.id.tv_stock_shortLine), item._1DayState)
        setShortestLineGraph(helper.getView(R.id.tv_stock_shortestLine), item._30MinState)

    }

    override fun onClick(helper: BaseViewHolder, view: View, data: BaseNode, position: Int) {
        val entity = data as SecondNode

        val intentExtraArgs = Bundle()
        intentExtraArgs.putInt("stockID", data.stockId)
        intentExtraArgs.putString("stockSymbol", data.stockSymbol)
        intentExtraArgs.putString("stockName", data.stockName)

        navController.navigate(R.id.action_fragment_DfjSuggestedGroupHistoricalResult_to_fragment_StockDetail, intentExtraArgs)
    }


    private fun setLongLineGraph(view: TextView, graphStyle: Int) {
        when (graphStyle) {
            0 -> view.setCompoundDrawablesWithIntrinsicBounds(null,
                null,
                null,
                ContextCompat.getDrawable(view.context, R.drawable.icon_triangle_inverted_green))

            1 -> view.setCompoundDrawablesWithIntrinsicBounds(null,
                null,
                null,
                ContextCompat.getDrawable(view.context, R.drawable.icon_triangle_red))

            2 -> view.setCompoundDrawablesWithIntrinsicBounds(null,
                null,
                null,
                ContextCompat.getDrawable(view.context, R.drawable.icon_square_yellow))
        }
    }

    private fun setMiddleLineGraph(view: TextView, graphStyle: Int) {
        when (graphStyle) {

            0 -> view.setCompoundDrawablesWithIntrinsicBounds(null,
                null,
                null,
                ContextCompat.getDrawable(view.context, R.drawable.icon_triangle_inverted_green))

            1 -> view.setCompoundDrawablesWithIntrinsicBounds(null,
                null,
                null,
                ContextCompat.getDrawable(view.context, R.drawable.icon_triangle_red))

            2 -> view.setCompoundDrawablesWithIntrinsicBounds(null,
                null,
                null,
                ContextCompat.getDrawable(view.context, R.drawable.icon_square_yellow))
        }
    }

    private fun setShortLineLineGraph(view: TextView, graphStyle: Int) {
        when (graphStyle) {
            0 -> view.setCompoundDrawablesWithIntrinsicBounds(null,
                null,
                null,
                ContextCompat.getDrawable(view.context, R.drawable.icon_triangle_inverted_green))

            1 -> view.setCompoundDrawablesWithIntrinsicBounds(null,
                null,
                null,
                ContextCompat.getDrawable(view.context, R.drawable.icon_triangle_red))

            2 -> view.setCompoundDrawablesWithIntrinsicBounds(null,
                null,
                null,
                ContextCompat.getDrawable(view.context, R.drawable.icon_square_yellow))
        }
    }

    private fun setShortestLineGraph(view: TextView, graphStyle: Int) {
        when (graphStyle) {
            0 -> view.setCompoundDrawablesWithIntrinsicBounds(null,
                null,
                null,
                ContextCompat.getDrawable(view.context, R.drawable.icon_triangle_inverted_green))

            1 -> view.setCompoundDrawablesWithIntrinsicBounds(null,
                null,
                null,
                ContextCompat.getDrawable(view.context, R.drawable.icon_triangle_red))

            2 -> view.setCompoundDrawablesWithIntrinsicBounds(null,
                null,
                null,
                ContextCompat.getDrawable(view.context, R.drawable.icon_square_yellow))
        }
    }
}