package com.gcreate.dfjStock

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.pm.ActivityInfo
import android.content.res.Configuration
import android.graphics.BitmapFactory
import android.graphics.Color
import android.media.RingtoneManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import com.gcreate.dfjStock.Listener.KViewOrientationListenerManager
import com.gcreate.dfjStock.StorageDataController.KChartLevelMaintain
import com.gcreate.dfjStock.StorageDataController.KNumGroupMaintain
import com.gcreate.dfjStock.StorageDataController.StockGroupMaintain
import com.gcreate.dfjStock.databinding.ActivityMainBinding
import com.gcreate.dfjStock.view.WelcomePageActivity.Companion.mContext
import com.gcreate.dfjStock.view.dialog.VersionCheckDialogFragment
import com.gcreate.dfjStock.view.tab1.StockDetailFragment
import com.gcreate.dfjStock.webAPI.RetrofitInstance
import com.gcreate.dfjStock.webAPI.search.ApiObjectAllStockCategories
import com.gcreate.dfjStock.webAPI.search.ApiObjectSearchResult
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.messaging.FirebaseMessaging
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class MainActivity : AppCompatActivity() {


    private var bottomNavigationView: BottomNavigationView? = null
    private lateinit var binding: ActivityMainBinding

    companion object {

        private var currentFragmentName: String? = null

        @JvmField
        var objectAllStockCategories: ApiObjectAllStockCategories? = null

        @JvmField
        var objectAllStocks: ApiObjectSearchResult? = null

        var currentActivity = ""

        @JvmName("setCurrentActivity1")
        fun setCurrentActivity(currentActivity: String) {
            this.currentActivity = currentActivity
        }

        fun sendNotification(context: Context, messageTitle: String?, messageBody: String?) {
            val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            val channelId = "default_notification_channel_id"
            val channelDescription = "Others"
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                var notificationChannel = notificationManager.getNotificationChannel(channelId)
                if (notificationChannel == null) {
                    val importance = NotificationManager.IMPORTANCE_HIGH //Set the importance level
                    notificationChannel = NotificationChannel(channelId, channelDescription, importance)
                    notificationChannel.lightColor = Color.GREEN //Set if it is necesssary
                    notificationChannel.enableVibration(true) //Set if it is necesssary
                    notificationManager.createNotificationChannel(notificationChannel)
                }
            }

            val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
            val notificationBuilder: NotificationCompat.Builder =
                // in app catch notify
                NotificationCompat.Builder(context, channelId)
                    .setSmallIcon(R.mipmap.logo_dfj)
                    .setColor(ContextCompat.getColor(context, R.color.chart_red))
                    .setLargeIcon(BitmapFactory.decodeResource(context.resources, R.mipmap.logo_dfj))
                    .setContentTitle(messageTitle)
                    .setContentText(messageBody)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setChannelId(channelId)

            if (messageBody!!.split("，").toTypedArray().isNotEmpty()) {
                notificationBuilder.setContentTitle(messageTitle)
            }
            notificationManager.notify(0 /* ID of notification */, notificationBuilder.build())
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        bottomNavigationView = binding.bottomNavigationView
        currentActivity = this.localClassName

        initView()

        val versionCheckResult = intent.getBooleanExtra("versionCheckResult", true)

        if (!versionCheckResult) {
            val versionCheckDialogFragment: DialogFragment = VersionCheckDialogFragment()
            versionCheckDialogFragment.show(supportFragmentManager, "simple dialog")
        }

        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                return@OnCompleteListener
            }

            // Get new FCM registration token
            val token = task.result
            Log.d("ben000", "token = $token")
        })

        val navController = Navigation.findNavController(this, R.id.nav_host_fragment)
        NavigationUI.setupWithNavController(binding.bottomNavigationView, navController)

    }

    // -----------------------------------------------------------------Some function----------------------------------------------------------------------
    private fun initView() {
        settingWindowStatusBar()
        allStockCategories()
        getAllStock
        //-------------------------------------    Init FavStockGroupList      --------------------------------
        if (global.getIsFirstTimeOpenApp(mContext)) {
            StockGroupMaintain.initFavStockGroupList(this)
            KNumGroupMaintain.initKNumGroupList(this)
            KChartLevelMaintain.setKChartTrendLineLevel(this, 1)
            global.setIsFirstTimeOpenApp(mContext, false)
        } else {
            StockGroupMaintain.getFavStockGroupList(this)
        }

    }

    private fun settingWindowStatusBar() {
        global.getWindowSize(this)
        global.setWindowStatusBarColor(this, R.color.RedDC0000)
    }

    // ------------------------------------------------preload Api data   -----------------------------------------------
    private fun allStockCategories() {
        if (objectAllStockCategories == null) {

            RetrofitInstance.getDfjApiInstance2().allStockCategories.enqueue(object : Callback<ApiObjectAllStockCategories> {
                override fun onResponse(call: Call<ApiObjectAllStockCategories>, response: Response<ApiObjectAllStockCategories>) {

                    if (response.isSuccessful) {
                        objectAllStockCategories = response.body()
                    }

                }

                override fun onFailure(call: Call<ApiObjectAllStockCategories>, t: Throwable) {
                    binding.loadingProgress.visibility = View.INVISIBLE
                    Toast.makeText(this@MainActivity, "連線失敗，請檢察網際網路", Toast.LENGTH_SHORT).show()
                }
            })
        }
    }

    private val getAllStock: Unit
        get() {
            if (objectAllStocks == null) {
                binding.loadingProgress.visibility = View.VISIBLE
                RetrofitInstance.getDfjApiInstance2().allStocks.enqueue(object : Callback<ApiObjectSearchResult> {
                    override fun onResponse(call: Call<ApiObjectSearchResult>, response: Response<ApiObjectSearchResult>) {
                        binding.loadingProgress.visibility = View.INVISIBLE
                        if (response.isSuccessful) {
                            objectAllStocks = response.body()
                            runBlocking {
                                delay(1000L)
                                binding.loadingProgress.visibility = View.GONE
                            }
                        }
                    }

                    override fun onFailure(call: Call<ApiObjectSearchResult>, t: Throwable) {
                        binding.loadingProgress.visibility = View.INVISIBLE
                    }
                })
            }
        }

    // ----------------------------複寫監聽手機功能鍵 function----------------------------------------------------------------------
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            onBackPressed()
            return true
        }
        return if (KeyEvent.KEYCODE_HOME == keyCode) {
            true
        } else super.onKeyDown(keyCode, event)
    }

//    override fun onBackPressed() {
//        val fragmentManager = supportFragmentManager
//        if (fragmentManager.backStackEntryCount > 1) {
//            fragmentManager.popBackStackImmediate()
//            if (fragmentManager.fragments[0].toString().startsWith("OptionalStockFragment")) {
//                currentFragmentName = "OptionalStockFragment"
//                BottomNavigationView!!.menu.findItem(R.id.item1).isChecked = true
//            } else if (fragmentManager.fragments[0].toString().startsWith("DfjSuggestedGroupFragment")) {
//                currentFragmentName = "DfjSuggestedGroupFragment"
//                BottomNavigationView!!.menu.findItem(R.id.item2).isChecked = true
//            } else if (fragmentManager.fragments[0].toString().startsWith("Fragment_Taiwan_Index")) {
//                currentFragmentName = "Fragment_Taiwan_Index"
//                BottomNavigationView!!.menu.findItem(R.id.item3).isChecked = true
//            } else if (fragmentManager.fragments[0].toString().startsWith("OtherInfoFragment")) {
//                currentFragmentName = "OtherInfoFragment"
//                BottomNavigationView!!.menu.findItem(R.id.item4).isChecked = true
//            } else {
//                currentFragmentName = ""
//            }
//        } else {
//            finish()
//        }
//    }

    // ----------------------------橫屏不重新加載activity，調用該方法    ----------------------------------------------------------------------
    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        val c = resources.configuration

        if (StockDetailFragment.currentStockDetailFragment == "Fragment_StockDetail_KChart") {
            requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_SENSOR
            if (c.orientation == Configuration.ORIENTATION_PORTRAIT && currentActivity == "MainActivity") {
                // portrait
                StockDetailFragment.setViewHideOrShow(View.VISIBLE, View.VISIBLE)
                bottomNavigationView!!.visibility = View.VISIBLE
                KViewOrientationListenerManager.getInstance().sendBroadCast("PORTRAIT")

            } else if (c.orientation == Configuration.ORIENTATION_LANDSCAPE) {
                // landscape
                if (currentActivity == "MainActivity") {
                    StockDetailFragment.setViewHideOrShow(View.GONE, View.GONE)
                    bottomNavigationView!!.visibility = View.GONE
                    KViewOrientationListenerManager.getInstance().sendBroadCast("LANDSCAPE")
                }

            }
        } else if (currentFragmentName == "Fragment_Taiwan_Index") {
            requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_SENSOR
            if (c.orientation == Configuration.ORIENTATION_PORTRAIT) {
                // portrait
                findViewById<View>(R.id.cl_toolbar).visibility = View.VISIBLE
                findViewById<View>(R.id.nestedScrollView2).visibility = View.VISIBLE
                findViewById<View>(R.id.cl_taiwanIndexType).visibility = View.VISIBLE
                val constraintLayout = findViewById<ConstraintLayout>(R.id.cv_Kline)
                val constraintSet = ConstraintSet()
                constraintSet.clone(constraintLayout)
                constraintSet.connect(R.id.kchart_view, ConstraintSet.BOTTOM, R.id.KLine_hg1, ConstraintSet.TOP, 0)
                constraintSet.connect(R.id.kchart_view, ConstraintSet.TOP, R.id.rv_klineType, ConstraintSet.BOTTOM, 0)
                constraintSet.applyTo(constraintLayout)
                bottomNavigationView!!.visibility = View.VISIBLE
                KViewOrientationListenerManager.getInstance().sendBroadCast("PORTRAIT")
            } else if (c.orientation == Configuration.ORIENTATION_LANDSCAPE) {
                // landscape
                findViewById<View>(R.id.cl_toolbar).visibility = View.GONE
                findViewById<View>(R.id.nestedScrollView2).visibility = View.GONE
                findViewById<View>(R.id.cl_taiwanIndexType).visibility = View.GONE
                val constraintLayout = findViewById<ConstraintLayout>(R.id.cv_Kline)
                val constraintSet = ConstraintSet()
                constraintSet.clone(constraintLayout)
                constraintSet.connect(R.id.kchart_view, ConstraintSet.BOTTOM, ConstraintSet.PARENT_ID, ConstraintSet.BOTTOM, 0)
                constraintSet.connect(R.id.kchart_view, ConstraintSet.TOP, R.id.rv_klineType, ConstraintSet.BOTTOM, 0)
                constraintSet.applyTo(constraintLayout)
                bottomNavigationView!!.visibility = View.GONE
                KViewOrientationListenerManager.getInstance().sendBroadCast("LANDSCAPE")
            }
        } else {
            requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
            bottomNavigationView!!.visibility = View.VISIBLE
        }
    }
}