package com.gcreate.dfjStock;

import static android.content.Context.MODE_PRIVATE;
import static androidx.core.content.ContextCompat.startActivity;
import static com.gcreate.dfjStock.webAPI.gcAPI.GC_SERVER;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.text.InputFilter;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.gcreate.dfjStock.view.webview.WebViewActivity;
import com.gcreate.dfjStock.webAPI.gcAPI;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class global {
    public static int windowWidth;
    public static int windowHeigh;

    public static gcAPI gcAPI;
    public static String searchStockID;
    public static String searchStockName;


    public static void getWindowSize(Activity mactivity) {
        DisplayMetrics dm = new DisplayMetrics();
        mactivity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        global.windowHeigh = dm.heightPixels;
        global.windowWidth = dm.widthPixels;
    }


    /* Init System setting default value*/
    // Storage User first time open app status
    public static void setIsFirstTimeOpenApp(Context context, boolean mode) {
        SharedPreferences.Editor editor = ((AppCompatActivity) context).getPreferences(MODE_PRIVATE).edit();
        editor.putBoolean("isFirstTimeOpenApp", mode);
        editor.apply();
        editor.commit();
    }

    public static boolean getIsFirstTimeOpenApp(Context context) {
        return ((AppCompatActivity) context).getPreferences(MODE_PRIVATE).getBoolean("isFirstTimeOpenApp", true);
    }


    public static void initGCAPI() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {

                        Request.Builder builder = chain.request().newBuilder();
                        Request build = builder.build();
                        return chain.proceed(build);

                    }
                })
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(GC_SERVER)
                .client(client) //Only for Web API debug
                .build();
        global.gcAPI = retrofit.create(gcAPI.class);

    }

    /**
     * 禁止EditText輸入空格和換行符以及特殊符號
     *
     * @param editText EditText輸入框
     */
    public static void setEditTextInputSpace(EditText editText) {
        InputFilter filter = (source, start, end, dest, dstart, dend) -> {
            String speChat = "[`[-]#$%^&*()+=|{}':;',\\[\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？_＋－＊]";
            Pattern pattern = Pattern.compile(speChat);
            Matcher matcher = pattern.matcher(source.toString());
            if (source.equals(" ") || source.toString().contentEquals("\n") || matcher.find()) {
                return "";
            } else {
                return null;
            }
        };
        editText.setFilters(new InputFilter[]{filter});
    }

    public static void setWindowStatusBarColor(Activity mActivity, int color) {
        Window window = mActivity.getWindow();
        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        // finally change the color
        window.setStatusBarColor(ContextCompat.getColor(mActivity, color));
        WindowManager.LayoutParams lp = mActivity.getWindow().getAttributes();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            lp.layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES;
        }
        window.setAttributes(lp);
    }

    public static void setHideWindowStatusBar(Window window) {

        View decorView = window.getDecorView();
        int option = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE;
        decorView.setSystemUiVisibility(option);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }

    }

    public static void alertDialog(Context context) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setTitle("此功能需先成為VIP會員");
        alertDialogBuilder.setMessage("是否前往東方靖官網");
        alertDialogBuilder.setPositiveButton("確定", (dialog, which) -> {
            Intent intent = new Intent(context, WebViewActivity.class);
            intent.putExtra("DFJ_webUrl", "https://lihi1.com/ZXDMO");
            startActivity(context, intent, null);
        });

        alertDialogBuilder.setNeutralButton("取消", (dialog, which) -> {
            alertDialogBuilder.create().dismiss();
        });
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.show();
    }


}
