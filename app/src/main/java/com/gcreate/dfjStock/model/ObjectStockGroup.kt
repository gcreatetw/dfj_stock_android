package com.gcreate.dfjStock.model

class ObjectStockGroup {
    //public class ObjectStockGroup implements Comparable<ObjectStockGroup> {
    //    @Override
    //    public int compareTo(ObjectStockGroup o) {
    //        //按照对象的年龄进行排序
    //        //主要判断条件: 按照年龄从小到大排序
    //        //當result小于0的时候，就将新存入的数据放在左边，大于0就将新存入的数据放在右边，等于0的时候不进行存取
    //        int result = this.index - o.index;
    //
    //        return result;
    //    }
    var groupName: String
    var groupItemList: List<String>? = null

    constructor(groupName: String) {
        this.groupName = groupName
    }

    constructor(groupName: String, groupItemList: List<String>?) {
        this.groupName = groupName
        this.groupItemList = groupItemList
    }
}