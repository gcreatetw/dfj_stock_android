package com.gcreate.dfjStock.model.tableView

import com.gcreate.dfjStock.webAPI.tab3.ApiObjectTaiwanIndexText
import java.util.*

class TaiwanIndexTableViewModel(private val apiObject: ApiObjectTaiwanIndexText) {

    val cellList: List<List<Cell>>
        get() = cellListForSortingTest

    val rowHeaderList: List<RowHeader>
        get() = simpleRowHeaderList

    val columnHeaderList: List<ColumnHeader>
        get() = randomColumnHeaderList


    // Vertical Row value
    private val simpleRowHeaderList: List<RowHeader>
        get() {
            val list: MutableList<RowHeader> = ArrayList()

            for (i in apiObject.data.meta_value.indices) {
                list.add(RowHeader(i.toString()))
            }

            return list
        }

    // Horizontal Column value
    private val randomColumnHeaderList: List<ColumnHeader>
        get() {
            val columnValue = arrayListOf("時間", "方向", "欠點", "距離", "非當天訊號", "當天訊號", "後續訊號")

            val list: MutableList<ColumnHeader> = ArrayList()

            for (i in columnValue) {
                val header = ColumnHeader(i)
                list.add(header)
            }
            return list
        }

    // Cell data
    private val cellListForSortingTest: List<List<Cell>>
        get() {
            val list: MutableList<List<Cell>> = ArrayList()
            for (i in rowHeaderList.indices) {
                val cellList: MutableList<Cell> = ArrayList()
                for (j in 0 until 7) {
                    /* 資料排序
                    * 1.kDatetime: 時間
                    * 2.direaction: 方向
                    * 3.price: 欠點
                    * 4.distance: 距離
                    * 5.offTheDayMessage: 非當天訊號
                    * 6.inTheDayMessage: 當天訊號
                    * 7.continueMessage: 後續訊號
                    * */
                    when (j) {
                        0 -> cellList.add(Cell(apiObject.data.meta_value[i].kDatetime))
                        1 -> cellList.add(Cell(apiObject.data.meta_value[i].direaction))
                        2 -> cellList.add(Cell(apiObject.data.meta_value[i].price.toString()))
                        3 -> cellList.add(Cell(apiObject.data.meta_value[i].distance.toString()))
                        4 -> cellList.add(Cell(apiObject.data.meta_value[i].offTheDayMessage))
                        5 -> cellList.add(Cell(apiObject.data.meta_value[i].inTheDayMessage))
                        6 -> cellList.add(Cell(apiObject.data.meta_value[i].continueMessage))
                    }
                }
                list.add(cellList)
            }

            list.reverse()

            return list
        }

}