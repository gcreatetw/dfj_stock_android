package com.gcreate.dfjStock.model.tableView

import com.gcreate.dfjStock.webAPI.tab1.ApiObjectStockAnalysisText
import java.util.*

class SmartAnalysisTableViewModel(private val apiObject: ApiObjectStockAnalysisText) {

    val cellList: List<List<Cell>>
        get() = cellListForSortingTest

    val rowHeaderList: List<RowHeader>
        get() = simpleRowHeaderList

    val columnHeaderList: List<ColumnHeader>
        get() = randomColumnHeaderList


    // Vertical Row value
    private val simpleRowHeaderList: List<RowHeader>
        get() {
            val list: MutableList<RowHeader> = ArrayList()
            for (i in 0 until apiObject.data.metaValue.size) {
                when (apiObject.data.metaValue[i].timeInterval) {
                    "day20" -> {
                        val header = RowHeader("長線")
                        list.add(header)
                    }
                    "day5" -> {
                        val header = RowHeader("中線")
                        list.add(header)
                    }
                    "day1" -> {
                        val header = RowHeader("短線")
                        list.add(header)
                    }
                    else -> {
                        val header = RowHeader("極短線")
                        list.add(header)
                    }
                }
            }
            list.reverse()
            return list
        }

    // Horizontal Column value
    private val randomColumnHeaderList: List<ColumnHeader>
        get() {
            val columnValue = arrayListOf("今日狀態", "前次狀態", "狀態區間(單位: 天)",
                "支撐價格", "壓力價格", "支撐數量", "壓力數量", "支撐角度(單位: °)", "壓力角度(單位: °)", "支撐距離(單位: %)", "壓力距離(單位: %)")

            val list: MutableList<ColumnHeader> = ArrayList()

            for (i in columnValue) {
                val header = ColumnHeader(i)
                list.add(header)
            }
            return list
        }

    // Cell data
    private val cellListForSortingTest: List<List<Cell>>
        get() {
            val list: MutableList<List<Cell>> = ArrayList()
            for (i in rowHeaderList.indices) {
                val cellList: MutableList<Cell> = ArrayList()
                for (j in 0 until 11) {
                    /* 資料排序
                    * 1.trendlineNowState: 今日狀態
                    * 2.trendlineBeforeState: 前次狀態
                    * 3.trendlineStateChangeDay: 狀態區間(單位: 天)
                    * 4.supportPrice: 支撐價格
                    * 5.pressurePrice: 壓力價格
                    * 6.supportNumber: 支撐數量
                    * 7.pressureNumber: 壓力數量
                    * 8.supportAngle: 支撐角度(單位: °)
                    * 9.pressureAngle: 壓力角度(單位: °)
                    * 10.supportDistancePercent: 支撐距離(單位: %)
                    * 11.pressureDistancePercent: 壓力距離(單位: %)
                    * */
                    when (j) {
                        0 -> cellList.add(Cell(apiObject.data.metaValue[i].trendlineNowState))
                        1 -> cellList.add(Cell(apiObject.data.metaValue[i].trendlineBeforeState))
                        2 -> cellList.add(Cell(apiObject.data.metaValue[i].trendlineStateChangeDay.toString()))
                        3 -> cellList.add(Cell(apiObject.data.metaValue[i].supportPrice.toString()))
                        4 -> cellList.add(Cell(apiObject.data.metaValue[i].pressurePrice.toString()))
                        5 -> cellList.add(Cell(apiObject.data.metaValue[i].supportNumber.toString()))
                        6 -> cellList.add(Cell(apiObject.data.metaValue[i].pressureNumber.toString()))
                        7 -> cellList.add(Cell(apiObject.data.metaValue[i].supportAngle.toString()))
                        8 -> cellList.add(Cell(apiObject.data.metaValue[i].pressureAngle.toString()))
                        9 -> cellList.add(Cell(apiObject.data.metaValue[i].supportDistancePercent.toString()))
                        10 -> cellList.add(Cell(apiObject.data.metaValue[i].pressureDistancePercent.toString()))
                    }
                }
                list.add(cellList)
            }
            list.reverse()
            return list
        }

}