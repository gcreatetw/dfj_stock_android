package com.gcreate.dfjStock.model

class ObjectStockFavItem(
    var stock_ID: Int, var stock_name: String, var stock_closing_price: Int, var stock_LongLine_graph: Int,
    var stock_middleLine_graph: Int, var stock_shortLine_graph: Int, var stock_shortestLine_graph: Int
)