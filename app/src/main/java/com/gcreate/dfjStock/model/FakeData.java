package com.gcreate.dfjStock.model;

import java.util.ArrayList;
import java.util.List;

public class FakeData {


    public static List<String> KChartGraphType(){

        List<String> graphType = new ArrayList<>();
        graphType.add("量價趨勢線分析");
        graphType.add("智慧分析");
        graphType.add("智慧圖形");

        return graphType;
    }

    public static List<String> KChartLineType(){

        List<String> KChartLineType = new ArrayList<>();
        KChartLineType.add("長線");
        KChartLineType.add("中線");
        KChartLineType.add("短線");
        KChartLineType.add("極短線");

        return KChartLineType;
    }

    public static List<ObjectHistoricalBroad> getHistoricalBroadFakeData(){

        List<ObjectHistoricalBroad> historicalBroadFakeData = new ArrayList<>();
        historicalBroadFakeData.add(new ObjectHistoricalBroad("title1","subTitle1","9分鐘"));
        historicalBroadFakeData.add(new ObjectHistoricalBroad("title2","subTitle2","10分鐘"));
        historicalBroadFakeData.add(new ObjectHistoricalBroad("title3","subTitle3","11分鐘"));
        historicalBroadFakeData.add(new ObjectHistoricalBroad("title4","subTitle4","12分鐘"));
        historicalBroadFakeData.add(new ObjectHistoricalBroad("title5","subTitle5","13分鐘"));
        historicalBroadFakeData.add(new ObjectHistoricalBroad("title6","subTitle6","14分鐘"));
        historicalBroadFakeData.add(new ObjectHistoricalBroad("title7","subTitle7","15分鐘"));
        historicalBroadFakeData.add(new ObjectHistoricalBroad("title8","subTitle8","16分鐘"));
        historicalBroadFakeData.add(new ObjectHistoricalBroad("title9","subTitle9","17分鐘"));
        historicalBroadFakeData.add(new ObjectHistoricalBroad("title10","subTitle10","18分鐘"));
        return historicalBroadFakeData;
    }

}
