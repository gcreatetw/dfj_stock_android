package com.gcreate.dfjStock.viewModel.tab2

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import com.gcreate.dfjStock.R
import com.gcreate.dfjStock.adapter.tab2.DfjSuggestedConditionAdapter
import com.gcreate.dfjStock.view.dialog.CalendarAlertDialogFragment
import com.gcreate.dfjStock.view.tab2.DfjSuggestedGroupFragmentArgs
import com.gcreate.dfjStock.webAPI.RetrofitInstance
import com.gcreate.dfjStock.webAPI.tab2.ApiObjectDfjSuggestedCondition
import com.gcreate.dfjStock.webAPI.tab2.ApiObjectSuggestedConditionItems
import com.gcreate.dfjStock.webAPI.tab2.SuggestedConditionsData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*

@SuppressLint("NotifyDataSetChanged")
class DfjSuggestedItemModel(private val navController: NavController) : ViewModel() {

    private val dfjApiInstance = RetrofitInstance.getDfjApiInstance2()
    private var args: DfjSuggestedGroupFragmentArgs? = null
    private var isHistorical = false
    private var isSelectedFirstButton = true
    private var startDate = ""
    private var endDate = ""

    companion object {
        private var callDfjSuggestedConditionListApi: Call<ApiObjectDfjSuggestedCondition>? = null

        fun callCancel() {
            if (callDfjSuggestedConditionListApi != null) callDfjSuggestedConditionListApi!!.cancel()
        }

        //-----------------------------------   比較起訖時間  -------------------------------------------
        fun timeCompare(startTime: String, endTime: String): Int {
            var i = 0
            //注意：传过来的时间格式必须要和这里填入的时间格式相同
            val dateFormat = SimpleDateFormat("yyyy/MM/dd", Locale.TAIWAN)
            try {
                val date1 = dateFormat.parse(startTime)!!
                val date2 = dateFormat.parse(endTime)!!
                when {
                    date2.time < date1.time -> {
                        //結束時間小於開始時間
                        i = 1
                    }
                    date2.time == date1.time -> {
                        //结束時間等於開始時間
                        i = 2
                    }
                    date2.time > date1.time -> {
                        //结束時間大於開始時間
                        i = if (date2.time - date1.time > 5 * 24 * 60 * 60 * 1000) {
                            3
                        } else {
                            4
                        }
                    }
                }
            } catch (e: Exception) {
                //Log.e(DfjSuggestedGroupFragment.TAG, e.toString())
            }
            return i
        }

    }

    fun setArgs(args: DfjSuggestedGroupFragmentArgs) {
        this.args = args
    }

    fun setIsHistorical(isHistorical: Boolean) {
        this.isHistorical = isHistorical
    }

    fun setIsSelectedFirstButton(isSelectedFirstButton: Boolean) {
        this.isSelectedFirstButton = isSelectedFirstButton
    }

    fun setStartDate(startDate: String) {
        this.startDate = startDate
    }

    fun setEndDate(endDate: String) {
        this.endDate = endDate
    }

    private var dfjSuggestedConditionListData: MutableLiveData<ApiObjectSuggestedConditionItems> = MutableLiveData()
    private var dfjSuggestedConditionListsAdapter: DfjSuggestedConditionAdapter = DfjSuggestedConditionAdapter(mutableListOf())

    fun getDfjSuggestedConditionAdapter(): DfjSuggestedConditionAdapter {
        return dfjSuggestedConditionListsAdapter
    }

    fun setDfjSuggestedConditionAdapterData(data: MutableList<SuggestedConditionsData>) {

        dfjSuggestedConditionListsAdapter.data = data
        dfjSuggestedConditionListsAdapter.notifyDataSetChanged()
        dfjSuggestedConditionListsAdapter.setOnItemClickListener { _, view, position ->
            val intentExtraArgs = Bundle()

            if (isHistorical) {
                // 比較起訖日
                if (timeCompare(startDate, endDate) == 1 || timeCompare(startDate, endDate) == 3) {
                    val selectStockGroupDialog: DialogFragment = CalendarAlertDialogFragment()
                    val dialogBundle = Bundle()
                    dialogBundle.putInt("timeCompareResult", timeCompare(startDate, endDate))
                    selectStockGroupDialog.arguments = dialogBundle
                    selectStockGroupDialog.show((view.context as FragmentActivity).supportFragmentManager, "simple dialog")
                } else {
                    intentExtraArgs.putString("suggestedCategory", data[position].description)
                    intentExtraArgs.putInt("suggestedCategoryID", data[position].id)
                    intentExtraArgs.putString("startDate", startDate)
                    intentExtraArgs.putString("endDate", endDate)
                    intentExtraArgs.putInt("exChangeID", args!!.exChangeID)
                    intentExtraArgs.putInt("securitiesID", args!!.securitiesID)
                    intentExtraArgs.putInt("marketID", args!!.marketID)
                    intentExtraArgs.putInt("industrialID", args!!.industrialID)
                    intentExtraArgs.putBoolean("isSelectedFirstButton", isSelectedFirstButton)
                    navController.navigate(R.id.action_item2_to_fragment_DfjSuggestedGroupHistoricalResult, intentExtraArgs)
                }
            } else {
                intentExtraArgs.putString("suggestedCategory", data[position].description)
                intentExtraArgs.putInt("suggestedCategoryID", data[position].id)
                intentExtraArgs.putInt("exChangeID", args!!.exChangeID)
                intentExtraArgs.putInt("securitiesID", args!!.securitiesID)
                intentExtraArgs.putInt("marketID", args!!.marketID)
                intentExtraArgs.putInt("industrialID", args!!.industrialID)
                navController.navigate(R.id.action_item2_to_fragment_DfjSuggestedGroupResult, intentExtraArgs)
            }
        }
    }

    fun getDfjSuggestedConditionDataObserver(): MutableLiveData<ApiObjectSuggestedConditionItems> {
        return dfjSuggestedConditionListData
    }

    fun getDfjSuggestedConditionApiData() {
        dfjApiInstance.dfjSuggestedCondition!!.enqueue(object : Callback<ApiObjectSuggestedConditionItems> {
            override fun onResponse(call: Call<ApiObjectSuggestedConditionItems>, response: Response<ApiObjectSuggestedConditionItems>) {
                if (response.isSuccessful) {
                    dfjSuggestedConditionListData.postValue(response.body())
                } else {
                    dfjSuggestedConditionListData.postValue(null)
                }
            }

            override fun onFailure(call: Call<ApiObjectSuggestedConditionItems>, t: Throwable) {
                dfjSuggestedConditionListData.postValue(null)
            }
        })
    }
}

class DfjSuggestedItemModelFactory(private val navController: NavController) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return DfjSuggestedItemModel(navController) as T
    }
}