package com.gcreate.dfjStock.viewModel.tab2

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import com.gcreate.dfjStock.MainActivity
import com.gcreate.dfjStock.R
import com.gcreate.dfjStock.adapter.tab2.DfjSuggestedTodayResultAdapter
import com.gcreate.dfjStock.databinding.FragmentDfjSuggestedGroupResultBinding
import com.gcreate.dfjStock.view.dialog.LoadingDialog
import com.gcreate.dfjStock.webAPI.RetrofitInstance
import com.gcreate.dfjStock.webAPI.tab2.ApiObjectSuggestedConditionResult
import com.gcreate.dfjStock.webAPI.tab2.SuggestedConditionResultData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DfjSuggestedTodayStockModel(private val binding: FragmentDfjSuggestedGroupResultBinding, private val navController: NavController) :
    ViewModel() {

    private val dfjApiInstance = RetrofitInstance.getDfjApiInstance2()
    private var recyclerListData: MutableLiveData<ApiObjectSuggestedConditionResult> = MutableLiveData()
    private var fetchResultAdapter: DfjSuggestedTodayResultAdapter = DfjSuggestedTodayResultAdapter(mutableListOf())
    private val loadingDialog = LoadingDialog()

    fun getAdapter(): DfjSuggestedTodayResultAdapter {
        return fetchResultAdapter
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setAdapterData(data: MutableList<SuggestedConditionResultData>) {
        loadingDialog.dismissDialog()
        fetchResultAdapter.data = data
        fetchResultAdapter.notifyDataSetChanged()
        fetchResultAdapter.setOnItemClickListener { _, _, position ->

            val intentExtraArgs = Bundle()
            intentExtraArgs.putString("stockName", data[position].description)
            intentExtraArgs.putInt("stockID", data[position].id)
            intentExtraArgs.putString("stockSymbol", data[position].symbol)
            navController.navigate(R.id.action_fragment_DfjSuggestedGroupResult_to_fragment_StockDetail, intentExtraArgs)
        }
    }

    fun getRecyclerListDataObserver(): MutableLiveData<ApiObjectSuggestedConditionResult> {
        return recyclerListData
    }

    fun getApiData(suggestedConditionID: Int, exChangeID: Int, marketCategoryId: Int, industryCategoryId: Int) {
        loadingDialog.show((binding.root.context as FragmentActivity).supportFragmentManager, "simple dialog")

         if (exChangeID == -1){
            dfjApiInstance.getDfjSuggestedConditionResult(suggestedConditionID)!!
                .enqueue(object : Callback<ApiObjectSuggestedConditionResult> {
                    override fun onResponse(call: Call<ApiObjectSuggestedConditionResult>, response: Response<ApiObjectSuggestedConditionResult>) {
                        if (response.isSuccessful) {
                            recyclerListData.postValue(response.body())
                        } else {
                            recyclerListData.postValue(null)
                        }
                        loadingDialog.dismissDialog()
                    }

                    override fun onFailure(call: Call<ApiObjectSuggestedConditionResult>, t: Throwable) {
                        recyclerListData.postValue(null)
                        loadingDialog.dismissDialog()
                    }

                })
        }else{
            val stockExchangeCode = MainActivity.objectAllStockCategories!!.data!![exChangeID -1].code
            dfjApiInstance.getDfjSuggestedConditionResult(suggestedConditionID, stockExchangeCode, marketCategoryId, industryCategoryId)!!
                .enqueue(object : Callback<ApiObjectSuggestedConditionResult> {
                    override fun onResponse(call: Call<ApiObjectSuggestedConditionResult>, response: Response<ApiObjectSuggestedConditionResult>) {
                        if (response.isSuccessful) {
                            recyclerListData.postValue(response.body())
                        } else {
                            recyclerListData.postValue(null)
                        }
                        loadingDialog.dismissDialog()
                    }

                    override fun onFailure(call: Call<ApiObjectSuggestedConditionResult>, t: Throwable) {
                        recyclerListData.postValue(null)
                        loadingDialog.dismissDialog()
                    }
                })
        }
    }
}

class DfjSuggestedFetchResultModelFactory(private val binding: FragmentDfjSuggestedGroupResultBinding, private val navController: NavController) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return DfjSuggestedTodayStockModel(binding, navController) as T
    }

}