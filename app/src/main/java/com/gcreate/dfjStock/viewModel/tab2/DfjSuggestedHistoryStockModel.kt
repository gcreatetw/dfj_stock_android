package com.gcreate.dfjStock.viewModel.tab2

import android.annotation.SuppressLint
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import com.chad.library.adapter.base.entity.node.BaseNode
import com.gcreate.dfjStock.MainActivity
import com.gcreate.dfjStock.adapter.tab2.NodeTreeAdapter
import com.gcreate.dfjStock.adapter.tab2.expandView.FirstNode
import com.gcreate.dfjStock.adapter.tab2.expandView.SecondNode
import com.gcreate.dfjStock.databinding.FragmentDfjSuggestedGroupHistoricalResultBinding
import com.gcreate.dfjStock.view.dialog.LoadingDialog
import com.gcreate.dfjStock.webAPI.RetrofitInstance
import com.gcreate.dfjStock.webAPI.tab2.ApiObjectSuggestedHistoricalResult
import com.gcreate.dfjStock.webAPI.tab2.SuggestedHistoricalResultData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class DfjSuggestedHistoryStockModel(private val binding: FragmentDfjSuggestedGroupHistoricalResultBinding, navController: NavController) :
    ViewModel() {

    private val dfjApiInstance = RetrofitInstance.getDfjApiInstance2()
    private val loadingDialog = LoadingDialog()
    private val nodeTreeAdapter: NodeTreeAdapter = NodeTreeAdapter(navController)

    private var recyclerListData: MutableLiveData<ApiObjectSuggestedHistoricalResult> = MutableLiveData()

    fun getAdapter(): NodeTreeAdapter {
        return nodeTreeAdapter
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setAdapterData(data: MutableList<SuggestedHistoricalResultData>) {

        loadingDialog.dismissDialog()
        nodeTreeAdapter.data = getEntity(data)
        nodeTreeAdapter.notifyDataSetChanged()
    }

    fun getRecyclerListDataObserver(): MutableLiveData<ApiObjectSuggestedHistoricalResult> {
        return recyclerListData
    }

    fun getApiData(
        isSelectedFirstButton: Boolean, suggestedCategoryID: Int, startDate: String,
        endDate: String, exChangeID: Int, marketCategoryId: Int, industryCategoryId: Int,
    ) {
        val startDate2 = startDate.replace("/", "-")
        val endDate2 = endDate.replace("/", "-")

        loadingDialog.show((binding.root.context as FragmentActivity).supportFragmentManager, "simple dialog")

        if (isSelectedFirstButton) {
            // 歷史 - 當日選股
            if (exChangeID == -1) {
                // 無篩選條件
                dfjApiInstance.getDfjSuggestedHistoricalResult(suggestedCategoryID, startDate2, endDate2)
                    .enqueue(object : Callback<ApiObjectSuggestedHistoricalResult> {
                        override fun onResponse(
                            call: Call<ApiObjectSuggestedHistoricalResult>,
                            response: Response<ApiObjectSuggestedHistoricalResult>,
                        ) {
                            if (response.isSuccessful) {
                                recyclerListData.postValue(response.body())
                            } else {
                                recyclerListData.postValue(null)
                            }
                        }

                        override fun onFailure(call: Call<ApiObjectSuggestedHistoricalResult>, t: Throwable) {
                            recyclerListData.postValue(null)
                        }
                    })
            } else {
                // 有篩選條件
                val stockExchangeCode = MainActivity.objectAllStockCategories!!.data!![exChangeID - 1].code
                dfjApiInstance.getDfjSuggestedHistoricalResult(suggestedCategoryID,
                    startDate2,
                    endDate2,
                    stockExchangeCode,
                    marketCategoryId,
                    industryCategoryId)
                    .enqueue(object : Callback<ApiObjectSuggestedHistoricalResult> {
                        override fun onResponse(
                            call: Call<ApiObjectSuggestedHistoricalResult>,
                            response: Response<ApiObjectSuggestedHistoricalResult>,
                        ) {
                            if (response.isSuccessful) {
                                recyclerListData.postValue(response.body())
                            } else {
                                recyclerListData.postValue(null)
                            }
                        }

                        override fun onFailure(call: Call<ApiObjectSuggestedHistoricalResult>, t: Throwable) {
                            recyclerListData.postValue(null)
                        }
                    })
            }

        } else {
            // 歷史 - 當日新增
            if (exChangeID == -1) {
                // 無篩選條件
                dfjApiInstance.getDfjSuggestedHistoricalResult2(suggestedCategoryID, startDate2, endDate2)
                    .enqueue(object : Callback<ApiObjectSuggestedHistoricalResult?> {
                        override fun onResponse(
                            call: Call<ApiObjectSuggestedHistoricalResult?>,
                            response: Response<ApiObjectSuggestedHistoricalResult?>,
                        ) {
                            if (response.isSuccessful) {
                                recyclerListData.postValue(response.body())
                            } else {
                                recyclerListData.postValue(null)
                            }
                        }

                        override fun onFailure(call: Call<ApiObjectSuggestedHistoricalResult?>, t: Throwable) {}
                    })
            } else {
                // 有篩選條件
                val stockExchangeCode = MainActivity.objectAllStockCategories!!.data!![exChangeID - 1].code
                dfjApiInstance.getDfjSuggestedHistoricalResult2(suggestedCategoryID,
                    startDate2,
                    endDate2,
                    stockExchangeCode,
                    marketCategoryId,
                    industryCategoryId)
                    .enqueue(object : Callback<ApiObjectSuggestedHistoricalResult?> {
                        override fun onResponse(
                            call: Call<ApiObjectSuggestedHistoricalResult?>,
                            response: Response<ApiObjectSuggestedHistoricalResult?>,
                        ) {
                            if (response.isSuccessful) {
                                recyclerListData.postValue(response.body())
                            } else {
                                recyclerListData.postValue(null)
                            }
                        }

                        override fun onFailure(call: Call<ApiObjectSuggestedHistoricalResult?>, t: Throwable) {}
                    })
            }

        }
    }


    private fun getEntity(data: MutableList<SuggestedHistoricalResultData>): MutableList<BaseNode> {
        //总的 list，里面放的是 FirstNode
        val list: MutableList<BaseNode> = ArrayList()
        for (i in 0 until data.size) {

            //SecondNode 的 list
            val secondNodeList: MutableList<BaseNode> = ArrayList()
            for (n in 0 until data[i].listInfos.size) {
                val stockBeans = data[i].listInfos[n]

                val desc =
                    if (stockBeans.description.isNullOrEmpty()) ""
                    else stockBeans.description


                val seNode = SecondNode(stockBeans.id, stockBeans.symbol, desc, stockBeans.priceChange, stockBeans.price,
                    stockBeans.states.day20,
                    stockBeans.states.day5,
                    stockBeans.states.day1,
                    stockBeans.states.min30
                )
                secondNodeList.add(seNode)
            }
            val entity = FirstNode(secondNodeList, data[i].date)
            entity.isExpanded = false
            list.add(entity)
        }
        return list
    }
}

class DfjSuggestedHistoryFetchResultModelFactory(
    private val binding: FragmentDfjSuggestedGroupHistoricalResultBinding,
    private val navController: NavController,
) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return DfjSuggestedHistoryStockModel(binding, navController) as T
    }

}