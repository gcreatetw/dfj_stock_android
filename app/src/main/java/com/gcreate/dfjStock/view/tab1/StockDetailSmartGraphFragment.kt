package com.gcreate.dfjStock.view.tab1

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.CompoundButton
import android.widget.LinearLayout
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.gcreate.dfjStock.R
import com.gcreate.dfjStock.chart.DataRequest
import com.gcreate.dfjStock.chart.KChartAdapter
import com.gcreate.dfjStock.databinding.FragmentStockDetailKchartBinding
import com.gcreate.dfjStock.kchartlibs.chart.KChartView
import com.gcreate.dfjStock.kchartlibs.chart.formatter.DateFormatter
import com.gcreate.dfjStock.webAPI.ApiCallback
import com.gcreate.dfjStock.webAPI.RetrofitInstance
import com.gcreate.dfjStock.webAPI.tab1.ApiObjectSmartAnalysisTrendLine
import com.gcreate.dfjStock.webAPI.tab1.ApiObjectSmartAnalysisTrendLine0
import com.gcreate.dfjStock.webAPI.tab1.ApiObjectStockHistoricalData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

@SuppressLint("NonConstantResourceId")
class StockDetailSmartGraphFragment : Fragment(), ApiCallback {
    /*   智慧圖形 */
    private var mAdapter: KChartAdapter? = null
    private var mCallback: ApiCallback? = null
    private var isShowLongTrendLine = true
    private var isShowMiddleTrendLine = true
    private var isShowShortTrendLine = true
    private var isShowShortestTrendLine = false

    private var colorRed = 0
    private var colorGreen = 0
    private var colorTransparent = 0
    private var colorWhite = 0


    private lateinit var binding: FragmentStockDetailKchartBinding
    private lateinit var mKChartView: KChartView
    private var stockID = 0

    private lateinit var callStockHistoricalData: Call<ApiObjectStockHistoricalData>

    companion object {
        //--------------------------------------------------------------------------------------------------
        @JvmField
        var objectStockHistoricalData: ApiObjectStockHistoricalData? = null
        @JvmField
        var isSmartGraph = false
        private var objectSmartAnalysisDownTrendLine: ApiObjectSmartAnalysisTrendLine0? = null
        private var objectSmartAnalysisUpTrendLine: ApiObjectSmartAnalysisTrendLine0? = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isSmartGraph = true
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_stock_detail_kchart, container, false)


        mCallback = this
        setKLineLayoutParam(binding.root)
        initView()
        getStockHistoricalData()
        initKLineView()
        createCheckBox()

        return binding.root
    }

    private fun initView() {
        binding.kchartView.rvKlineType.visibility = View.GONE
        binding.kchartView.nestedScrollView2.visibility = View.GONE
        mKChartView = binding.kchartView.kchartView

        colorRed = ContextCompat.getColor(requireActivity(), R.color.RedDC0000)
        colorGreen = ContextCompat.getColor(requireActivity(), R.color.Green00E600)
        colorWhite = ContextCompat.getColor(requireActivity(), R.color.WhiteFFFFFF)
        colorTransparent = ContextCompat.getColor(requireActivity(), android.R.color.transparent)

        stockID = StockDetailFragment.stockID
    }

    //------------------------------------------Stock Data function  ----------------------------------------
    private fun initKLineView() {
        mAdapter = KChartAdapter()
        mKChartView.setIsSmartGraph(true)
        mKChartView.adapter = mAdapter
        mKChartView.dateTimeFormatter = DateFormatter()
        mKChartView.setGridRows(4) //  設定背景畫面切幾行
        mKChartView.setGridColumns(4) //  設定背景畫面切幾列
        mKChartView.setIsNeedHorizontalLine(false, null, null)
        mKChartView.isScaleEnable = true // 可放大縮小
        mKChartView.isScrollEnable = false //可滑動
        mKChartView.setMa5Color(colorTransparent)
        mKChartView.setMa10Color(colorTransparent)
        mKChartView.setMa20Color(colorTransparent)

        // 監聽十字線定位
        // mKChartView.setOnSelectedChangedListener { view, point, index -> }
    }

    private fun initData() {
        mKChartView.showLoading() // 加載資料動畫 : 轉圈圈
//        if (isAdded){
//            job = GlobalScope.launch(Dispatchers.Main) {
//                val data = DataRequest().getALL(requireActivity().applicationContext, objectStockHistoricalData)
//                requireActivity().runOnUiThread {
//                    mAdapter!!.addFooterData(data)
//                    mKChartView.startAnimation() //  資料加載完 Kline圖產生時間
//                    mKChartView.refreshEnd()
//                }
//            }
//        }

        Thread {
            if (isAdded) {
                val data = DataRequest().getALL(requireActivity().applicationContext, objectStockHistoricalData)
                requireActivity().runOnUiThread {
                    mAdapter!!.addFooterData(data)
                    mKChartView.startAnimation() //  資料加載完 Kline圖產生時間
                    mKChartView.refreshEnd()
                }
            }
        }.start()

    }

    private fun getStockHistoricalData() {
        callStockHistoricalData = RetrofitInstance.getDfjApiInstance2().getStockHistoricalData(stockID, "day1", 100)
        callStockHistoricalData.enqueue(object : Callback<ApiObjectStockHistoricalData> {
            override fun onResponse(call: Call<ApiObjectStockHistoricalData>, response: Response<ApiObjectStockHistoricalData>) {
                if (response.isSuccessful) {
                    objectStockHistoricalData = response.body()

                    if (objectStockHistoricalData != null) {
                        mCallback!!.loadComplete()
                    }
                } else {
                    try {
                        Toast.makeText(context, "無歷史資料", Toast.LENGTH_LONG).show()
                    } catch (e: Exception) {
                        Toast.makeText(context, e.message, Toast.LENGTH_LONG).show()
                    }
                }
            }

            override fun onFailure(call: Call<ApiObjectStockHistoricalData>, t: Throwable) {
            }
        })

    }

    /**
     * 取得斜線斜率
     */
    private fun getSmartAnalysisDownTrendLine() {

        RetrofitInstance.getDfjApiInstance2().getSmartAnalysisTrendLine(stockID).enqueue(object : Callback<ApiObjectSmartAnalysisTrendLine> {
            override fun onResponse(call: Call<ApiObjectSmartAnalysisTrendLine>, response: Response<ApiObjectSmartAnalysisTrendLine>) {
                if (response.isSuccessful) {
                    val objectSmartAnalysisTrendLine = response.body()
                    mKChartView.setDownSlashLineParams(
                        colorGreen,
                        isShowShortestTrendLine,
                        isShowShortTrendLine,
                        isShowMiddleTrendLine,
                        isShowLongTrendLine)

                    initData()

                    if (objectSmartAnalysisTrendLine != null) {
                        mKChartView.setSmartGrapghSlashLineData(objectSmartAnalysisTrendLine, objectStockHistoricalData)
                    }
                }
            }

            override fun onFailure(call: Call<ApiObjectSmartAnalysisTrendLine>, t: Throwable) {
                Toast.makeText(activity, "連線失敗，請檢察網際網路", Toast.LENGTH_SHORT).show()
            }

        })
    }


    override fun loadComplete() {
        getSmartAnalysisDownTrendLine()
    }

    private fun createCheckBox() {
        val scale = this.resources.displayMetrics.density

        //  上升  , 下降 趨勢線 CheckBox
        val arrayOfCheckBox = ArrayList<CheckBox>()
        val list = ArrayList<String>()
        list.add("下降趨勢線")
        list.add("上升趨勢線")
        for (i in list.indices) {
            val checkBox = CheckBox(activity)
            checkBox.id = i
            checkBox.text = list[i]
            checkBox.tag = list[i]
            checkBox.isChecked = true
            checkBox.setTextColor(colorWhite)
            checkBox.setButtonDrawable(R.drawable.selector_checkbox)
            //  checkBox  icon 與 text 距離
            checkBox.setPadding(checkBox.paddingLeft + (10.0f * scale + 0.5f).toInt(),
                checkBox.paddingTop + (1f * scale).toInt(),
                checkBox.paddingRight + (10.0f * scale + 0.5f).toInt(),
                checkBox.paddingBottom + (1f * scale).toInt())
            checkBox.setOnCheckedChangeListener { buttonView: CompoundButton, isChecked: Boolean ->
                when (buttonView.tag.toString()) {
                    "下降趨勢線" -> if (isChecked) {
                        mKChartView.setDownSlashLineParams(colorGreen,
                            isShowShortestTrendLine,
                            isShowShortTrendLine,
                            isShowMiddleTrendLine,
                            isShowLongTrendLine)
                    } else {
                        mKChartView.setDownSlashLineParams(colorTransparent, true, true, true, true)
                        if (arrayOfCheckBox[1].isChecked) {
                            mKChartView.setUpSlashLineParams(colorRed,
                                isShowShortestTrendLine,
                                isShowShortTrendLine,
                                isShowMiddleTrendLine,
                                isShowLongTrendLine)
                        } else {
                            mKChartView.setUpSlashLineParams(colorTransparent, true, true, true, true)
                        }
                    }
                    "上升趨勢線" -> if (isChecked) {
                        mKChartView.setUpSlashLineParams(colorRed,
                            isShowShortestTrendLine,
                            isShowShortTrendLine,
                            isShowMiddleTrendLine,
                            isShowLongTrendLine)
                    } else {
                        mKChartView.setUpSlashLineParams(colorTransparent, true, true, true, true)
                        if (arrayOfCheckBox[0].isChecked) {
                            mKChartView.setDownSlashLineParams(colorGreen,
                                isShowShortestTrendLine,
                                isShowShortTrendLine,
                                isShowMiddleTrendLine,
                                isShowLongTrendLine)
                        } else {
                            mKChartView.setDownSlashLineParams(colorTransparent, true, true, true, true)
                        }
                    }
                }
                mKChartView.notifyChanged()
            }
            arrayOfCheckBox.add(checkBox)
            val params = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            params.setMargins(16, 8, 0, 8)
            binding.kchartView.lyCheckboxContainer.addView(checkBox, params)
        }

        //  長線 ,中線,短線,極短線 CheckBox
        val trendLineTypeOfCheckBox = ArrayList<CheckBox>()
        val trendLineTypeList = ArrayList<String>()
        trendLineTypeList.add("長線")
        trendLineTypeList.add("中線")
        trendLineTypeList.add("短線")
        trendLineTypeList.add("極短線")
        for (i in trendLineTypeList.indices) {
            val trendLineTypeCheckBox = CheckBox(activity)
            trendLineTypeCheckBox.id = i
            trendLineTypeCheckBox.text = trendLineTypeList[i]
            trendLineTypeCheckBox.tag = trendLineTypeList[i]
            trendLineTypeCheckBox.isChecked = i != 3
            trendLineTypeCheckBox.setTextColor(colorWhite)
            trendLineTypeCheckBox.setButtonDrawable(R.drawable.selector_checkbox)
            //  checkBox  icon 與 text 距離
            trendLineTypeCheckBox.setPadding(trendLineTypeCheckBox.paddingLeft + (10.0f * scale + 0.5f).toInt(),
                trendLineTypeCheckBox.paddingTop + (1f * scale).toInt(),
                trendLineTypeCheckBox.paddingRight + (10.0f * scale + 0.5f).toInt(),
                trendLineTypeCheckBox.paddingBottom + (1f * scale).toInt())
            trendLineTypeCheckBox.setOnCheckedChangeListener { buttonView: CompoundButton, isChecked: Boolean ->
                when (buttonView.tag.toString()) {
                    "長線" -> if (isChecked) {
                        isShowLongTrendLine = true
                        if (arrayOfCheckBox[0].isChecked) {
                            mKChartView.setDownSlashLineParams(colorGreen,
                                isShowShortestTrendLine,
                                isShowShortTrendLine,
                                isShowMiddleTrendLine,
                                isShowLongTrendLine)
                        } else {
                            mKChartView.setDownSlashLineParams(colorTransparent,
                                isShowShortestTrendLine,
                                isShowShortTrendLine,
                                isShowMiddleTrendLine,
                                isShowLongTrendLine)
                        }
                        if (arrayOfCheckBox[1].isChecked) {
                            mKChartView.setUpSlashLineParams(colorRed,
                                isShowShortestTrendLine,
                                isShowShortTrendLine,
                                isShowMiddleTrendLine,
                                isShowLongTrendLine)
                        } else {
                            mKChartView.setUpSlashLineParams(colorTransparent,
                                isShowShortestTrendLine,
                                isShowShortTrendLine,
                                isShowMiddleTrendLine,
                                isShowLongTrendLine)
                        }
                    } else {
                        isShowLongTrendLine = false
                        if (arrayOfCheckBox[0].isChecked) {
                            mKChartView.setDownSlashLineParams(colorGreen,
                                isShowShortestTrendLine,
                                isShowShortTrendLine,
                                isShowMiddleTrendLine,
                                isShowLongTrendLine)
                        }
                        if (arrayOfCheckBox[1].isChecked) {
                            mKChartView.setUpSlashLineParams(colorRed,
                                isShowShortestTrendLine,
                                isShowShortTrendLine,
                                isShowMiddleTrendLine,
                                isShowLongTrendLine)
                        }
                    }
                    "中線" -> if (isChecked) {
                        isShowMiddleTrendLine = true
                        if (arrayOfCheckBox[0].isChecked) {
                            mKChartView.setDownSlashLineParams(colorGreen,
                                isShowShortestTrendLine,
                                isShowShortTrendLine,
                                isShowMiddleTrendLine,
                                isShowLongTrendLine)
                        } else {
                            mKChartView.setDownSlashLineParams(colorTransparent,
                                isShowShortestTrendLine,
                                isShowShortTrendLine,
                                isShowMiddleTrendLine,
                                isShowLongTrendLine)
                        }
                        if (arrayOfCheckBox[1].isChecked) {
                            mKChartView.setUpSlashLineParams(colorRed,
                                isShowShortestTrendLine,
                                isShowShortTrendLine,
                                isShowMiddleTrendLine,
                                isShowLongTrendLine)
                        } else {
                            mKChartView.setUpSlashLineParams(colorTransparent,
                                isShowShortestTrendLine,
                                isShowShortTrendLine,
                                isShowMiddleTrendLine,
                                isShowLongTrendLine)
                        }
                    } else {
                        isShowMiddleTrendLine = false
                        if (arrayOfCheckBox[0].isChecked) {
                            mKChartView.setDownSlashLineParams(colorGreen,
                                isShowShortestTrendLine,
                                isShowShortTrendLine,
                                isShowMiddleTrendLine,
                                isShowLongTrendLine)
                        }
                        if (arrayOfCheckBox[1].isChecked) {
                            mKChartView.setUpSlashLineParams(colorRed,
                                isShowShortestTrendLine,
                                isShowShortTrendLine,
                                isShowMiddleTrendLine,
                                isShowLongTrendLine)
                        }
                    }
                    "短線" -> if (isChecked) {
                        isShowShortTrendLine = true
                        if (arrayOfCheckBox[0].isChecked) {
                            mKChartView.setDownSlashLineParams(colorGreen,
                                isShowShortestTrendLine,
                                isShowShortTrendLine,
                                isShowMiddleTrendLine,
                                isShowLongTrendLine)
                        } else {
                            mKChartView.setDownSlashLineParams(colorTransparent,
                                isShowShortestTrendLine,
                                isShowShortTrendLine,
                                isShowMiddleTrendLine,
                                isShowLongTrendLine)
                        }
                        if (arrayOfCheckBox[1].isChecked) {
                            mKChartView.setUpSlashLineParams(colorRed,
                                isShowShortestTrendLine,
                                isShowShortTrendLine,
                                isShowMiddleTrendLine,
                                isShowLongTrendLine)
                        } else {
                            mKChartView.setUpSlashLineParams(colorTransparent,
                                isShowShortestTrendLine,
                                isShowShortTrendLine,
                                isShowMiddleTrendLine,
                                isShowLongTrendLine)
                        }
                    } else {
                        isShowShortTrendLine = false
                        if (arrayOfCheckBox[0].isChecked) {
                            mKChartView.setDownSlashLineParams(colorGreen,
                                isShowShortestTrendLine,
                                isShowShortTrendLine,
                                isShowMiddleTrendLine,
                                isShowLongTrendLine)
                        }
                        if (arrayOfCheckBox[1].isChecked) {
                            mKChartView.setUpSlashLineParams(colorRed,
                                isShowShortestTrendLine,
                                isShowShortTrendLine,
                                isShowMiddleTrendLine,
                                isShowLongTrendLine)
                        }
                    }
                    "極短線" -> if (isChecked) {
                        isShowShortestTrendLine = true
                        if (arrayOfCheckBox[0].isChecked) {
                            mKChartView.setDownSlashLineParams(colorGreen,
                                isShowShortestTrendLine,
                                isShowShortTrendLine,
                                isShowMiddleTrendLine,
                                isShowLongTrendLine)
                        } else {
                            mKChartView.setDownSlashLineParams(colorTransparent,
                                isShowShortestTrendLine,
                                isShowShortTrendLine,
                                isShowMiddleTrendLine,
                                isShowLongTrendLine)
                        }
                        if (arrayOfCheckBox[1].isChecked) {
                            mKChartView.setUpSlashLineParams(colorRed,
                                isShowShortestTrendLine,
                                isShowShortTrendLine,
                                isShowMiddleTrendLine,
                                isShowLongTrendLine)
                        } else {
                            mKChartView.setUpSlashLineParams(colorTransparent,
                                isShowShortestTrendLine,
                                isShowShortTrendLine,
                                isShowMiddleTrendLine,
                                isShowLongTrendLine)
                        }
                    } else {
                        isShowShortestTrendLine = false
                        if (arrayOfCheckBox[0].isChecked) {
                            mKChartView.setDownSlashLineParams(colorGreen,
                                isShowShortestTrendLine,
                                isShowShortTrendLine,
                                isShowMiddleTrendLine,
                                isShowLongTrendLine)
                        }
                        if (arrayOfCheckBox[1].isChecked) {
                            mKChartView.setUpSlashLineParams(colorRed,
                                isShowShortestTrendLine,
                                isShowShortTrendLine,
                                isShowMiddleTrendLine,
                                isShowLongTrendLine)
                        }
                    }
                }
                mKChartView.notifyChanged()
            }
            trendLineTypeOfCheckBox.add(trendLineTypeCheckBox)
            val params = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            params.setMargins(16, 8, 0, 8)
            binding.kchartView.lyCheckboxTrendLineType.addView(trendLineTypeCheckBox, params)
        }
    }

    private fun setKLineLayoutParam(view: View) {
        val constraintLayout: ConstraintLayout = binding.kchartView.cvKline!!
        val constraintSet = ConstraintSet()
        constraintSet.clone(constraintLayout)
        constraintSet.connect(R.id.kchart_view, ConstraintSet.TOP, R.id.ly_checkboxTrendLineType, ConstraintSet.BOTTOM, 0)
        constraintSet.connect(R.id.kchart_view, ConstraintSet.BOTTOM, ConstraintSet.PARENT_ID, ConstraintSet.BOTTOM, 0)
        constraintSet.applyTo(constraintLayout)
    }

    override fun onStop() {
        super.onStop()
        isSmartGraph = false
        callStockHistoricalData.cancel()
    }

}