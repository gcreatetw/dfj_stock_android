package com.gcreate.dfjStock.view.dialog

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.gcreate.dfjStock.R
import com.gcreate.dfjStock.StorageDataController.StockGroupMaintain
import com.gcreate.dfjStock.adapter.StockDetailSelectGroupAdapter
import com.gcreate.dfjStock.databinding.DialogfragmentSelectstockgroupBinding
import com.gcreate.dfjStock.global
import com.gcreate.dfjStock.view.LoginPageActivity
import com.gcreate.dfjStock.view.tab1.StockDetailFragment

class AddStockToSelectedGroupDialogFragment : DialogFragment() {

    override fun onStart() {
        super.onStart()
        val dialog = dialog
        if (dialog != null) {
            dialog.window!!.setGravity(Gravity.BOTTOM) //設置弹出位置
            dialog.window!!.setLayout(global.windowWidth, (global.windowHeigh * 0.46).toInt())
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val binding: DialogfragmentSelectstockgroupBinding =
            DataBindingUtil.inflate(inflater, R.layout.dialogfragment_selectstockgroup, container, false)

        binding.rvStockGroupList.layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
        val adapter = StockDetailSelectGroupAdapter(LoginPageActivity.objectLoginFeedback!!.data.stock_group)
        binding.rvStockGroupList.adapter = adapter

        adapter.setOnItemClickListener(object : StockDetailSelectGroupAdapter.OnItemClickListener {
            override fun onItemClick(view: View?, position: Int) {
                val stockList: MutableList<Int> = LoginPageActivity.objectLoginFeedback!!.data.stock_group[position].contain
                if (stockList.contains(StockDetailFragment.stockID)) {
                    Toast.makeText(requireActivity(), "已加入過該群組", Toast.LENGTH_SHORT).show()
                } else {
                    StockGroupMaintain.addStockToGroup(position, StockDetailFragment.stockID)
                    Toast.makeText(requireActivity(), "加入群組成功", Toast.LENGTH_SHORT).show()
                }
                dialog!!.dismiss()
            }
        })

        return binding.root
    }
}