package com.gcreate.dfjStock.view.search

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.LinearLayout
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.gcreate.dfjStock.MainActivity
import com.gcreate.dfjStock.R
import com.gcreate.dfjStock.adapter.search.ExchangeItemAdapter
import com.gcreate.dfjStock.databinding.FragmentCategoryExchangeItemBinding
import com.gcreate.dfjStock.webAPI.RetrofitInstance
import com.gcreate.dfjStock.webAPI.search.ApiObjectSearchResult
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class ExchangeItemFragment : Fragment() {
    /*  篩選股票(第一層)  ExchangeItem */
    private lateinit var binding: FragmentCategoryExchangeItemBinding
    private lateinit var navController: NavController
    private val allStockCategories = MainActivity.objectAllStockCategories
    private val allStocks = MainActivity.objectAllStocks?.data


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_category_exchange_item, container, false)
        navController = NavHostFragment.findNavController(this)

        initView()
        autoSearch()

        return binding.root
    }

    override fun onStart() {
        super.onStart()
        binding.searchExchange.autoCompleteTxt.setText("")
    }

    private fun initView() {

        binding.toolbarExchange.apply {
            toolbarArrowUp.visibility = View.GONE
            toolbarArrowDown.visibility = View.GONE
            toolbarTitle.text = "篩選股票"
            toolbar.title = ""
            toolbar.setNavigationIcon(R.drawable.arrow_icon_back)
            toolbar.setNavigationOnClickListener {
                requireActivity().onBackPressed()
                hideSoftKeyBroad()
            }
        }

        binding.rvExchange.componentRv.apply {
            layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
            val params = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
            params.setMargins(0, 12, 0, 0)
            layoutParams = params

            val exchangeAdapter = ExchangeItemAdapter(allStockCategories!!.data)
            adapter = exchangeAdapter

            exchangeAdapter.setOnItemClickListener { _, _, position ->
                val bundle = Bundle()
                bundle.putString("category", allStockCategories.data[position].description)
                bundle.putInt("exchangeItemID", allStockCategories.data[position].id)
                bundle.putInt("securitiesClassificationItemID", 0)
                navController.navigate(R.id.action_exchangeItemFragment_to_marketItemFragment, bundle)
            }
        }

        binding.searchExchange.imgSearch.setOnClickListener {
            hideSoftKeyBroad()
            if (binding.searchExchange.autoCompleteTxt.text.toString().trim().isNotEmpty()) {
                getSingleStock(binding.searchExchange.autoCompleteTxt.text.toString())
            }
        }

        binding.searchExchange.clContent.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                binding.searchExchange.autoCompleteTxt.setText("")
            }
        }
    }

    private fun autoSearch() {
        val stockStringList = arrayListOf<String>()

        for (item in allStocks!!) {
            stockStringList.add("${item.symbol}.${item.stockExchangeCode} ${item.description}")
        }

        val searchItemAdapter = SearchItemAdapter(requireActivity(), stockStringList)
        binding.searchExchange.autoCompleteTxt.apply {
            setAdapter(searchItemAdapter)

            setOnItemClickListener { _, _, position, _ ->
                getSingleStock(stockStringList[position].split(" ")[0])
                hideSoftKeyBroad()
            }

            setOnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_NEXT || actionId == EditorInfo.IME_ACTION_DONE) {
                    hideSoftKeyBroad()
                    if (text.toString().trim().isNotEmpty()) {
                        getSingleStock(text.toString())
                    }
                    true
                } else {
                    false
                }
            }
        }
    }

    private fun getSingleStock(queryText: String) {
        RetrofitInstance.getDfjApiInstance2().getSearchStock(queryText).enqueue(object : Callback<ApiObjectSearchResult> {
            override fun onResponse(call: Call<ApiObjectSearchResult>, response: Response<ApiObjectSearchResult>) {
                if (response.isSuccessful) {
                    val searchResult = response.body()
                    if (searchResult!!.message == "stocks are not found") {
                        Toast.makeText(activity, "查無此股票", Toast.LENGTH_SHORT).show()
                    } else {
                        val bundle = Bundle()
                        bundle.putString("stockName", searchResult.data!![0].description)
                        bundle.putString("stockSymbol", searchResult.data[0].symbol)
                        bundle.putInt("stockID", searchResult.data[0].id)
                        navController.navigate(R.id.action_exchangeItemFragment_to_fragment_StockDetail, bundle)
                    }
                }
            }

            override fun onFailure(call: Call<ApiObjectSearchResult>, t: Throwable) {
                Toast.makeText(activity, "連線失敗，請檢查網際網路", Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun hideSoftKeyBroad() {
        val imm = (Objects.requireNonNull(requireActivity())
            .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
        imm.hideSoftInputFromWindow(binding.root.windowToken, 0)
    }
}