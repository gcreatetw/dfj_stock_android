package com.gcreate.dfjStock.view.dialog

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import com.bumptech.glide.Glide
import com.gcreate.dfjStock.R
import com.gcreate.dfjStock.databinding.DialogScalledImageBinding
import com.gcreate.dfjStock.global

/**
 * 圖片縮放
 */
class ScaledImageDialogFragment : DialogFragment() {

    override fun onStart() {
        super.onStart()
        //  設定 Dialog 視窗寬高
        if (dialog != null) {
            dialog!!.window!!.setLayout(global.windowWidth, (global.windowHeigh * 0.5).toInt())
            dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val binding: DialogScalledImageBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_scalled_image, container, false)

        Glide.with(requireActivity()).load(requireArguments()["ImageUrl"]).into(binding.photoView)

        return binding.root
    }
}