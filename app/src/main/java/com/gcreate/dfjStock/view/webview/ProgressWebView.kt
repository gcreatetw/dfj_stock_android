package com.gcreate.dfjStock.view.webview

import android.R
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ClipDrawable
import android.graphics.drawable.ColorDrawable
import android.util.AttributeSet
import android.view.Gravity
import android.webkit.WebView
import android.widget.ProgressBar

/* 網頁加載進度條 */
class ProgressWebView : WebView {
    private var progressbar: ProgressBar? = null

    constructor(context: Context?) : super(context!!)

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context!!, attrs, defStyleAttr)

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        initProgressBar(context)
        webChromeClient = WebChromeClient()
    }

    private fun initProgressBar(context: Context) {
        progressbar = ProgressBar(context, null, R.attr.progressBarStyleHorizontal)
        progressbar!!.layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, dp2px(context, 3f), 0, 0)
        //改變progressbar預設進度條的顏色（深紅色）為Color.GREEN
        progressbar!!.progressDrawable = ClipDrawable(ColorDrawable(Color.BLUE), Gravity.LEFT, ClipDrawable.HORIZONTAL)
        addView(progressbar)
    }


    /**
     * 方法描述：根據手機的解析度從 dp 的單位 轉成為 px(畫素)
     */
    private fun dp2px(context: Context, dpValue: Float): Int {
        val scale = context.resources.displayMetrics.density
        return (dpValue * scale + 0.5f).toInt()
    }

    /**
     * 類描述：顯示WebView載入的進度情況
     */
    inner class WebChromeClient : android.webkit.WebChromeClient() {
        override fun onProgressChanged(view: WebView, newProgress: Int) {
            if (newProgress == 100) {
                progressbar!!.visibility = GONE
            } else {
                if (progressbar!!.visibility == GONE) progressbar!!.visibility = VISIBLE
                progressbar!!.progress = newProgress
            }
            super.onProgressChanged(view, newProgress)
        }
    }
}