package com.gcreate.dfjStock.view.tab4

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.gcreate.dfjStock.R
import com.gcreate.dfjStock.adapter.tab4.StockGroupMaintainAdapter
import com.gcreate.dfjStock.databinding.FragmentOtherInfoGroupMaintainBinding
import com.gcreate.dfjStock.view.LoginPageActivity
import com.gcreate.dfjStock.view.dialog.AddStockGroupItemDialogFragment

class OtherInfoGroupMaintainFragment : Fragment() {
    /*  群組維護*/
    private lateinit var binding: FragmentOtherInfoGroupMaintainBinding

    companion object {
        private var stockGroupMaintainAdapter: StockGroupMaintainAdapter? = null
    }

    //  View content
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_other_info_group_maintain, container, false)

        initView()

        return binding.root
    }

    private fun initView() {
        // ToolBar
        binding.otherInfoGroupToolbar.apply {
            toolbarArrowUp.visibility = View.GONE
            toolbarArrowDown.visibility = View.GONE
            toolbarTitle.text = "群組維護"
            toolbarTitle.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null)
            toolbar.title = ""
            toolbar.setNavigationIcon(R.drawable.arrow_icon_back)
            toolbar.setNavigationOnClickListener { requireActivity().onBackPressed() }
        }

        setHasOptionsMenu(false)
        (requireActivity() as AppCompatActivity?)!!.setSupportActionBar(binding.otherInfoGroupToolbar.toolbar)
        binding.otherInfoGroupToolbar.toolbar.setNavigationOnClickListener { requireActivity().onBackPressed() }

        // Recyclerview Bind Data
        binding.rvContent.componentRv.layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
        stockGroupMaintainAdapter = StockGroupMaintainAdapter(LoginPageActivity.objectLoginFeedback.data.stock_group)
        binding.rvContent.componentRv.adapter = stockGroupMaintainAdapter

    }

    //------------------------------------------Menu function (新增群組)-----------------------------------------
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.stockgroupmenu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        //---------------新增群組----------------------
        if (item.itemId == R.id.toolbarMenuItem_Search) {
            val addStockGroupItem: DialogFragment = AddStockGroupItemDialogFragment()
            addStockGroupItem.show(requireActivity().supportFragmentManager, "simple dialog")
        }
        return super.onOptionsItemSelected(item)
    }

    //------------------------------------------BroadcastReceiver-----------------------------------------
    internal class UpdateGroupListBroadcastReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if ("updateStockGroupList" == intent.action) {
                stockGroupMaintainAdapter!!.setNewInstance(LoginPageActivity.objectLoginFeedback.data.stock_group)
                stockGroupMaintainAdapter!!.notifyDataSetChanged()
            }
        }
    }


}