package com.gcreate.dfjStock.view.dialog

import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import com.gcreate.dfjStock.R
import com.gcreate.dfjStock.StorageDataController.StockGroupMaintain
import com.gcreate.dfjStock.databinding.ComponentGroupMaintainBinding
import com.gcreate.dfjStock.global
import com.gcreate.dfjStock.view.tab4.OtherInfoGroupMaintainFragment.UpdateGroupListBroadcastReceiver

class DeleteStockGroupItemDialogFragment : DialogFragment() {
    /*  刪除群組*/
    private lateinit var binding: ComponentGroupMaintainBinding
    private var intent: Intent? = null
    private val mUpdateGroupListBroadcastReceiver = UpdateGroupListBroadcastReceiver()

    override fun onStart() {
        super.onStart()
        if (dialog != null) {
            dialog!!.window!!.setLayout((global.windowWidth * 0.81).toInt(), (global.windowHeigh * 0.25).toInt())
            dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.component_group_maintain, container, false)

        initView()

        return binding.root
    }

    private fun initView() {
        binding.tvAlertTitle.text = "刪除群組"

        binding.editGroupName.apply {
            setText(requireArguments().getString("GroupName"))
            isClickable = false
            isFocusable = false
            isFocusableInTouchMode = false
        }

        binding.btnOk.setOnClickListener {
            StockGroupMaintain.deleteFavStockGroupItem(requireActivity(), requireArguments().getInt("GroupIndex"))
            val intentFilter = IntentFilter("updateStockGroupList")
            requireActivity().registerReceiver(mUpdateGroupListBroadcastReceiver, intentFilter)
            intent = Intent()
            intent!!.action = "updateStockGroupList"
            requireActivity().sendBroadcast(intent)
            binding.editGroupName.clearFocus()
            dialog!!.dismiss()
        }

        binding.btnCancel.setOnClickListener { dialog!!.dismiss() }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (intent != null) {
            requireActivity().unregisterReceiver(mUpdateGroupListBroadcastReceiver)
        }
    }
}