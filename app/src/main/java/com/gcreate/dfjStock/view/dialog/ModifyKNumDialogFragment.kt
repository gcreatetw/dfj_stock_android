package com.gcreate.dfjStock.view.dialog

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnFocusChangeListener
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import com.gcreate.dfjStock.Listener.KNumListenerManager
import com.gcreate.dfjStock.R
import com.gcreate.dfjStock.StorageDataController.KChartLevelMaintain
import com.gcreate.dfjStock.StorageDataController.KNumGroupMaintain
import com.gcreate.dfjStock.databinding.DialogfragmentModifyKnumBinding
import com.gcreate.dfjStock.global

class ModifyKNumDialogFragment : DialogFragment() {

    private lateinit var binding: DialogfragmentModifyKnumBinding
    private var kNum = 0
    private var trendLineValue = 0
    private var selectKNumPosition = 0

    override fun onStart() {
        super.onStart()
        val dialog = dialog
        if (dialog != null) {
            dialog.window!!.setLayout((global.windowWidth * 0.86).toInt(), (global.windowHeigh * 0.3).toInt())
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialogfragment_modify_knum, container, false)

        intiView()
        editTextOnFocusChangeListener()
        viewOnClickAction()

        return binding.root
    }

    private fun intiView() {

        selectKNumPosition = requireArguments().getInt("KNumListPosition")

        val kNumType = KNumGroupMaintain.getKNumModelsList(requireActivity())[selectKNumPosition].kNumType
        val storageKNumValue = KNumGroupMaintain.getKNumModelsList(requireActivity())[selectKNumPosition].kNumValue
        val storageTrendLineValue = KChartLevelMaintain.getKChartTrendLineLevel(requireActivity())

        binding.tvKNumShortestLine.text = kNumType

        binding.edKNum.setText(storageKNumValue.toString())
        binding.edTrendLineLevel.setText(storageTrendLineValue.toString())
        kNum = storageKNumValue
        trendLineValue = storageTrendLineValue
    }

    private fun viewOnClickAction() {
        val kNumName = KNumGroupMaintain.getKNumModelsList(requireActivity())[selectKNumPosition].kNumType

        binding.btnOk.setOnClickListener {
            val imm = (requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
            imm.hideSoftInputFromWindow(binding.root.windowToken, 0)
            binding.edKNum.clearFocus()
            binding.edTrendLineLevel.clearFocus()

            if (kNum < 100 || kNum >= 10000) {
                Toast.makeText(requireActivity(), "KNum不可低於100，或大於10000", Toast.LENGTH_LONG).show()
            } else {
                KNumGroupMaintain.updateKNumModelsList(requireActivity(), selectKNumPosition, kNumName, kNum)
            }

            if (trendLineValue < 0 || trendLineValue > 10) {
                Toast.makeText(requireActivity(), "Level 不可低於0，或大於10", Toast.LENGTH_LONG).show()
            } else {
                KChartLevelMaintain.setKChartTrendLineLevel(requireActivity(), trendLineValue)
            }

            KNumListenerManager.getInstance().sendBroadCast(true)
            dialog!!.dismiss()
        }

        binding.btnCancel.setOnClickListener { dismiss() }
    }

    private fun editTextOnFocusChangeListener() {
        binding.edKNum.onFocusChangeListener = OnFocusChangeListener { _: View?, hasFocus: Boolean ->
            if (hasFocus) {
                binding.edKNum.setText("")
            } else {
                if (binding.edKNum.text.toString().trim { it <= ' ' }.isEmpty()) {
                    binding.edKNum.setText("0")
                }
                kNum = binding.edKNum.text.toString().trim { it <= ' ' }.toInt()
            }
        }

        binding.edTrendLineLevel.onFocusChangeListener = OnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                binding.edTrendLineLevel.setText("")
            } else {
                if (binding.edTrendLineLevel.text.toString().trim { it <= ' ' }.isEmpty()) {
                    binding.edTrendLineLevel.setText("0")
                }
                trendLineValue = binding.edTrendLineLevel.text.toString().trim { it <= ' ' }.toInt()
            }
        }

        binding.edKNum.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_NEXT || actionId == EditorInfo.IME_ACTION_DONE) {
                hideSoftKeyBroad()
                true
            } else {
                false
            }
        }

        binding.edTrendLineLevel.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_NEXT || actionId == EditorInfo.IME_ACTION_DONE) {
                hideSoftKeyBroad()
                true
            } else {
                false
            }
        }
    }

    private fun hideSoftKeyBroad() {
        val imm = (requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
        imm.hideSoftInputFromWindow(binding.root.windowToken, 0)
        binding.edKNum.clearFocus()
        binding.edTrendLineLevel.clearFocus()
    }
}