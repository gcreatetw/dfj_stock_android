package com.gcreate.dfjStock.view

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.gcreate.dfjStock.MainActivity
import com.gcreate.dfjStock.R
import com.gcreate.dfjStock.StorageDataController.MemberMaintain
import com.gcreate.dfjStock.databinding.ActivityLoginPageBinding
import com.gcreate.dfjStock.global
import com.gcreate.dfjStock.view.webview.WebViewActivity
import com.gcreate.dfjStock.webAPI.login.ApiObjectLogin
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginPageActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLoginPageBinding

    companion object {
        lateinit var objectLoginFeedback: ApiObjectLogin
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login_page)

        viewAction(this)
        global.setHideWindowStatusBar(window)

    }


    private fun viewAction(context: Context) {
        val intent = Intent(context, WebViewActivity::class.java)


        // 註冊
        binding.tvWelcomeRegistered.setOnClickListener {
            intent.putExtra("DFJ_webUrl", "https://www.dfj.com.tw/%e6%9c%83%e5%93%a1%e5%b0%88%e5%8d%80/#register_area")
            startActivity(intent)
        }

        // 忘記密碼
        binding.tvWelcomeForgetPassword.setOnClickListener {
            intent.putExtra("DFJ_webUrl", "https://www.dfj.com.tw/%e6%9c%83%e5%93%a1%e5%b0%88%e5%8d%80/#")
            intent.putExtra("isLogin",false)
            startActivity(intent)
        }

        // 登入
        binding.btnLogin.setOnClickListener { getLoginFeedBack(context) }


        binding.edWelcomePassword.setOnEditorActionListener { _, _, _ ->
            val imm = (this.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager)
            imm.hideSoftInputFromWindow(binding.root.windowToken, 0)
            binding.edWelcomePassword.clearFocus()
            getLoginFeedBack(context)
            true
        }
    }

    private fun getLoginFeedBack(context: Context) {
        binding.loadingProgress.visibility = View.VISIBLE
        val inputPhone = binding.edWelcomeAccount.text.toString()
        val inputPassword = binding.edWelcomePassword.text.toString()

        if (inputPhone.isEmpty() || inputPassword.isEmpty()){
            binding.loadingProgress.visibility = View.GONE
            Toast.makeText(this,"欄位不得空白",Toast.LENGTH_SHORT).show()
        }else{
            val call = global.gcAPI.GetLoginFeedback(inputPhone, inputPassword)
            call.enqueue(object : Callback<ApiObjectLogin?> {
                override fun onResponse(call: Call<ApiObjectLogin?>, response: Response<ApiObjectLogin?>) {
                    binding.loadingProgress.visibility = View.INVISIBLE
                    objectLoginFeedback = response.body()!!
                    if (objectLoginFeedback.success) {
                        // 儲存帳號
                        val account = binding.edWelcomeAccount.text.toString()
                        val password = binding.edWelcomePassword.text.toString()
                        MemberMaintain.updateAccountPassword(WelcomePageActivity.mContext, account, password)
                        // 取得資料後，直接跳轉到首頁
                        val intent = Intent(context, MainActivity::class.java)
                        intent.putExtra("versionCheckResult",getIntent().getBooleanExtra("versionCheckResult",true))
                        startActivity(intent)
                        finish()
                    } else {
                        Toast.makeText(this@LoginPageActivity, "登入失敗", Toast.LENGTH_SHORT).show()
                    }
                }

                override fun onFailure(call: Call<ApiObjectLogin?>, t: Throwable) {
                    binding.loadingProgress.visibility = View.INVISIBLE
                    Toast.makeText(this@LoginPageActivity, "連線失敗，請檢察網際網路", Toast.LENGTH_SHORT).show()
                }
            })
        }

    }

}