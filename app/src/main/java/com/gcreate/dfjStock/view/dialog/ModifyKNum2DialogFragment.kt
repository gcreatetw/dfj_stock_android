package com.gcreate.dfjStock.view.dialog

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnFocusChangeListener
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import com.gcreate.dfjStock.Listener.KNumListenerManager
import com.gcreate.dfjStock.R
import com.gcreate.dfjStock.StorageDataController.KNumGroupMaintain
import com.gcreate.dfjStock.databinding.DialogfragmentModifyKnum2Binding
import com.gcreate.dfjStock.global

class ModifyKNum2DialogFragment : DialogFragment() {

    private lateinit var binding: DialogfragmentModifyKnum2Binding
    private var kNum = 0
    private var selectKNumPosition = 0

    override fun onStart() {
        super.onStart()
        val dialog = dialog
        if (dialog != null) {
            dialog.window!!.setLayout((global.windowWidth * 0.86).toInt(), (global.windowHeigh * 0.3).toInt())
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialogfragment_modify_knum2, container, false)

        selectKNumPosition = requireArguments().getInt("KNumListPosition")

        val storageValue = KNumGroupMaintain.getKNumModelsList(activity)[selectKNumPosition].kNumValue

        viewOnClickAction()

        binding.edKNum.setText(storageValue.toString())
        kNum = binding.edKNum.text.toString().trim { it <= ' ' }.toInt()

        binding.edKNum.onFocusChangeListener = OnFocusChangeListener { _: View?, hasFocus: Boolean ->
            if (hasFocus) {
                binding.edKNum.setText("")
            } else {
                if (binding.edKNum.text.toString().trim { it <= ' ' }.isEmpty()) {
                    binding.edKNum.setText("0")
                }
                kNum = binding.edKNum.text.toString().trim { it <= ' ' }.toInt()
            }
        }


        return binding.root
    }

    private fun viewOnClickAction() {
        val kNumName = KNumGroupMaintain.getKNumModelsList(requireActivity())[selectKNumPosition].kNumType

        binding.btnOk.setOnClickListener {
            val imm = (requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
            imm.hideSoftInputFromWindow(binding.root.windowToken, 0)
            binding.edKNum.clearFocus()
            if (kNum < 100 || kNum >= 10000) Toast.makeText(requireActivity(), "KNum不可低於100，或大於10000", Toast.LENGTH_LONG).show() else {
                KNumGroupMaintain.updateKNumModelsList(requireActivity(), selectKNumPosition, kNumName, kNum)
                KNumListenerManager.getInstance().sendBroadCast(true)
                dismiss()
            }
        }

        binding.btnCancel.setOnClickListener { dismiss() }

        binding.edKNum.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_NEXT || actionId == EditorInfo.IME_ACTION_DONE) {
                hideSoftKeyBroad()
                true
            } else {
                false
            }
        }

    }

    private fun hideSoftKeyBroad() {
        val imm = (requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
        imm.hideSoftInputFromWindow(binding.root.windowToken, 0)
        binding.edKNum.clearFocus()
    }
}