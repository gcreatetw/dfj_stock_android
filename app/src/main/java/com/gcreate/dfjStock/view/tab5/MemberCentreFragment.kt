package com.gcreate.dfjStock.view.tab5

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.gcreate.dfjStock.Listener.UpdateNameListener
import com.gcreate.dfjStock.Listener.UpdateUserNameListenerManager
import com.gcreate.dfjStock.model.FakeData
import com.gcreate.dfjStock.R
import com.gcreate.dfjStock.adapter.tab5.HistoricalBroadAdapter
import com.gcreate.dfjStock.StorageDataController.MemberMaintain
import com.gcreate.dfjStock.databinding.FragmentMemberCentreBinding
import com.gcreate.dfjStock.view.LoginPageActivity
import com.gcreate.dfjStock.view.WelcomePageActivity.Companion.mContext
import com.gcreate.dfjStock.view.dialog.EditMemberNameDialogFragment
import com.gcreate.dfjStock.view.webview.WebViewActivity

class MemberCentreFragment : Fragment(), UpdateNameListener {

    private lateinit var binding: FragmentMemberCentreBinding


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_member_centre, container, false)

        viewBinding()
        initView()

        // getHistoricalBroadData();
        return binding.root
    }

    private fun viewBinding() {
        /* View Action*/
        binding.tvMemberInfo.setOnClickListener { refreshUI(true) }

        binding.tvHistoricalBroadcast.setOnClickListener { refreshUI(false) }

        binding.imgEditName.setOnClickListener {
            val editStockGroupItemName: DialogFragment = EditMemberNameDialogFragment()
            editStockGroupItemName.show(requireActivity().supportFragmentManager, "simple dialog")
        }

        binding.logout.setOnClickListener {
            requireActivity().finish()
            // 登出回到登入畫面，並將儲存的帳密移除
            MemberMaintain.updateAccountPassword(mContext, "", "")
            val intent = Intent(activity, LoginPageActivity::class.java)
            startActivity(intent)
        }

        binding.btnDelete.setOnClickListener {
            val intent = Intent(requireActivity(), WebViewActivity::class.java)
            intent.putExtra("DFJ_webUrl", "https://www.dfj.com.tw/contact/")
            startActivity(intent)
        }
    }

    private fun initView() {
        UpdateUserNameListenerManager.getInstance().registerListener(this)
        //  ToolBar
        binding.memberCentreToolbar.apply {
            toolbarArrowUp.visibility = View.GONE
            toolbarArrowDown.visibility = View.GONE
            toolbarTitle.text = "會員系統"
            toolbar.title = ""
            toolbar.setNavigationIcon(R.drawable.arrow_icon_back)
            toolbar.setNavigationOnClickListener { requireActivity().onBackPressed() }
        }


        binding.clMemberView1.visibility = View.VISIBLE

        //  會員資料  init data
        binding.tvMemberName.text = LoginPageActivity.objectLoginFeedback.data.name
        binding.tvMemberPhone.text = LoginPageActivity.objectLoginFeedback.data.phone
        binding.tvMemberMail.text = LoginPageActivity.objectLoginFeedback.data.email

        if (LoginPageActivity.objectLoginFeedback.data.is_vip) {
            binding.tvVIP.text = "鑽石會員"
            val effectiveDate = LoginPageActivity.objectLoginFeedback.data.vip_date + "到期"
            binding.tvVIPEffectiveDate.text = effectiveDate
        } else {
            binding.tvVIP.text = "一般會員"
            binding.tvVIPEffectiveDate.visibility = View.INVISIBLE
        }
    }

    private val historicalBroadData: Unit
        get() {
            binding.memberCentreRecyclerview.componentRv.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
            binding.memberCentreRecyclerview.componentRv.adapter =
                HistoricalBroadAdapter(FakeData.getHistoricalBroadFakeData())
        }

    private fun refreshUI(isMemberInfo: Boolean) {
        if (isMemberInfo) {
            //--------   會員資料   --------
            binding.tvMemberInfo.setTextColor(ContextCompat.getColor(requireActivity(), R.color.RedDC0000))
            binding.tvHistoricalBroadcast.setTextColor(ContextCompat.getColor(requireActivity(), R.color.WhiteFFFFFF))
            binding.clMemberView1.visibility = View.VISIBLE
            binding.clMemberView2.visibility = View.GONE
        } else {
            //--------   歷史推播   --------
            binding.tvMemberInfo.setTextColor(ContextCompat.getColor(requireActivity(), R.color.WhiteFFFFFF))
            binding.tvHistoricalBroadcast.setTextColor(ContextCompat.getColor(requireActivity(), R.color.RedDC0000))
            binding.clMemberView1.visibility = View.GONE
            binding.clMemberView2.visibility = View.VISIBLE
        }
    }

    //------------------------------------------收到通知後要做的事  -----------------------------------------
    override fun notifyUserName(userName: String) {
        binding.tvMemberName.text = userName
    }

    override fun onDestroy() {
        super.onDestroy()
        UpdateUserNameListenerManager.getInstance().unRegisterListener(this)
    }
}