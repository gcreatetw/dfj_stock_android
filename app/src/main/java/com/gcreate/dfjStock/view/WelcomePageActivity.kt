package com.gcreate.dfjStock.view

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.gcreate.dfjStock.MainActivity
import com.gcreate.dfjStock.R
import com.gcreate.dfjStock.StorageDataController.MemberMaintain
import com.gcreate.dfjStock.global
import com.gcreate.dfjStock.webAPI.login.ApiObjectAppVersion
import com.gcreate.dfjStock.webAPI.login.ApiObjectLogin
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class WelcomePageActivity : AppCompatActivity() {

    companion object {
        @JvmField
        var mContext: Context? = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome_page)

        mContext = this

        global.initGCAPI()
        global.setHideWindowStatusBar(window)

        if (global.getIsFirstTimeOpenApp(mContext)) {
            MemberMaintain.initAccountPassword(this)
        }


        global.gcAPI.appVersion.enqueue(object : Callback<ApiObjectAppVersion> {
            override fun onResponse(call: Call<ApiObjectAppVersion>, response: Response<ApiObjectAppVersion>) {
                val currentVersion = packageManager.getPackageInfo(packageName, 0).versionName
                val androidVersionList = response.body()!!.Android_version
                val versionCheckResult: Boolean = androidVersionList.contains(currentVersion)

                //  有登入儲存帳密過
                //  先取得會員 Storage Data
                MemberMaintain.getAccountPassword(this@WelcomePageActivity)
                if (MemberMaintain.ap[0] == "") {
                    //  無儲存資料，跳轉到登入
                    Handler().postDelayed({
                        val intent = Intent(this@WelcomePageActivity, LoginPageActivity::class.java)
                        intent.putExtra("versionCheckResult", versionCheckResult)
                        startActivity(intent)
                        finish()
                    }, 1000)

                } else {
                    //  有儲存資料，Call Api 要會員資料
                    val call = global.gcAPI.GetLoginFeedback(MemberMaintain.ap[0], MemberMaintain.ap[1])
                    call.enqueue(object : Callback<ApiObjectLogin?> {
                        override fun onResponse(call: Call<ApiObjectLogin?>, response: Response<ApiObjectLogin?>) {
                            val responseBody = response.body()!!
                            if (responseBody.success) {
                                LoginPageActivity.objectLoginFeedback = response.body()!!
                                //   取得資料後，直接跳轉到首頁
                                val intent = Intent(this@WelcomePageActivity, MainActivity::class.java)
                                intent.putExtra("versionCheckResult", versionCheckResult)
                                startActivity(intent)
                            } else {
                                // 使用儲存的帳密登入失敗，跳轉登入畫面
                                Toast.makeText(this@WelcomePageActivity, responseBody.user.errors.invalid_username[0], Toast.LENGTH_SHORT).show()
                                val intent = Intent(this@WelcomePageActivity, LoginPageActivity::class.java)
                                intent.putExtra("versionCheckResult", versionCheckResult)
                                startActivity(intent)
                            }
                            finish()
                        }

                        override fun onFailure(call: Call<ApiObjectLogin?>, t: Throwable) {}
                    })
                }
            }

            override fun onFailure(call: Call<ApiObjectAppVersion>, t: Throwable) {
                Toast.makeText(this@WelcomePageActivity, "please check network", Toast.LENGTH_SHORT).show()
            }
        })
    }
}