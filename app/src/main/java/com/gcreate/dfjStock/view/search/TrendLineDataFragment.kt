package com.gcreate.dfjStock.view.search

import android.os.Bundle
import android.view.*
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.gcreate.dfjStock.R
import com.gcreate.dfjStock.adapter.search.TrendLineDataAdapter
import com.gcreate.dfjStock.databinding.FragmentCategoryTrendlinedataBinding
import com.gcreate.dfjStock.view.dialog.StockTipDialogFragment
import com.gcreate.dfjStock.webAPI.RetrofitInstance
import com.gcreate.dfjStock.webAPI.search.ApiObjectCategoriesResult
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TrendLineDataFragment : Fragment() {
    /*  篩選股票. 篩選結果頁 TrendLineData */
    private lateinit var binding: FragmentCategoryTrendlinedataBinding
    private lateinit var navController: NavController
    private val args: TrendLineDataFragmentArgs by navArgs()

    private var lastOffset = 0
    private var lastPosition = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_category_trendlinedata, container, false)
        navController = NavHostFragment.findNavController(this)

        initView()

        return binding.root
    }

    private fun initView() {

        binding.trendlineToolbar.apply {
            toolbarArrowUp.visibility = View.GONE
            toolbarArrowDown.visibility = View.GONE
            toolbarTitle.text = args.category
            toolbar.title = ""
            toolbar.setNavigationIcon(R.drawable.arrow_icon_back)
            toolbar.setNavigationOnClickListener { requireActivity().onBackPressed() }
        }

        setHasOptionsMenu(true)
        (requireActivity() as AppCompatActivity?)!!.setSupportActionBar(binding.trendlineToolbar.toolbar)
        binding.trendlineToolbar.toolbar.setNavigationOnClickListener { requireActivity().onBackPressed() }

        getCategoriesResult()

        val params = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
        params.setMargins(0, 12, 0, 0)
        binding.rvContent.componentRv.layoutParams = params
        binding.rvContent.componentRv.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)

        // 監聽RecyclerView滾動狀態
        binding.rvContent.componentRv.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (recyclerView.layoutManager != null) {
                    positionAndOffset
                }
            }
        })
    }

    private fun getCategoriesResult() {
        RetrofitInstance.getDfjApiInstance2()!!.getCategoriesResult(args.exchangeItemID, args.marketItemID, args.industrialItemID).enqueue(object :
            Callback<ApiObjectCategoriesResult> {
            override fun onResponse(call: Call<ApiObjectCategoriesResult>, response: Response<ApiObjectCategoriesResult>) {
                if (response.isSuccessful) {
                    val stockCategoryResult = response.body()
                    val mAdapter = TrendLineDataAdapter(stockCategoryResult!!.data!!)
                    binding.rvContent.componentRv.adapter = mAdapter

                    mAdapter.setOnItemClickListener { _, _, position ->
                        val bundle = Bundle()
                        bundle.putString("stockName", stockCategoryResult.data!![position].description)
                        bundle.putString("stockSymbol", stockCategoryResult.data!![position].symbol)
                        bundle.putInt("stockID", stockCategoryResult.data!![position].id)
                        navController.navigate(R.id.action_TrendLineDataFragment_to_fragment_StockDetail, bundle)
                    }
                }
            }

            override fun onFailure(call: Call<ApiObjectCategoriesResult>, t: Throwable) {
                Toast.makeText(activity, "連線失敗，請檢察網際網路", Toast.LENGTH_SHORT).show()
            }
        })
    }


    //------------------------------------------保存離開 Fragment时的瀏覽位置-----------------------------------------
    // 獲取與該view的頂部的偏移量
    // 得到該View的數組位置
    // 獲取可視的第一個 view
    /*** 紀錄RecyclerView當前位置  */
    private val positionAndOffset: Unit
        get() {
            val layoutManager = binding.rvContent.componentRv.layoutManager as LinearLayoutManager?
            // 獲取可視的第一個 view
            val topView = layoutManager!!.getChildAt(0)
            if (topView != null) {
                //獲取與該view的頂部的偏移量
                lastOffset = topView.top
                //得到該View的數組位置
                lastPosition = layoutManager.getPosition(topView)
            }
        }

    /** * 讓RecyclerView滾動到指定位置  */
    private fun scrollToPosition() {
        if (binding.rvContent.componentRv.layoutManager != null && lastPosition >= 0) {
            (binding.rvContent.componentRv.layoutManager as LinearLayoutManager?)!!.scrollToPositionWithOffset(lastPosition, lastOffset)
        }
    }

    //------------------------------------------Menu function  -----------------------------------------
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.toolbar, menu)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        val filterIcon = menu.findItem(R.id.toolbarMenuItem_filter)
        val searchIcon = menu.findItem(R.id.toolbarMenuItem_Search)
        filterIcon.isVisible = false
        searchIcon.isVisible = false
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        //-----------------------    股票圖示註記  ----------------------------
        when (item.itemId) {
            R.id.toolbarMenuItem_Tip -> {
                val tripDialog: DialogFragment = StockTipDialogFragment()
                tripDialog.show(requireActivity().supportFragmentManager, "simple dialog")
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onStart() {
        super.onStart()
        scrollToPosition()
    }

    override fun onStop() {
        super.onStop()
        cancelApiCall()
    }

    private fun cancelApiCall() {
        //objectTrendLineStateDataCall!!.cancel()
    }

}