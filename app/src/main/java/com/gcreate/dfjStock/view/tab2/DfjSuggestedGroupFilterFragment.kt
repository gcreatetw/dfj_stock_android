package com.gcreate.dfjStock.view.tab2

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.gcreate.dfjStock.MainActivity
import com.gcreate.dfjStock.R
import com.gcreate.dfjStock.databinding.FragmentDfjSuggestedFilterBinding

import com.gcreate.dfjStock.webAPI.search.IndustryCategory
import com.gcreate.dfjStock.webAPI.search.MarketCategory
import java.util.*

class DfjSuggestedGroupFilterFragment : Fragment() {
    /**
     * 智慧選股篩選
     */
    private lateinit var binding: FragmentDfjSuggestedFilterBinding
    private lateinit var navController: NavController
    private val args = Bundle()

    private var exChangeID = 0
    private var marketID = 0
    private var industrialID = 0

    companion object {
        // 站存篩選條件
        private var exchangeSelectedItem = -1
        private var marketSelectedItem = -1
        private var industrialSelectedItem = -1

        fun setSpinnerSelectItem(exchangeSelectedItem: Int, marketSelectedItem: Int, industrialSelectedItem: Int) {
            this.exchangeSelectedItem = exchangeSelectedItem
            this.marketSelectedItem = marketSelectedItem
            this.industrialSelectedItem = industrialSelectedItem
        }
    }

    private var marketItemsBeans: MutableList<MarketCategory>? = null
    private var industrialClassificationItemsBeans: MutableList<IndustryCategory>? = null
    private var objectAllStockCategories = MainActivity.objectAllStockCategories!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_dfj_suggested_filter, container, false)
        navController = NavHostFragment.findNavController(this)

        initView()
        setExchangeCategoriesSpinnerFilterData()

        binding.btnOk.setOnClickListener {
            navController.navigate(R.id.action_fragment_DFJSuggestedFilter_to_item2, args)
        }

        return binding.root
    }

    private fun initView() {
        //  ToolBar
        binding.include.toolbar.title = ""
        setHasOptionsMenu(false)
        (requireActivity() as AppCompatActivity).setSupportActionBar(binding.include.toolbar)
        binding.include.toolbarTitle.text = "智慧選股"
        binding.include.toolbarArrowUp.visibility = View.GONE
        binding.include.toolbarArrowDown.visibility = View.GONE
    }

    private fun setExchangeCategoriesSpinnerFilterData() {
        val exchangeItemList: MutableList<String> = ArrayList()
        exchangeItemList.add("請選擇")

        for (i in objectAllStockCategories.data.indices) {
            val exchangeItem = objectAllStockCategories.data[i].description
            exchangeItemList.add(exchangeItem)
        }

        val adapter = object : ArrayAdapter<String>(requireActivity(), android.R.layout.simple_spinner_dropdown_item, exchangeItemList) {
            override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
                val v = super.getDropDownView(position, null, parent)
                if (position == exchangeSelectedItem) {
                    v.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.RedDC0000))
                } else {
                    v.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.WhiteFFFFFF))
                }
                return v
            }
        }

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        binding.filterSpinner1.adapter = adapter
        binding.filterSpinner1.setSelection(exchangeSelectedItem)
        binding.filterSpinner1.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View, position: Int, id: Long) {
                exchangeSelectedItem = position
                if (exchangeSelectedItem != 0) {
                    setSpinnerVisible(View.VISIBLE, View.GONE)
                    exChangeID = objectAllStockCategories.data[exchangeSelectedItem - 1].id
                    setReturnArgument(exchangeSelectedItem, -1, -1)
                    //setSpinnerFilter2Data()
                    setMarketCategoriesSpinnerFilterData()
                } else {
                    setReturnArgument(-1, -1, -1)
                    setSpinnerSelectItem(0, 0, 0)
                    setSpinnerVisible(View.GONE, View.GONE)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
    }

    private fun setMarketCategoriesSpinnerFilterData() {
        val marketCategoriesItemList: MutableList<String> = ArrayList()
        marketCategoriesItemList.add("請選擇")

        marketItemsBeans = objectAllStockCategories.data[exChangeID - 1].market_categories
        val marketItemDataSize = marketItemsBeans!!.size
        for (i in 0 until marketItemDataSize) {
            val marketItem = marketItemsBeans!![i].description
            marketCategoriesItemList.add(marketItem)
        }

        val adapter = object : ArrayAdapter<String>(requireActivity(), android.R.layout.simple_spinner_dropdown_item, marketCategoriesItemList) {
            override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
                val v = super.getDropDownView(position, null, parent)
                if (position == marketSelectedItem) {
                    v.setBackgroundColor(ContextCompat.getColor(requireActivity(),
                        R.color.RedDC0000))
                } else {
                    v.setBackgroundColor(ContextCompat.getColor(
                        requireActivity(), R.color.WhiteFFFFFF))
                }
                return v
            }
        }

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.filterSpinner2.adapter = adapter
        binding.filterSpinner2.setSelection(marketSelectedItem)
        binding.filterSpinner2.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View, position: Int, id: Long) {
                marketSelectedItem = position
                if (marketSelectedItem != 0) {
                    marketID = marketItemsBeans!![marketSelectedItem - 1].id
                    setReturnArgument(exChangeID, marketID, -1)
                    setSpinnerVisible(View.VISIBLE, View.VISIBLE)
                    setIndustrialCategoriesSpinnerFilterData()
                    setSpinnerSelectItem(exchangeSelectedItem, marketSelectedItem, marketSelectedItem)
                } else {
                    setReturnArgument(exChangeID, -1, -1)
                    setSpinnerSelectItem(exchangeSelectedItem, 0, 0)
                    setSpinnerVisible(View.VISIBLE, View.GONE)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
    }

    private fun setIndustrialCategoriesSpinnerFilterData() {
        val industrialItemList: MutableList<String> = ArrayList()
        industrialItemList.add("請選擇")
        industrialClassificationItemsBeans = marketItemsBeans!![marketSelectedItem - 1].industry_categories

        val industrialItemDataSize = industrialClassificationItemsBeans!!.size
        for (i in 0 until industrialItemDataSize) {
            val industrialItem = industrialClassificationItemsBeans!![i].description
            industrialItemList.add(industrialItem)
        }

        val adapter = object : ArrayAdapter<String>(requireActivity(), android.R.layout.simple_spinner_dropdown_item, industrialItemList) {
            override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
                val v = super.getDropDownView(position, null, parent)
                if (position == industrialSelectedItem) v.setBackgroundColor(ContextCompat.getColor(requireActivity(),
                    R.color.RedDC0000)) else v.setBackgroundColor(ContextCompat.getColor(
                    requireActivity(), R.color.WhiteFFFFFF))
                return v
            }
        }

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.filterSpinner3.adapter = adapter
        binding.filterSpinner3.setSelection(industrialSelectedItem)
        binding.filterSpinner3.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View, position: Int, id: Long) {
                industrialSelectedItem = position

                if (industrialSelectedItem != 0) {
                    industrialID = industrialClassificationItemsBeans!![industrialSelectedItem - 1].id
                    setReturnArgument(exChangeID, marketID, industrialID)
                    setSpinnerSelectItem(exchangeSelectedItem, marketSelectedItem, industrialSelectedItem)
                } else {
                    setReturnArgument(exChangeID, marketID, -1)
                    setSpinnerSelectItem(exchangeSelectedItem, marketSelectedItem, 0)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
    }

    private fun setSpinnerVisible(flitter2Visibility: Int, flitter3Visibility: Int) {
        binding.lyFilter2.visibility = flitter2Visibility
        binding.lyFilter3.visibility = flitter3Visibility
    }

    private fun setReturnArgument(exChangeCategoryID: Int, marketCategoryId: Int, industryCategoryId: Int) {
        args.putInt("exChangeID", exChangeCategoryID)
        args.putInt("marketID", marketCategoryId)
        args.putInt("industrialID", industryCategoryId)
    }

}