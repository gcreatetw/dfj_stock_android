package com.gcreate.dfjStock.view.tab2

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.gcreate.dfjStock.R
import com.gcreate.dfjStock.databinding.FragmentDfjSuggestedGroupBinding
import com.gcreate.dfjStock.viewModel.tab2.DfjSuggestedItemModel
import com.gcreate.dfjStock.viewModel.tab2.DfjSuggestedItemModelFactory
import java.text.SimpleDateFormat
import java.util.*

/** 智慧選股 */
class DfjSuggestedGroupFragment : Fragment() {

    private lateinit var binding: FragmentDfjSuggestedGroupBinding
    private lateinit var navController: NavController
    private val args: DfjSuggestedGroupFragmentArgs by navArgs()
    private var viewModel: DfjSuggestedItemModel? = null

    companion object {
        private var isHistorical = false
        private var sStartDate = ""
        private var sEndDate = ""
        private var tmpYear_begin = 0
        private var tmpMonth_begin = 0
        private var tmpDayOfMonth_begin = 0
        private var tmpYear_end = 0
        private var tmpMonth_end = 0
        private var tmpDayOfMonth_end = 0
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_dfj_suggested_group, container, false)
        navController = NavHostFragment.findNavController(this)

        getModelInstance()
        setupBinding(makeApiCall())

        initView()
        itemClickEvent(binding)

        viewModel!!.setArgs(args)

        return binding.root
    }

    private fun initView() {
        //  ToolBar
        binding.include.toolbar.title = ""
        setHasOptionsMenu(true)
        (requireActivity() as AppCompatActivity).setSupportActionBar(binding.include.toolbar)
        binding.include.toolbarTitle.text = "智慧選股"
        binding.include.toolbarArrowUp.visibility = View.GONE
        binding.include.toolbarArrowDown.visibility = View.GONE

        if (sStartDate.isEmpty() && sEndDate.isEmpty()) {
            date
        } else {
            binding.tvBeginDate.text = sStartDate
            binding.tvEndDate.text = sEndDate
        }

        refreshUI(isHistorical)
    }

    // click event
    private fun itemClickEvent(binding: FragmentDfjSuggestedGroupBinding) {

        binding.tvDfjToday.setOnClickListener { refreshUI(false) }

        binding.tvDfjHistorical.setOnClickListener { refreshUI(true) }

        binding.tvBeginDate.setOnClickListener {
            val startTimeCalendarDialog = DatePickerDialog(requireActivity(), { _, year, month, dayOfMonth ->
                tmpYear_begin = year
                tmpMonth_begin = month
                tmpDayOfMonth_begin = dayOfMonth
                binding.tvBeginDate.text = String.format("%d/%d/%d", tmpYear_begin, tmpMonth_begin + 1, tmpDayOfMonth_begin)
                sStartDate = binding.tvBeginDate.text.toString()
                viewModel!!.setStartDate(sStartDate)
            }, tmpYear_begin, tmpMonth_begin, tmpDayOfMonth_begin)
            // hide calendar title
            (startTimeCalendarDialog.datePicker.getChildAt(0) as ViewGroup).getChildAt(0).visibility = View.GONE
            startTimeCalendarDialog.show()
        }

        binding.tvEndDate.setOnClickListener {
            val endTimeCalendarDialog = DatePickerDialog(requireActivity(), { _, year, month, dayOfMonth ->
                tmpYear_end = year
                tmpMonth_end = month
                tmpDayOfMonth_end = dayOfMonth
                binding.tvEndDate.text = String.format("%d/%d/%d", tmpYear_end, tmpMonth_end + 1, tmpDayOfMonth_end)
                sEndDate = binding.tvEndDate.text.toString()
                viewModel!!.setEndDate(sEndDate)
            }, tmpYear_end, tmpMonth_end, tmpDayOfMonth_end)
            // hide calendar title
            (endTimeCalendarDialog.datePicker.getChildAt(0) as ViewGroup).getChildAt(0).visibility = View.GONE
            endTimeCalendarDialog.show()
        }

        binding.mRadioGroup.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.mRadioButton0 -> viewModel!!.setIsSelectedFirstButton(true)
                R.id.mRadioButton1 -> viewModel!!.setIsSelectedFirstButton(false)
            }
        }

    }

    // 結束日期
    private val date: Unit
        // 開始日期 //日期減 5 天
        get() {
            val sdf = SimpleDateFormat("yyyy/MM/dd", Locale.TAIWAN)

            // 結束日期
            val endDate = Calendar.getInstance()
            tmpYear_end = endDate[Calendar.YEAR]
            tmpMonth_end = endDate[Calendar.MONTH]
            tmpDayOfMonth_end = endDate[Calendar.DAY_OF_MONTH]

            // 開始日期
            val beginDate = Calendar.getInstance()
            beginDate.add(Calendar.YEAR, 0)
            beginDate.add(Calendar.MONTH, 0)
            beginDate.add(Calendar.DAY_OF_YEAR, -5) //日期減 5 天
            tmpYear_begin = beginDate[Calendar.YEAR]
            tmpMonth_begin = beginDate[Calendar.MONTH]
            tmpDayOfMonth_begin = beginDate[Calendar.DAY_OF_MONTH]

            binding.tvBeginDate.text = sdf.format(beginDate.time)
            sStartDate = sdf.format(beginDate.time)
            viewModel!!.setStartDate(sStartDate)

            binding.tvEndDate.text = sdf.format(endDate.time)
            sEndDate = sdf.format(endDate.time)
            viewModel!!.setEndDate(sEndDate)

        }

    private fun refreshUI(isFirstTimeHistorical: Boolean) {
        isHistorical = isFirstTimeHistorical
        viewModel!!.setIsHistorical(isFirstTimeHistorical)
        if (isFirstTimeHistorical) {
            //--------   歷史選股   --------
            binding.tvDfjNotice.visibility = View.GONE
            binding.tvDfjToday.setTextColor(ContextCompat.getColor(requireActivity(), R.color.WhiteFFFFFF))
            binding.tvDfjHistorical.setTextColor(ContextCompat.getColor(requireActivity(), R.color.RedDC0000))
            binding.lyDatePicker.visibility = View.VISIBLE
            binding.mRadioGroup.visibility = View.VISIBLE
        } else {
            //--------   今日選股   --------
            binding.tvDfjNotice.visibility = View.VISIBLE
            binding.tvDfjToday.setTextColor(ContextCompat.getColor(requireActivity(), R.color.RedDC0000))
            binding.tvDfjHistorical.setTextColor(ContextCompat.getColor(requireActivity(), R.color.WhiteFFFFFF))
            binding.lyDatePicker.visibility = View.GONE
            binding.mRadioGroup.visibility = View.GONE
        }
    }

    //------------------------------------------Menu function  -----------------------------------------
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.toolbar, menu)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        val tripIcon = menu.findItem(R.id.toolbarMenuItem_Tip)
        tripIcon.isVisible = false
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        //-----------------------    股票圖示註記  ----------------------------
        when (item.itemId) {
            R.id.toolbarMenuItem_Search -> {
                val bundle = Bundle()
                navController.navigate(R.id.action_item2_to_exchangeItemFragment, bundle)
            }
            R.id.toolbarMenuItem_filter -> navController.navigate(R.id.action_item2_to_fragment_DFJSuggestedFilter)
        }
        return super.onOptionsItemSelected(item)
    }


    private fun getModelInstance(): DfjSuggestedItemModel? {
        if (viewModel == null) {
            viewModel =
                ViewModelProvider(requireActivity(), DfjSuggestedItemModelFactory(navController)).get(DfjSuggestedItemModel::class.java)
        }

        return viewModel
    }

    private fun setupBinding(viewModel: DfjSuggestedItemModel) {
//        binding.setVariable(BR.viewModel, viewModel)
        binding.executePendingBindings()
        binding.rvContent.componentRv.apply {
            layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
            adapter = viewModel.getDfjSuggestedConditionAdapter()

            hasFixedSize()
        }

    }

    @SuppressLint("InflateParams")
    private fun makeApiCall(): DfjSuggestedItemModel {

        viewModel!!.getDfjSuggestedConditionDataObserver().observe(requireActivity(), {
            if (it != null)
                viewModel!!.setDfjSuggestedConditionAdapterData(it.data)
        })

        callApiData()

        return viewModel!!
    }

    private fun callApiData() {
        DfjSuggestedItemModel.callCancel()
        viewModel!!.getDfjSuggestedConditionApiData()
    }

    override fun onStop() {
        super.onStop()
        DfjSuggestedItemModel.callCancel()
    }

    override fun onDestroy() {
        super.onDestroy()
        DfjSuggestedItemModel.callCancel()
        DfjSuggestedGroupFilterFragment.setSpinnerSelectItem(-1,-1,-1)
    }

}