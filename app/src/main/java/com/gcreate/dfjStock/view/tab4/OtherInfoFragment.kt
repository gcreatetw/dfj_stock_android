package com.gcreate.dfjStock.view.tab4

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.gcreate.dfjStock.MainActivity
import com.gcreate.dfjStock.R
import com.gcreate.dfjStock.adapter.tab4.VerticalItemListAdapter
import com.gcreate.dfjStock.databinding.FragmentOtherInfoBinding
import com.gcreate.dfjStock.view.webview.WebViewActivity
import java.util.*

class OtherInfoFragment : Fragment() {
    /*  其他資訊 */
    private lateinit var binding: FragmentOtherInfoBinding
    private lateinit var navController: NavController

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_other_info, container, false)
        navController = NavHostFragment.findNavController(this)

        initView()

        val listData: MutableList<String> = ArrayList()
        listData.add("東方靖官網")
        listData.add("功能介紹")
        listData.add("影音分析")
        listData.add("群組維護")
        listData.add("K量設定")

        val adapter = VerticalItemListAdapter(listData)

        binding.rvContent.componentRv.layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
        binding.rvContent.componentRv.adapter = adapter

        adapter.setOnItemClickListener { _, _, position ->
            val intent = Intent(requireActivity(), WebViewActivity::class.java)
            intent.putExtra("isLogin",true)
            MainActivity.setCurrentActivity("webViewActivity")
            when (listData[position]) {
                "東方靖官網" -> {
                    intent.putExtra("DFJ_webUrl", "https://www.dfj.com.tw/")
                    startActivity(intent)
                }
                "功能介紹" -> {
                    intent.putExtra("DFJ_webUrl", "https://www.dfj.com.tw/%e6%9d%b1%e6%96%b9%e9%9d%96%e8%82%a1%e5%b8%82%e9%9b%b2-app/")
                    startActivity(intent)
                }
                "影音分析" -> {
                    intent.putExtra("DFJ_webUrl", "https://www.dfj.com.tw/video_subscription_member/")
                    startActivity(intent)
                }
                "群組維護" -> navController.navigate(R.id.action_item4_to_otherInfoGroupMaintainFragment)
                "K量設定" -> navController.navigate(R.id.action_item4_to_otherInfoKNumSettingFragment)
            }
        }

        return binding.root
    }

    private fun initView() {
        // ToolBar
        binding.otherInfoToolbar.apply {
            toolbarArrowUp.visibility = View.GONE
            toolbarArrowDown.visibility = View.GONE
            toolbarTitle.text = "其他資訊"
            toolbar.title = ""
            toolbar.navigationIcon = null
        }

        val params = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
        params.setMargins(0, 12, 0, 0)
        binding.rvContent.componentRv.layoutParams = params
    }
}