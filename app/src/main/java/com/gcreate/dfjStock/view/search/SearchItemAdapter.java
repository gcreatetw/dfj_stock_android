package com.gcreate.dfjStock.view.search;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.gcreate.dfjStock.R;
import com.gcreate.dfjStock.global;
import com.gcreate.dfjStock.model.ObjectSearchStockItem;

import java.util.ArrayList;
import java.util.List;

public class SearchItemAdapter extends ArrayAdapter<String> {

    private final List<String> dataList;

    public SearchItemAdapter(@NonNull Context context, @NonNull ArrayList<String> stockItems) {
        super(context, 0, stockItems);
        dataList = new ArrayList<>(stockItems);
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return stockFilter;
    }

    private Filter stockFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            FilterResults results = new FilterResults();
            List<String> suggestions = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                suggestions.addAll(dataList);
            } else {
                String filterPattern = constraint.toString();
                for (String item : dataList) {
                    if (item.contains(filterPattern)) {
                        suggestions.add(item);
                    }
                }
            }
            results.values = suggestions;
            results.count = suggestions.size();

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            clear();
            addAll((List) results.values);
            notifyDataSetChanged();
        }

    };


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View listItemView = convertView;
        //listItemView可能會是空的，例如App剛啟動時，沒有預先儲存的view可使用
        if (listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(R.layout.search_dropdown_list_item, parent, false);
        }

        TextView list_item_StockName = listItemView.findViewById(R.id.list_item);
        list_item_StockName.setText(getItem(position));

        return listItemView;
    }
}
