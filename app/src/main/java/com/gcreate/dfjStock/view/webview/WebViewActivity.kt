package com.gcreate.dfjStock.view.webview

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import android.view.View
import android.webkit.WebSettings
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.gcreate.dfjStock.R
import com.gcreate.dfjStock.databinding.ActivityWebviewBinding
import com.gcreate.dfjStock.view.LoginPageActivity

class WebViewActivity : AppCompatActivity() {

    private var url: String? = null
    private lateinit var binding: ActivityWebviewBinding

    @SuppressLint("SetJavaScriptEnabled")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_webview)

        //獲得值
        val intent = intent
        //傳值
        if (intent.getStringExtra("DFJ_webUrl") != null) {
            url = intent.getStringExtra("DFJ_webUrl")
            binding.toolbar.visibility = View.GONE
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            binding.webView.settings.mixedContentMode = WebSettings.MIXED_CONTENT_COMPATIBILITY_MODE
        }

        // 開啟網頁設定
        //binding.webView.settings.loadWithOverviewMode = true
        binding.webView.settings.builtInZoomControls = true
        //binding.webView.settings.useWideViewPort = true;
        binding.webView.settings.setSupportZoom(true)
        binding.webView.settings.displayZoomControls = true
        binding.webView.settings.blockNetworkImage = false
        binding.webView.settings.javaScriptEnabled = true
        binding.webView.settings.databaseEnabled = true
        binding.webView.settings.domStorageEnabled = true
        binding.webView.settings.cacheMode = WebSettings.LOAD_NO_CACHE
        binding.webView.settings.useWideViewPort = false
        binding.webView.settings.userAgentString = "Android"
        binding.webView.webViewClient = WebViewClient()


        if (intent.getBooleanExtra("isLogin", false)) {
            val map = mapOf(
                Pair("userId", LoginPageActivity.objectLoginFeedback.data.user_id.toString()),
                Pair("gcKey", "gc_dfj_login")
            )
            binding.webView.loadUrl(url!!, map)
        } else {
            binding.webView.loadUrl(url!!)
        }

    }

    override fun onDestroy() {
        binding.webView.clearCache(true) // 清除暫存 cache
        super.onDestroy()
    }

}