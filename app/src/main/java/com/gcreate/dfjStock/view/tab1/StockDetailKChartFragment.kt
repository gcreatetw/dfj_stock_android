package com.gcreate.dfjStock.view.tab1

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.CompoundButton
import android.widget.LinearLayout
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.gcreate.dfjStock.Listener.*
import com.gcreate.dfjStock.R
import com.gcreate.dfjStock.StorageDataController.KNumGroupMaintain
import com.gcreate.dfjStock.adapter.tab1.StockDetailKIlineGraphTypeAdapter
import com.gcreate.dfjStock.chart.DataRequest
import com.gcreate.dfjStock.chart.KChartAdapter
import com.gcreate.dfjStock.chart.KLineEntity
import com.gcreate.dfjStock.databinding.FragmentStockDetailKchartBinding
import com.gcreate.dfjStock.kchartlibs.chart.BaseKChartView
import com.gcreate.dfjStock.kchartlibs.chart.KChartView
import com.gcreate.dfjStock.kchartlibs.chart.formatter.DateFormatter
import com.gcreate.dfjStock.kchartlibs.chart.formatter.TimeFormatter2
import com.gcreate.dfjStock.model.FakeData
import com.gcreate.dfjStock.webAPI.ApiCallback
import com.gcreate.dfjStock.webAPI.RetrofitInstance
import com.gcreate.dfjStock.webAPI.tab1.ApiObjectStockDFJTrendLine
import com.gcreate.dfjStock.webAPI.tab1.ApiObjectStockHistoricalData
import com.gcreate.dfjStock.webAPI.tab1.TrendlineData
import kotlinx.android.synthetic.main.card_stock_graph_item.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class StockDetailKChartFragment : Fragment(), KViewOrientationListener, ApiCallback, KNumListener, TrendLineListener {
    /*    單一選股畫面 - K圖分析 */
    private lateinit var binding: FragmentStockDetailKchartBinding
    private var mKChartView: KChartView? = null
    private var mCallback: ApiCallback? = null
    private lateinit var callStockHistoricalData: Call<ApiObjectStockHistoricalData>
    private var stockID = 0
    private var apiCallFinishedCount = 0

    private val kLineType = StockDetailKIlineGraphTypeAdapter(FakeData.KChartLineType())

    companion object {

        @JvmField
        var objectStockHistoricalData: ApiObjectStockHistoricalData? = null
        private const val TAG = "Fragment_StockDetail_KChart"
        private var mAdapter: KChartAdapter? = null

        private var objectStockUpTrendLine: MutableList<TrendlineData> = mutableListOf()
        private var objectStockDownTrendLine: MutableList<TrendlineData> = mutableListOf()
        private var currentKLineType = 0
        private var data: List<KLineEntity> = ArrayList()
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_stock_detail_kchart, container, false)

        KNumListenerManager.getInstance().registerListener(this)
        TrendLineListenerManager.getInstance().registerListener(this)
        mCallback = this
        initView()

        mKChartView = binding.kchartView.kchartView

        try {
            setKLineLayoutParam(binding.root)
        } catch (e: Exception) {
            Log.d(TAG, e.toString())
        }

        getStockHistoricalData(4, KNumGroupMaintain.getKNumModelsList(requireActivity())[0].kNumValue)
        createCheckBox()

        binding.kchartView.rvKlineType.adapter = kLineType

        kLineType.setOnItemClickListener { _, _, position ->
            StockDetailFragment.currentKLineType = position
            when (position) {
                0 -> { // 長線
                    cancelApiCall()
                    getStockHistoricalData(4, KNumGroupMaintain.getKNumModelsList(requireActivity())[0].kNumValue)
                    setBackgroungDrawable(R.drawable.solid_r10_orange_f63e02, 0, 0, 0)
                }
                1 -> { // 中縣
                    cancelApiCall()
                    getStockHistoricalData(3, KNumGroupMaintain.getKNumModelsList(requireActivity())[1].kNumValue)
                    setBackgroungDrawable(0, R.drawable.solid_r10_orange_f63e02, 0, 0)
                }
                2 -> { //短線
                    cancelApiCall()
                    getStockHistoricalData(2, KNumGroupMaintain.getKNumModelsList(requireActivity())[2].kNumValue)
                    setBackgroungDrawable(0, 0, R.drawable.solid_r10_orange_f63e02, 0)
                }
                3 -> { // 極短線
                    cancelApiCall()
                    getStockHistoricalData(1, KNumGroupMaintain.getKNumModelsList(requireActivity())[3].kNumValue)
                    setBackgroungDrawable(0, 0, 0, R.drawable.solid_r10_orange_f63e02)
                }
            }

        }

        binding.kchartView.btnD.setOnClickListener {
            resetAverageData()
            getTrendLineData()

        }
        return binding.root
    }

    private fun initView() {
        binding.kchartView.nestedScrollView2.visibility = View.GONE
        val linearLayoutManager2: LinearLayoutManager = object : LinearLayoutManager(activity, HORIZONTAL, false) {
            //禁止水平滑动
            override fun canScrollHorizontally(): Boolean {
                return false
            }
        }
        binding.kchartView.rvKlineType.layoutManager = linearLayoutManager2
        binding.kchartView.lyCheckboxContainer.visibility = View.GONE
        stockID = StockDetailFragment.stockID
    }

    private fun setBackgroungDrawable(drawable1: Int, drawable2: Int, drawable3: Int, drawable4: Int) {
        kLineType.getViewByPosition(0, R.id.tv_stock_DetailCategory)!!.tv_stock_DetailCategory.setBackgroundResource(drawable1)
        kLineType.getViewByPosition(1, R.id.tv_stock_DetailCategory)!!.tv_stock_DetailCategory.setBackgroundResource(drawable2)
        kLineType.getViewByPosition(2, R.id.tv_stock_DetailCategory)!!.tv_stock_DetailCategory.setBackgroundResource(drawable3)
        kLineType.getViewByPosition(3, R.id.tv_stock_DetailCategory)!!.tv_stock_DetailCategory.setBackgroundResource(drawable4)
    }

    //------------------------------------------Stock Data function  ----------------------------------------
    /**
     * @param timeInterval 0 - 1min    ;
     * 1 - min30 (極短線)     ; 2 - day1 (短線)
     * 3 - day5 (中線)       ; 4 - day20 (長線)
     */
    private fun getStockHistoricalData(timeInterval: Int, kNum: Int) {

        var timeIntervalString = "day20"
        when (timeInterval) {
            4 -> timeIntervalString = "day20"
            3 -> timeIntervalString = "day5"
            2 -> timeIntervalString = "day1"
            1 -> timeIntervalString = "min30"
        }

        currentKLineType = timeInterval
        callStockHistoricalData = RetrofitInstance.getDfjApiInstance2().getStockHistoricalData(stockID, timeIntervalString, kNum)

        callStockHistoricalData.enqueue(object : Callback<ApiObjectStockHistoricalData> {
            override fun onResponse(call: Call<ApiObjectStockHistoricalData>, response: Response<ApiObjectStockHistoricalData>) {
                if (response.isSuccessful) {
                    objectStockHistoricalData = response.body()
                    initKLineView()
                    initData()
                    mKChartView!!.notifyChanged()

                } else {
                    try {
                        Toast.makeText(context, "無歷史資料", Toast.LENGTH_LONG).show()
                    } catch (e: Exception) {
                        Toast.makeText(context, e.message, Toast.LENGTH_LONG).show()
                    }
                }
            }

            override fun onFailure(call: Call<ApiObjectStockHistoricalData>, t: Throwable) {
            }
        })
    }

    /** DFJ 專屬趨勢線  */
    private fun getTrendLineData() {
        /** BaseKChartView.mStartIndex 螢幕最左邊的那根 K 棒 index
         *  BaseKChartView.mStopIndex螢幕最右邊的那根 K 棒 index
         */
        val startIdx = objectStockHistoricalData!!.data[BaseKChartView.mStartIndex].index
        val endIdx = objectStockHistoricalData!!.data[BaseKChartView.mStopIndex].index
        val startKDateTime = objectStockHistoricalData!!.data[BaseKChartView.mStartIndex].kDatetime
        val endKDateTime = objectStockHistoricalData!!.data[BaseKChartView.mStopIndex].kDatetime

        var timeInterval = "day20"
        when (currentKLineType) {
            4 -> timeInterval = "day20"
            3 -> timeInterval = "day5"
            2 -> timeInterval = "day1"
            1 -> timeInterval = "min30"
        }

        RetrofitInstance.getDfjApiInstance2().getAllStockDFJTrendLine(stockID, timeInterval,
            startKDateTime, endKDateTime, 1).enqueue(object : Callback<ApiObjectStockDFJTrendLine> {
            override fun onResponse(call: Call<ApiObjectStockDFJTrendLine>, response: Response<ApiObjectStockDFJTrendLine>) {
                if (response.isSuccessful) {
                    val objectStockTrendLineData = response.body()
                    if (objectStockTrendLineData!!.data.isNotEmpty()) {
                        splitUpDownTrendLinesData(objectStockTrendLineData)
                    }
                }

            }

            override fun onFailure(call: Call<ApiObjectStockDFJTrendLine>, t: Throwable) {
            }
        })
    }

    private fun splitUpDownTrendLinesData(objectStockDFJTrendLine: ApiObjectStockDFJTrendLine) {
        // 畫黃色均線
        objectStockUpTrendLine.clear()
        objectStockDownTrendLine.clear()

        for (i in objectStockDFJTrendLine.data.indices) {
            when (objectStockDFJTrendLine.data[i].name) {
                "uptrendlines-1-level-1" -> {
                    objectStockUpTrendLine.add(objectStockDFJTrendLine.data[i])
                    // 第二個 For  跑該Level 有多少個小段趨勢線
                    for (j in objectStockDFJTrendLine.data[i].items.indices) {
                        // 第三個 For . 找每個小段的起始 k 棒的index 在陣列存的位子
                        for (k in objectStockHistoricalData!!.data.indices) {
                            if (objectStockHistoricalData!!.data[k].index == objectStockDFJTrendLine.data[i].items[j][0].index) {
                                calculateDfjRiseMA(data, k)
                                break
                            }
                        }
                    }
                }

                "downtrendlines-1-level-1" -> {
                    objectStockDownTrendLine.add(objectStockDFJTrendLine.data[i])
                    for (j in objectStockDFJTrendLine.data[i].items.indices) {
                        // 第三個 For . 找每個小段的起始 k 棒的index 在陣列存的位子
                        for (k in objectStockHistoricalData!!.data.indices) {
                            if (objectStockHistoricalData!!.data[k].index == objectStockDFJTrendLine.data[i].items[j][0].index) {
                                calculateDfjFallMA(data, k)
                                break
                            }
                        }
                    }
                }
            }
        }

        mKChartView!!.setIsKChart(true, objectStockUpTrendLine, objectStockDownTrendLine)
        mKChartView!!.notifyChanged()
    }

    private fun initKLineView() {
        KViewOrientationListenerManager.getInstance().registerListener(this)
        mAdapter = KChartAdapter()
        //------------------------------------------
        mKChartView!!.setPointWidth(10f)
        mKChartView!!.setCandleLineWidth(2f)
        mKChartView!!.setCandleWidth(8f)
        mKChartView!!.setKChildViewPillarWidth(4)
        //------------------------------------------
        mKChartView!!.setIsSmartGraph(false)
        mKChartView!!.adapter = mAdapter
        mKChartView!!.setGridRows(4) //  設定背景畫面切幾行
        mKChartView!!.setGridColumns(4) //  設定背景畫面切幾列
        mKChartView!!.setIsNeedHorizontalLine(false, null, null)
        mKChartView!!.setIsKChart(true, null, null)
        if (currentKLineType == 1) {
            mKChartView!!.dateTimeFormatter = TimeFormatter2()
        } else {
            mKChartView!!.dateTimeFormatter = DateFormatter()
        }

        // 監聽十字線定位
        // mKChartView!!.setOnSelectedChangedListener { view, point, index -> val data = point as KLineEntity }
    }

    private fun initData() {
        mKChartView!!.showLoading()
        Thread {
            data = DataRequest().getALL(requireContext().applicationContext, objectStockHistoricalData)
            requireActivity().runOnUiThread {
                mAdapter!!.addFooterData(data)
                mKChartView!!.startAnimation()
                mKChartView!!.refreshEnd()
            }
        }.start()
    }

    private fun createCheckBox() {
        val list = ArrayList<String>()
        list.add("MA5")
        list.add("MA10")
        list.add("MA20")
        val scale = this.resources.displayMetrics.density
        val arrayOfCheckBox = ArrayList<CheckBox>()
        for (i in list.indices) {
            val checkBox = CheckBox(activity)
            checkBox.id = i
            checkBox.text = list[i]
            checkBox.tag = list[i]
            checkBox.isChecked = true
            checkBox.setTextColor(ContextCompat.getColor(requireActivity(), R.color.WhiteFFFFFF))
            checkBox.setButtonDrawable(R.drawable.selector_checkbox)
            //  checkBox  icon 與 text 距離
            checkBox.setPadding(checkBox.paddingLeft + (10.0f * scale + 0.5f).toInt(),
                checkBox.paddingTop + (1f * scale).toInt(),
                checkBox.paddingRight,
                checkBox.paddingBottom + (1f * scale).toInt())
            checkBox.setOnCheckedChangeListener { buttonView: CompoundButton, isChecked: Boolean ->
                when (buttonView.tag.toString()) {
                    "MA5" -> if (isChecked) mKChartView!!.setMa5Color(ContextCompat.getColor(
                        requireActivity(), R.color.chart_ma5)) else mKChartView!!.setMa5Color(ContextCompat.getColor(requireActivity(),
                        android.R.color.transparent))
                    "MA10" -> if (isChecked) mKChartView!!.setMa5Color(ContextCompat.getColor(requireActivity(),
                        R.color.chart_ma10)) else mKChartView!!.setMa5Color(ContextCompat.getColor(
                        requireActivity(), android.R.color.transparent))
                    "MA20" -> if (isChecked) mKChartView!!.setMa5Color(ContextCompat.getColor(requireActivity(),
                        R.color.chart_ma20)) else mKChartView!!.setMa5Color(ContextCompat.getColor(
                        requireActivity(), android.R.color.transparent))
                }
                mKChartView!!.notifyChanged()
            }
            arrayOfCheckBox.add(checkBox)
            val params = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            params.leftMargin = (16 * scale).toInt()
            binding.kchartView.lyCheckboxContainer.addView(checkBox, params)
        }
    }

    private fun cancelApiCall() {
        if (callStockHistoricalData != null) {
            callStockHistoricalData!!.cancel()
        }
        apiCallFinishedCount = 0
    }

    private fun setKLineLayoutParam(view: View) {
        val constraintLayout: ConstraintLayout = view.findViewById(R.id.cv_Kline)
        val constraintSet = ConstraintSet()
        constraintSet.clone(constraintLayout)
        constraintSet.connect(R.id.kchart_view, ConstraintSet.BOTTOM, ConstraintSet.PARENT_ID, ConstraintSet.BOTTOM, 0)
        constraintSet.connect(R.id.kchart_view, ConstraintSet.TOP, R.id.rv_klineType, ConstraintSet.BOTTOM, 0)
        constraintSet.applyTo(constraintLayout)
    }

    override fun notifyChangeParam(orientation: String) {
        when (orientation) {
            "PORTRAIT" -> {
                mKChartView!!.setPointWidth(10f)
                mKChartView!!.setCandleLineWidth(2f)
                mKChartView!!.setCandleWidth(8f)
                mKChartView!!.setKChildViewPillarWidth(4)
                binding.kchartView.btnD.visibility = View.GONE
            }
            "LANDSCAPE" -> {
                mKChartView!!.setPointWidth(4f)
                mKChartView!!.setCandleLineWidth(1f)
                mKChartView!!.setCandleWidth(3f)
                mKChartView!!.setKChildViewPillarWidth(1)
                binding.kchartView.btnD.visibility = View.VISIBLE
            }
        }
        mKChartView!!.notifyChanged()
    }

    override fun loadComplete() {
        if (apiCallFinishedCount != 1) {
            apiCallFinishedCount++
        } else {
            mKChartView!!.setIsKChart(true, objectStockUpTrendLine, objectStockDownTrendLine)
            mKChartView!!.notifyChanged()
        }
    }

    override fun onStop() {
        super.onStop()
        cancelApiCall()
        StockDetailFragment.currentKLineType = 0
        KNumListenerManager.getInstance().unRegisterListener(this)
        TrendLineListenerManager.getInstance().unRegisterListener(this)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        mKChartView!!.adapter = null
    }

    override fun onDestroy() {
        super.onDestroy()
        KNumListenerManager.getInstance().unRegisterListener(this)
        TrendLineListenerManager.getInstance().unRegisterListener(this)
    }

    //------------------------------------------------收到監聽到做的事------------------------------------------------------------------
    // 改變 KNum 後通知重新整理畫面
    override fun notifyRefreshView(hasChange: Boolean) {
        apiCallFinishedCount = 0
        if (hasChange) {
            when (StockDetailFragment.currentKLineType) {
                0 ->                     // 長線
                    getStockHistoricalData(4, KNumGroupMaintain.getKNumModelsList(activity)[0].kNumValue)
                1 ->                     // 中縣
                    getStockHistoricalData(3, KNumGroupMaintain.getKNumModelsList(activity)[1].kNumValue)
                2 ->                     //短線
                    getStockHistoricalData(2, KNumGroupMaintain.getKNumModelsList(activity)[2].kNumValue)
                3 ->                     // 極短線
                    getStockHistoricalData(1, KNumGroupMaintain.getKNumModelsList(activity)[3].kNumValue)
            }
        }
    }

    // 點擊右上畫 DFJ 趨勢線按鈕後.通知重新整理畫面( Call API and draw  TendLine )
    override fun notifyTrendLineData() {
        resetAverageData()
        getTrendLineData()
    }
    //-------------------------------------------  DFJ 黃色均線的  function -------------------------------------
    /**
     * 求上升均線各 index closePrice平均值
     */
    private fun calculateDfjRiseMA(datas: List<KLineEntity>, AverageStartPoint: Int) {
        var averageValue = 0f
        //  求每根 K棒的平均值，並將之加入至該 point 的資料陣列
        for (i in AverageStartPoint until datas.size) {
            val point = datas[i]
            val closePrice = point.closePrice
            averageValue += closePrice
            point.dfjRiseAverageDataList.add(averageValue / (i - AverageStartPoint + 1))
        }
    }

    /**
     * 求下降均線各 index closePrice平均值
     */
    private fun calculateDfjFallMA(datas: List<KLineEntity>, AverageStartPoint: Int) {
        var averageValue = 0f
        //  求每根 K棒的平均值，並將之加入至該 point 的資料陣列
        for (i in AverageStartPoint until datas.size) {
            val point = datas[i]
            val closePrice = point.closePrice
            averageValue += closePrice
            point.dfjFallAverageDataList.add(averageValue / (i - AverageStartPoint + 1))
        }
    }

    private fun resetAverageData() {
        for (i in data.indices) {
            val point = data[i]
            point.dfjRiseAverageDataList.removeAll(point.dfjRiseAverageDataList)
            point.dfjFallAverageDataList.removeAll(point.dfjFallAverageDataList)
        }
    }

}