package com.gcreate.dfjStock.view.dialog

import android.annotation.SuppressLint
import android.graphics.Color
import com.gcreate.dfjStock.global
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.ViewGroup
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import com.gcreate.dfjStock.R
import com.gcreate.dfjStock.databinding.DialogCanlendarAlertBinding

class CalendarAlertDialogFragment : DialogFragment() {

    override fun onStart() {
        super.onStart()
        if (dialog != null) {
            dialog!!.window!!.setLayout((global.windowWidth * 0.81).toInt(), (global.windowHeigh * 0.27).toInt())
            dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
    }

    @SuppressLint("SetTextI18n")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val binding: DialogCanlendarAlertBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_canlendar_alert, container, false)
        if (arguments != null) {
            when (requireArguments().getInt("timeCompareResult")) {
                1 -> binding.tvDialogDescription.text = "起訖時間範圍錯誤，\n 請重新選擇搜尋範圍。"
                3 -> binding.tvDialogDescription.text = "日期選擇區間請在五日內"
            }
        }
        binding.btnOk.setOnClickListener { dialog!!.dismiss() }
        return binding.root
    }
}