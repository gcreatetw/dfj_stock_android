package com.gcreate.dfjStock.view.dialog

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnFocusChangeListener
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import com.gcreate.dfjStock.Listener.UpdateUserNameListenerManager
import com.gcreate.dfjStock.R
import com.gcreate.dfjStock.databinding.ComponentGroupMaintainBinding
import com.gcreate.dfjStock.global
import com.gcreate.dfjStock.view.LoginPageActivity
import com.gcreate.dfjStock.webAPI.ApiObjectUpdateUserName
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class EditMemberNameDialogFragment : DialogFragment() {
    /*  變更使用者名稱*/ //------------------------View Binding params-------------------------------------------------
    private lateinit var binding: ComponentGroupMaintainBinding

    override fun onStart() {
        super.onStart()
        if (dialog != null) {
            dialog!!.window!!.setLayout((global.windowWidth * 0.81).toInt(), (global.windowHeigh * 0.25).toInt())
            dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.component_group_maintain, container, false)

        initView()

        return binding.root
    }

    private fun initView() {
        binding.tvAlertTitle.text = "更改使用者名稱"
        binding.editGroupName.hint = "請輸入使用者名稱"
        viewAction()
    }

    private fun viewAction() {

        binding.editGroupName.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_NEXT || actionId == EditorInfo.IME_ACTION_DONE) {
                hideSoftKeyBroad()
                true
            } else {
                false
            }
        }

        binding.editGroupName.onFocusChangeListener = OnFocusChangeListener { _: View?, hasFocus: Boolean ->
            if (hasFocus) {
                binding.editGroupName.setText("")
            } else {
                binding.editGroupName.clearFocus()
            }
        }

        binding.btnOk.setOnClickListener {
            if (binding.editGroupName.text.toString().trim { it <= ' ' }.isEmpty()) {
                binding.editGroupName.clearFocus()
                Toast.makeText(activity, "名稱不能空白", Toast.LENGTH_SHORT).show()
            } else {
                UpdateUserNameListenerManager.getInstance().sendBroadCast(binding.editGroupName.text.toString())
                binding.editGroupName.clearFocus()
                updateUserName()
                dialog!!.dismiss()
            }
        }

        binding.btnCancel.setOnClickListener { dialog!!.dismiss() }
    }

    private fun updateUserName() {
        val paramObject = JsonObject()
        paramObject.addProperty("user_id", LoginPageActivity.objectLoginFeedback!!.data.user_id)
        paramObject.addProperty("new_name", binding.editGroupName.text.toString())
        val call = global.gcAPI.updateUserName(paramObject)
        call.enqueue(object : Callback<ApiObjectUpdateUserName?> {
            override fun onResponse(call: Call<ApiObjectUpdateUserName?>, response: Response<ApiObjectUpdateUserName?>) {
                val objectUpdateUserName = response.body()
            }

            override fun onFailure(call: Call<ApiObjectUpdateUserName?>, t: Throwable) {}
        })
    }

    private fun hideSoftKeyBroad() {
        val imm = (requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
        imm.hideSoftInputFromWindow(binding.root.windowToken, 0)
        binding.editGroupName.clearFocus()
    }
}