package com.gcreate.dfjStock.view.search

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.LinearLayout
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.gcreate.dfjStock.R
import com.gcreate.dfjStock.adapter.search.SecuritiesItemAdapter
import com.gcreate.dfjStock.databinding.FragmentCategorySecuritiesclassificationItemBinding
import com.gcreate.dfjStock.global
import com.gcreate.dfjStock.webAPI.RetrofitInstance
import com.gcreate.dfjStock.webAPI.search.ApiObjectSearchResult
import com.gcreate.dfjStock.webAPI.tab2.ApiObjectAllStockClassification
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SecuritiesItemFragment : Fragment() {
    /*  篩選股票(第二層)  SecuritiesClassificationItem  */
    private lateinit var binding: FragmentCategorySecuritiesclassificationItemBinding
    private lateinit var navController: NavController
    private val args: SecuritiesItemFragmentArgs by navArgs()

    companion object{
        lateinit var securitiesItemsBeans : ApiObjectAllStockClassification.ItemsBean.SecuritiesClassificationItemsBean
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_category_securitiesclassification_item, container, false)
        navController = NavHostFragment.findNavController(this)

        initView()
//        viewOnClickAction()

        return binding.root
    }

    private fun initView() {

        binding.securityToolbar.apply {
            toolbarArrowUp.visibility = View.GONE
            toolbarArrowDown.visibility = View.GONE
            toolbarTitle.text = args.category
            toolbar.title = ""
            toolbar.setNavigationIcon(R.drawable.arrow_icon_back)
            toolbar.setNavigationOnClickListener { requireActivity().onBackPressed() }
        }
        val itemsBean = null
//        val itemsBean = MainActivity.objectAllStockClassification!!.items!![0].securitiesClassificationItems!!

        binding.rvContent.componentRv.layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)

        val adapter = SecuritiesItemAdapter(itemsBean)
        binding.rvContent.componentRv.adapter = adapter

        adapter.setOnItemClickListener { _, _, position ->
//            securitiesItemsBeans = itemsBean[position]
//            val bundle = Bundle()
//            bundle.putString("category", itemsBean[position].description)
//            bundle.putInt("exchangeItemID", args.exchangeItemID)
//            bundle.putInt("securitiesClassificationItemID", itemsBean[position].id)
//
//            if (position == 3) {
//                navController.navigate(R.id.action_securitiesClassificationItemFragment_to_TrendLineDataFragment, bundle)
//            } else {
//                navController.navigate(R.id.action_fragment_StockCategory_SecuritiesClassificationItem_to_fragment_StockCategory_MarketItem,
//                    bundle)
//            }
        }

        autoSearch()

//        global.setEditTextInputSpace(binding.autoCompleteTxt.searchEditTxt)

        val params = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
        params.setMargins(0, 12, 0, 0)
        binding.rvContent.componentRv.layoutParams = params
    }

//    private fun viewOnClickAction() {
//        binding.autoCompleteTxt.imgSearch.setOnClickListener {
//            hideSoftKeyBroad()
//            binding.autoCompleteTxt.searchEditTxt.clearFocus()
//            if (binding.autoCompleteTxt.searchEditTxt.text.toString().trim { it <= ' ' }.isNotEmpty()) {
//                getSingleStock(binding.autoCompleteTxt.searchEditTxt.text.toString())
//            }
//        }

//        binding.autoCompleteTxt.searchEditTxt.setOnEditorActionListener { _, actionId, _ ->
//
//            if (actionId == EditorInfo.IME_ACTION_NEXT || actionId == EditorInfo.IME_ACTION_DONE) {
//                hideSoftKeyBroad()
//                binding.autoCompleteTxt.searchEditTxt.clearFocus()
//                if (binding.autoCompleteTxt.searchEditTxt.text.toString().trim { it <= ' ' }.isNotEmpty()) {
//                    getSingleStock(binding.autoCompleteTxt.searchEditTxt.text.toString())
//                }
//                true
//            } else {
//                false
//            }
//        }
//    }

    private fun autoSearch() {
       // val searchItemAdapter = SearchItemAdapter(requireActivity(), ExchangeItemFragment.allStocks)
//        binding.autoCompleteTxt.autoCompleteTxt.setOnEditorActionListener(this)
       // binding.autoCompleteTxt.autoCompleteTxt.setAdapter(searchItemAdapter)
    }

    private fun getSingleStock(queryText: String) {
        RetrofitInstance.getDfjApiInstance2().getSearchStock(queryText).enqueue(object : Callback<ApiObjectSearchResult> {
            override fun onResponse(call: Call<ApiObjectSearchResult>, response: Response<ApiObjectSearchResult>) {
                if (response.isSuccessful) {
                    val searchResult = response.body()
                    if (searchResult!!.message == "stocks is not found"){
                        Toast.makeText(activity, "查無此股票", Toast.LENGTH_SHORT).show()
                    }else {
                        val bundle = Bundle()
                        bundle.putString("stockName", searchResult.data!![0].description)
                        bundle.putString("stockSymbol", searchResult.data[0].symbol)
                        bundle.putInt("stockID", searchResult.data[0].id)
                        navController.navigate(R.id.action_securitiesClassificationItemFragment_to_fragment_StockDetail, bundle)
                    }
                }
            }

            override fun onFailure(call: Call<ApiObjectSearchResult>, t: Throwable) {
                Toast.makeText(activity, "連線失敗，請檢察網際網路", Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun hideSoftKeyBroad() {
        val imm = (requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
        imm.hideSoftInputFromWindow(binding.root.windowToken, 0)
    }

}