package com.gcreate.dfjStock.view.dialog

import android.app.ActionBar
import android.content.ActivityNotFoundException
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.gcreate.dfjStock.databinding.DialogfragmentVersionCheckBinding

class VersionCheckDialogFragment : DialogFragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {

        val binding = DialogfragmentVersionCheckBinding.inflate(inflater, container, false)

        binding.btnCheck.setOnClickListener {
            val appPackageName = requireActivity().packageName
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")))
        }

        return binding.root

    }

    override fun onStart() {
        super.onStart()
        isCancelable = false
        val dialog = dialog
        if (dialog != null) {
            dialog.window!!.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT)
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
    }
}