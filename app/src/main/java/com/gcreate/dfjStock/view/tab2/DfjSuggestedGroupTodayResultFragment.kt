package com.gcreate.dfjStock.view.tab2

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.gcreate.dfjStock.Listener.InterruptCallControlListener
import com.gcreate.dfjStock.R
import com.gcreate.dfjStock.databinding.FragmentDfjSuggestedGroupResultBinding
import com.gcreate.dfjStock.viewModel.tab2.DfjSuggestedFetchResultModelFactory
import com.gcreate.dfjStock.viewModel.tab2.DfjSuggestedItemModel
import com.gcreate.dfjStock.viewModel.tab2.DfjSuggestedTodayStockModel
import com.gcreate.dfjStock.webAPI.tab2.ApiObjectSuggestedConditionResult
import retrofit2.Call

class DfjSuggestedGroupTodayResultFragment : Fragment(), InterruptCallControlListener {
    /** 智慧選股細項分類*/
    private var call: Call<ApiObjectSuggestedConditionResult>? = null
    private var lastOffset = 0
    private var lastPosition = 0

    private lateinit var binding: FragmentDfjSuggestedGroupResultBinding
    private lateinit var navController: NavController

    private val args: DfjSuggestedGroupTodayResultFragmentArgs by navArgs()
    private var viewModel: DfjSuggestedTodayStockModel? = null

    companion object {
        //--------------------------------------------------------------------------------------------------
        private var objectSuggestedConditionResult: ApiObjectSuggestedConditionResult? = null
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_dfj_suggested_group_result, container, false)
        navController = NavHostFragment.findNavController(this)

        getModelInstance()
        setupBinding(makeApiCall())
        initView()


        return binding.root
    }


    private fun getModelInstance(): DfjSuggestedTodayStockModel? {
        if (viewModel == null) {
            viewModel =
                ViewModelProvider(requireActivity(),DfjSuggestedFetchResultModelFactory(binding, navController)).get(DfjSuggestedTodayStockModel::class.java)
        }

        return viewModel
    }


    private fun setupBinding(viewModel: DfjSuggestedTodayStockModel) {
        binding.executePendingBindings()
        binding.rvContent.componentRv.apply {
            layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
            adapter = viewModel.getAdapter()
            hasFixedSize()
        }

    }

    @SuppressLint("InflateParams")
    private fun makeApiCall(): DfjSuggestedTodayStockModel {

        viewModel!!.getRecyclerListDataObserver().observe(requireActivity(), {
            if (it != null)
                viewModel!!.setAdapterData(it.data!!)
        })

        callApiData()

        return viewModel!!
    }

    private fun callApiData() {
        DfjSuggestedItemModel.callCancel()
        viewModel!!.getApiData(args.suggestedCategoryID, args.exChangeID, args.marketID, args.industrialID)
    }

    private fun initView() {
        //  ToolBar
        binding.include.toolbarTitle.text = args.suggestedCategory
        binding.include.toolbarArrowUp.visibility = View.GONE
        binding.include.toolbarArrowDown.visibility = View.GONE
        binding.include.toolbar.title = ""
        binding.include.toolbar.setNavigationIcon(R.drawable.arrow_icon_back)
        binding.include.toolbar.setNavigationOnClickListener { requireActivity().onBackPressed() }
        binding.rvContent.componentRv.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        // 監聽RecyclerView滾動狀態
        binding.rvContent.componentRv.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (recyclerView.layoutManager != null) {
                    positionAndOffset
                }
            }
        })
    }

    override fun notifyViewCancelCall() {
        if (call != null) {
            call!!.cancel()
        }
        if (activity != null) {
            Toast.makeText(activity, "中斷搜尋", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onStart() {
        super.onStart()
        scrollToPosition()
    }

    override fun onDestroy() {
        super.onDestroy()

        objectSuggestedConditionResult = null
    }
    //------------------------------------------保存離開 Fragment时的瀏覽位置-----------------------------------------
    // 獲取與該view的頂部的偏移量
    // 得到該View的數組位置
    // 獲取可視的第一個 view
    /**
     * 紀錄RecyclerView當前位置
     */
    private val positionAndOffset: Unit
        get() {
            val layoutManager = binding.rvContent.componentRv.layoutManager as LinearLayoutManager?
            // 獲取可視的第一個 view
            val topView = layoutManager!!.getChildAt(0)
            if (topView != null) {
                //獲取與該view的頂部的偏移量
                lastOffset = topView.top
                //得到該View的數組位置
                lastPosition = layoutManager.getPosition(topView)
            }
        }

    /**
     * 讓RecyclerView滾動到指定位置
     */
    private fun scrollToPosition() {
        if (binding.rvContent.componentRv.layoutManager != null && lastPosition >= 0) {
            (binding.rvContent.componentRv.layoutManager as LinearLayoutManager?)!!.scrollToPositionWithOffset(lastPosition, lastOffset)
        }
    }

}