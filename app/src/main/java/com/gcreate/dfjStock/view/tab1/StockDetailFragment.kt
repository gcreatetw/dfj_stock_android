package com.gcreate.dfjStock.view.tab1

import android.annotation.SuppressLint
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.*
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.gcreate.dfjStock.Listener.TrendLineListenerManager
import com.gcreate.dfjStock.R
import com.gcreate.dfjStock.adapter.tab1.StockDetailGraphTypeAdapter
import com.gcreate.dfjStock.databinding.FragmentStockDetailBinding
import com.gcreate.dfjStock.global
import com.gcreate.dfjStock.model.FakeData
import com.gcreate.dfjStock.view.LoginPageActivity
import com.gcreate.dfjStock.view.dialog.AddStockToSelectedGroupDialogFragment
import com.gcreate.dfjStock.view.dialog.LoadingDialog
import com.gcreate.dfjStock.view.dialog.ModifyKNumDialogFragment
import kotlinx.android.synthetic.main.card_stock_graph_item.view.*

@SuppressLint("SourceLockedOrientationActivity")
class StockDetailFragment : Fragment() {
    /**單一股票-外層 */
    private lateinit var navController: NavController
    private val args: StockDetailFragmentArgs by navArgs()
    private var menu: Menu? = null
    private val graphType = StockDetailGraphTypeAdapter(FakeData.KChartGraphType())

    companion object {
        private lateinit var binding: FragmentStockDetailBinding

        var currentStockDetailFragment = ""

        @JvmField
        var currentKLineType = 0
        private var currentGraphType = 0

        fun setCurrentFragmentName(currentStockDetailFragment: String) {
            this.currentStockDetailFragment = currentStockDetailFragment
        }

        var stockID = 0
        var stockSymbol = ""
        var stockName = ""

        fun setViewHideOrShow(toolbarVisible: Int, kLineTypeVisible: Int) {
            binding.toolbar.clToolbar.visibility = toolbarVisible
            binding.rvContent.componentRv.visibility = kLineTypeVisible
        }

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_stock_detail, container, false)
        navController = NavHostFragment.findNavController(this)

        val localNavHost = childFragmentManager.findFragmentById(R.id.nav_kchart_container) as NavHostFragment
        val graph = localNavHost.navController.navInflater.inflate(R.navigation.nav_kchart)

        stockID = args.stockID
        stockSymbol = args.stockSymbol!!
        stockName = args.stockName!!

        setCurrentFragmentName("Fragment_StockDetail_KChart")
        initView()

        binding.rvContent.componentRv.adapter = graphType

        graphType.setOnItemClickListener { _, _, position ->

            when (position) {
                0 -> {
                    if (position != currentGraphType) {
                        currentGraphType = position
                        setCurrentFragmentName("Fragment_StockDetail_KChart")
                        requireActivity().requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_SENSOR
                        toolbarViewControl(View.GONE, true)
                        graph.startDestination = R.id.stockDetailKChartFragment
                        localNavHost.navController.graph = graph
                        underLineHideShow(View.VISIBLE, View.INVISIBLE, View.INVISIBLE)
                    }
                }

                1 -> {
                    if (position != currentGraphType) {
                        currentGraphType = position
                        setCurrentFragmentName("fragment_StockDetail_SmartAnalysis")
                        requireActivity().requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
                        toolbarViewControl(View.VISIBLE, false)
                        graph.startDestination = R.id.fragment_StockDetail_SmartAnalysis
                        localNavHost.navController.graph = graph
                        underLineHideShow(View.INVISIBLE, View.VISIBLE, View.INVISIBLE)
                    }
                }

                2 -> if (LoginPageActivity.objectLoginFeedback!!.data.is_vip) {
                    if (position != currentGraphType) {
                        currentGraphType = position
                        setCurrentFragmentName("fragment_StockDetail_SmartGraph")
                        requireActivity().requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
                        toolbarViewControl(View.VISIBLE, false)
                        graph.startDestination = R.id.fragment_StockDetail_SmartGraph
                        localNavHost.navController.graph = graph
                        underLineHideShow(View.INVISIBLE, View.INVISIBLE, View.VISIBLE)
                    }
                } else {
                    // 非VIP 會員開啟網站畫面
                    global.alertDialog(activity)
                }


            }
            onPrepareOptionsMenu(menu!!)
        }

        return binding.root
    }

    private fun initView() {

        requireActivity().requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_SENSOR

        //  ToolBar
        toolbarViewControl(View.GONE, true)
        binding.toolbar.toolbar.setTitleTextColor(ContextCompat.getColor(requireActivity(), R.color.chart_white))
        binding.toolbar.toolbar.setNavigationIcon(R.drawable.arrow_icon_back)
        setHasOptionsMenu(true)
        (requireActivity() as AppCompatActivity?)!!.setSupportActionBar(binding.toolbar.toolbar)
        binding.toolbar.toolbar.setNavigationOnClickListener { requireActivity().onBackPressed() }

        //  Recyclerview ( K圖分析、智慧分析、智慧圖形)
        val params = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        params.setMargins(0, 0, 0, 0)
        val linearLayoutManager: LinearLayoutManager = object : LinearLayoutManager(activity, HORIZONTAL, false) {
            //禁止水平滑动
            override fun canScrollHorizontally(): Boolean {
                return false
            }
        }

        binding.rvContent.componentRv.layoutParams = params
        binding.rvContent.componentRv.isNestedScrollingEnabled = false
        binding.rvContent.componentRv.layoutManager = linearLayoutManager
    }

    private fun toolbarViewControl(isTitleCenter: Int, isTitleLeft: Boolean) {
        binding.toolbar.toolbarArrowUp.visibility = View.GONE
        binding.toolbar.toolbarArrowDown.visibility = View.GONE
        binding.toolbar.toolbarTitle.visibility = isTitleCenter
        binding.toolbar.toolbarTitle.text = stockName

        if (isTitleLeft) {
            binding.toolbar.toolbar.title = stockName
        } else {
            binding.toolbar.toolbar.title = ""
        }
    }

    private fun underLineHideShow(isShowUnderLine1: Int, isShowUnderLine2: Int, isShowUnderLine3: Int) {
        graphType.getViewByPosition(0, R.id.img_underLine)!!.img_underLine.visibility = isShowUnderLine1
        graphType.getViewByPosition(1, R.id.img_underLine)!!.img_underLine.visibility = isShowUnderLine2
        graphType.getViewByPosition(2, R.id.img_underLine)!!.img_underLine.visibility = isShowUnderLine3
    }


    //------------------------------------------Menu function  -----------------------------------------
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        this.menu = menu
        inflater.inflate(R.menu.stockgroupmenu, menu)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        val modifyKNumIcon = menu.findItem(R.id.toolbarMenuItem_modifyKNum)
        val drawTendLineIcon = menu.findItem(R.id.toolbarMenuItem_DFJTrendLine)
        if (currentGraphType != 0) {
            modifyKNumIcon.isVisible = false
            drawTendLineIcon.isVisible = false
        } else {
            modifyKNumIcon.isVisible = true
            drawTendLineIcon.isVisible = true
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val bundle = Bundle()
        when (item.itemId) {
            R.id.toolbarMenuItem_Search -> {
                bundle.putInt("stockID", stockID)
                val addStockToSelectedGroup: DialogFragment = AddStockToSelectedGroupDialogFragment()
                addStockToSelectedGroup.arguments = bundle
                addStockToSelectedGroup.show(requireActivity().supportFragmentManager, "simple dialog")
            }
            R.id.toolbarMenuItem_modifyKNum -> {
                val modifyKNumDialogFragment: DialogFragment = ModifyKNumDialogFragment()
                bundle.putInt("KNumListPosition", currentKLineType)
                modifyKNumDialogFragment.arguments = bundle
                modifyKNumDialogFragment.show(requireActivity().supportFragmentManager, "simple dialog")
            }
            R.id.toolbarMenuItem_DFJTrendLine -> {
                TrendLineListenerManager.getInstance().sendBroadCast()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onStop() {
        super.onStop()
        currentGraphType = 0
        requireActivity().requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
    }

}