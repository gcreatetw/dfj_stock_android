package com.gcreate.dfjStock.view.tab2

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.gcreate.dfjStock.R
import com.gcreate.dfjStock.databinding.FragmentDfjSuggestedGroupHistoricalResultBinding
import com.gcreate.dfjStock.viewModel.tab2.DfjSuggestedHistoryFetchResultModelFactory
import com.gcreate.dfjStock.viewModel.tab2.DfjSuggestedHistoryStockModel
import com.gcreate.dfjStock.viewModel.tab2.DfjSuggestedItemModel

class DfjSuggestedGroupHistoricalResultFragment : Fragment() {

    private lateinit var binding: FragmentDfjSuggestedGroupHistoricalResultBinding
    private lateinit var navController: NavController

    private val args: DfjSuggestedGroupHistoricalResultFragmentArgs by navArgs()
    private var viewModel: DfjSuggestedHistoryStockModel? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_dfj_suggested_group_historical_result, container, false)
        navController = NavHostFragment.findNavController(this)

        getModelInstance()
        setupBinding(makeApiCall())

        initView()

        return binding.root
    }

    private fun getModelInstance(): DfjSuggestedHistoryStockModel? {
        if (viewModel == null) {
            viewModel =
                ViewModelProvider(requireActivity(), DfjSuggestedHistoryFetchResultModelFactory(binding, navController)).get(
                    DfjSuggestedHistoryStockModel::class.java)
        }

        return viewModel
    }


    private fun setupBinding(viewModel: DfjSuggestedHistoryStockModel) {
        binding.executePendingBindings()
        binding.rvExpandingContent.adapter = viewModel.getAdapter()
    }

    @SuppressLint("InflateParams")
    private fun makeApiCall(): DfjSuggestedHistoryStockModel {

        viewModel!!.getRecyclerListDataObserver().observe(requireActivity(), {
            if (it != null)
                viewModel!!.setAdapterData(it.data!!)
        })

        callApiData()

        return viewModel!!
    }

    private fun callApiData() {
        DfjSuggestedItemModel.callCancel()
        viewModel!!.getApiData(args.isSelectedFirstButton,
            args.suggestedCategoryID,
            args.startDate!!,
            args.endDate!!,
            args.exChangeID,
            args.marketID,
            args.industrialID)
    }

    private fun initView() {
        //  ToolBar
        binding.include.toolbarTitle.text = "當日選股 / 當日新增"
        binding.include.toolbarArrowUp.visibility = View.GONE
        binding.include.toolbarArrowDown.visibility = View.GONE
        binding.include.toolbar.title = ""
        binding.include.toolbar.setNavigationIcon(R.drawable.arrow_icon_back)
        binding.include.toolbar.setNavigationOnClickListener { requireActivity()!!.onBackPressed() }
        binding.rvExpandingContent.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        binding.tvDfjSuggestedHistoricalTitle.text = args.suggestedCategory
    }

}