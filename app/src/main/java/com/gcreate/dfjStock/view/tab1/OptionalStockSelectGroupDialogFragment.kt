package com.gcreate.dfjStock.view.tab1

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.gcreate.dfjStock.R
import com.gcreate.dfjStock.adapter.tab1.OptionalStockSelectGroupAdapter
import com.gcreate.dfjStock.databinding.DialogfragmentSelectstockgroupBinding
import com.gcreate.dfjStock.global
import com.gcreate.dfjStock.view.LoginPageActivity

class OptionalStockSelectGroupDialogFragment : DialogFragment() {

    private lateinit var binding: DialogfragmentSelectstockgroupBinding

    override fun onStart() {
        super.onStart()
        val dialog = dialog
        if (dialog != null) {
            dialog.window!!.setGravity(Gravity.BOTTOM)
            dialog.window!!.setLayout(global.windowWidth, (global.windowHeigh * 0.46).toInt())
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialogfragment_selectstockgroup, container, false)

        binding.rvStockGroupList.apply {
            layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
            adapter = OptionalStockSelectGroupAdapter(dialog!!, LoginPageActivity.objectLoginFeedback.data.stock_group)
        }

        return binding.root
    }
}