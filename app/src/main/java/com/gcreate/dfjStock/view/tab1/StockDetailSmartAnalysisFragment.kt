package com.gcreate.dfjStock.view.tab1

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.gcreate.dfjStock.R
import com.gcreate.dfjStock.adapter.tab1.SmartAnalysisTableViewAdapter
import com.gcreate.dfjStock.databinding.FragmentStockDetailSmartAnalysisBinding
import com.gcreate.dfjStock.model.tableView.SmartAnalysisTableViewModel
import com.gcreate.dfjStock.view.dialog.ScaledImageDialogFragment
import com.gcreate.dfjStock.webAPI.RetrofitInstance
import com.gcreate.dfjStock.webAPI.search.ApiObjectCategoriesResult
import com.gcreate.dfjStock.webAPI.tab1.ApiObjectStockAnalysisText
import com.gcreate.dfjStock.webAPI.tab1.ApiObjectStockStateImage
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class StockDetailSmartAnalysisFragment : Fragment() {
    /*    智慧分析 */
    private lateinit var binding: FragmentStockDetailSmartAnalysisBinding

    private lateinit var apiCall: Call<ApiObjectStockStateImage>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_stock_detail_smart_analysis, container, false)

        smartAnalysisGraph()
        smartAnalysisText()

        return binding.root
    }

    private fun smartAnalysisGraph() {
        apiCall = RetrofitInstance.getDfjApiInstance2().getStockStateImage(StockDetailFragment.stockID)
        apiCall.enqueue(object : Callback<ApiObjectStockStateImage> {
            override fun onResponse(call: Call<ApiObjectStockStateImage>, response: Response<ApiObjectStockStateImage>) {
                if (response.isSuccessful) {
                    val stockStateImage = response.body()
                    val url = stockStateImage!!.data.url

                    Glide.with(requireActivity()).load(url)
                        .skipMemoryCache(true)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)//不缓冲disk硬盘中
                        .into(binding.imgStockDetailSmartAnalysis)

                    binding.imgStockDetailSmartAnalysis.setOnClickListener {
                        val scaledImageDialog: DialogFragment = ScaledImageDialogFragment()
                        val bundle = Bundle()
                        bundle.putString("ImageUrl", url)
                        scaledImageDialog.arguments = bundle
                        scaledImageDialog.show(activity!!.supportFragmentManager, "simple dialog")
                    }
                }
            }

            override fun onFailure(call: Call<ApiObjectStockStateImage>, t: Throwable) {
            }
        })
    }

    private fun smartAnalysisText() {
        RetrofitInstance.getDfjApiInstance2().getSmartAnalysisText(StockDetailFragment.stockID)
            .enqueue(object : Callback<ApiObjectStockAnalysisText> {
                override fun onResponse(call: Call<ApiObjectStockAnalysisText>, response: Response<ApiObjectStockAnalysisText>) {
                    if (response.isSuccessful) {
                        val apiObjectStockAnalysisText = response.body()
                        initializeTableView(apiObjectStockAnalysisText!!)
                    }
                }

                override fun onFailure(call: Call<ApiObjectStockAnalysisText>, t: Throwable) {
                }
            })
    }

    private fun initializeTableView(response: ApiObjectStockAnalysisText) {

        // Create TableView View model class  to group view models of TableView
        val tableViewModel = SmartAnalysisTableViewModel(response)

        // Create TableView Adapter
        val tableViewAdapter = SmartAnalysisTableViewAdapter(StockDetailFragment.stockSymbol)

        binding.tableContent.setHasFixedWidth(false)
        binding.tableContent.setAdapter(tableViewAdapter)


        // Load the dummy data to the TableView
        tableViewAdapter.setAllItems(tableViewModel.columnHeaderList, tableViewModel.rowHeaderList, tableViewModel.cellList)

        binding.tableContent.setHasFixedWidth(false)

    }

    override fun onStop() {
        super.onStop()
        apiCall.cancel()
    }
}