package com.gcreate.dfjStock.view.tab1

import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.gcreate.dfjStock.Listener.IListener
import com.gcreate.dfjStock.Listener.ListenerManager
import com.gcreate.dfjStock.Listener.ViewControlListener
import com.gcreate.dfjStock.Listener.ViewControlListenerManager
import com.gcreate.dfjStock.MainActivity
import com.gcreate.dfjStock.R
import com.gcreate.dfjStock.adapter.StockListItemSwipeToDeleteCallback
import com.gcreate.dfjStock.adapter.tab1.StockFavListAdapter
import com.gcreate.dfjStock.databinding.FragmentOptionalStockBinding
import com.gcreate.dfjStock.view.LoginPageActivity
import com.gcreate.dfjStock.view.dialog.StockTipDialogFragment
import com.gcreate.dfjStock.webAPI.RetrofitInstance
import com.gcreate.dfjStock.webAPI.search.ApiObjectCategoriesResult
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class OptionalStockFragment : Fragment(), IListener, ViewControlListener {

    private lateinit var binding: FragmentOptionalStockBinding
    private lateinit var navController: NavController
    private lateinit var mAdapter: StockFavListAdapter

    private var storagedStockItemList = LoginPageActivity.objectLoginFeedback!!.data.stock_group[0].contain
    private var lastOffset = 0
    private var lastPosition = 0
    private var objectCategoriesResult: ApiObjectCategoriesResult? = null

    private var apiCall: Call<ApiObjectCategoriesResult> = RetrofitInstance.getDfjApiInstance2().getOneFavGroupStockItems("")

    companion object {
        //--------------------------------------------------------------------------------------------------
        @JvmField
        var favGroupSelectedPosition = 0

    }

    override fun onStart() {
        super.onStart()
        scrollToPosition()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_optional_stock, container, false)
        navController = NavHostFragment.findNavController(this)

        ListenerManager.getInstance().registerListener(this)
        ViewControlListenerManager.getInstance().registerListener(this)

        initView()
        getFavGroupStockList(true)


        return binding.root
    }


    private fun initView() {
        //  ToolBar
        binding.toolbarOptionalStock.apply {
            toolbar.title = ""
            // 預設起始群組一
            toolbarTitle.text = LoginPageActivity.objectLoginFeedback!!.data.stock_group[favGroupSelectedPosition].group_name

            groupTitle.setOnClickListener {
                val selectStockGroupDialog: DialogFragment = OptionalStockSelectGroupDialogFragment()
                selectStockGroupDialog.show(requireActivity().supportFragmentManager, "")
            }
        }

        setHasOptionsMenu(true)
        (requireActivity() as AppCompatActivity).setSupportActionBar(binding.toolbarOptionalStock.toolbar)


        binding.recyclerContent.apply {
            layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
            // 監聽RecyclerView滾動狀態
            addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                    super.onScrollStateChanged(recyclerView, newState)
                    if (recyclerView.layoutManager != null) {
                        positionAndOffset
                    }
                }
            })
        }

        binding.swipeRefresh.setOnRefreshListener { getFavGroupStockList(false) }

    }

    private fun getFavGroupStockList(isFirstTime: Boolean) {

        // 群組有內容
        if (storagedStockItemList.isNotEmpty()) {
            if (isFirstTime) {
                viewVisibility(View.GONE, View.VISIBLE)
                getStockItemList(favGroupSelectedPosition)
            } else {
                if (objectCategoriesResult!!.data.size != 0) {
                    mAdapter.refresh(objectCategoriesResult!!.data)
                } else {
                    viewVisibility(View.VISIBLE, View.GONE)
                }
                binding.swipeRefresh.isRefreshing = false
            }
        } else {
            viewVisibility(View.VISIBLE, View.GONE)
            binding.swipeRefresh.isRefreshing = false
        }
    }

    //------------------------------------------Get stock data  -----------------------------------------
    private fun getStockItemList(favGroupSelectedPosition: Int) {
        storagedStockItemList = LoginPageActivity.objectLoginFeedback!!.data.stock_group[favGroupSelectedPosition].contain

        val sb = StringBuilder()

        for (item in storagedStockItemList) {
            sb.append("$item,")
        }

        val stockIds = if (storagedStockItemList.isNotEmpty()) sb.deleteCharAt(sb.length - 1).toString() else ""

        if (storagedStockItemList.size != 0) {

            viewVisibility(View.GONE, View.VISIBLE)

            apiCall = RetrofitInstance.getDfjApiInstance2().getOneFavGroupStockItems(stockIds)
            apiCall.enqueue(object : Callback<ApiObjectCategoriesResult> {
                override fun onResponse(call: Call<ApiObjectCategoriesResult>, response: Response<ApiObjectCategoriesResult>) {
                    if (response.isSuccessful) {
                        objectCategoriesResult = response.body()
                        mAdapter = StockFavListAdapter(binding, objectCategoriesResult!!.data)

                        binding.recyclerContent.adapter = mAdapter

                        mAdapter.setOnItemClickListener { _, _, position ->
                            val bundle = Bundle()
                            bundle.putString("stockName", objectCategoriesResult!!.data[position].description)
                            bundle.putString("stockSymbol", objectCategoriesResult!!.data[position].symbol)
                            bundle.putInt("stockID", objectCategoriesResult!!.data[position].id)
                            navController.navigate(R.id.action_item1_to_fragment_StockDetail, bundle)
                        }
                        val itemTouchHelper = ItemTouchHelper(StockListItemSwipeToDeleteCallback(activity, mAdapter))
                        itemTouchHelper.attachToRecyclerView(binding.recyclerContent)
                    }
                }

                override fun onFailure(call: Call<ApiObjectCategoriesResult>, t: Throwable) {
                    Toast.makeText(requireActivity(), "連線失敗，請檢察網際網路", Toast.LENGTH_SHORT).show()
                }
            })
        } else {
            viewVisibility(View.VISIBLE, View.GONE)
            binding.swipeRefresh.isRefreshing = false
        }
    }

    //------------------------------------------Menu function  -----------------------------------------
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.toolbar, menu)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        val filterIcon = menu.findItem(R.id.toolbarMenuItem_filter)
        filterIcon.isVisible = false
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.toolbarMenuItem_Search) {
            if (MainActivity.objectAllStocks != null) {
                navController.navigate(R.id.action_item1_to_exchangeItemFragment)
            }

        } else if (item.itemId == R.id.toolbarMenuItem_Tip) {
            val tripDialog: DialogFragment = StockTipDialogFragment()
            tripDialog.show(requireActivity().supportFragmentManager, "simple dialog")
        }
        return super.onOptionsItemSelected(item)
    }

    //------------------------------------------收到通知後要做的事  -----------------------------------------
    override fun notifyAllActivity(favGroupSelectedPosition: Int) {
        Companion.favGroupSelectedPosition = favGroupSelectedPosition
        binding.toolbarOptionalStock.toolbarTitle.text = LoginPageActivity.objectLoginFeedback!!.data.stock_group[favGroupSelectedPosition].group_name
        getStockItemList(favGroupSelectedPosition)
    }

    override fun notifyViewUpdate(isShow: Boolean) {
        if (isShow) {
            viewVisibility(View.VISIBLE, View.GONE)
        } else {
            viewVisibility(View.GONE, View.VISIBLE)
        }
        //        updateSingleGroup();
    }


    /**
     * 紀錄RecyclerView當前位置
     */
    private val positionAndOffset: Unit
        get() {
            val layoutManager = binding.recyclerContent.layoutManager as LinearLayoutManager?
            // 獲取可視的第一個 view
            val topView = layoutManager!!.getChildAt(0)
            if (topView != null) {
                //獲取與該view的頂部的偏移量
                lastOffset = topView.top
                //得到該View的數組位置
                lastPosition = layoutManager.getPosition(topView)
            }
        }

    /**
     * 讓 RecyclerView滾動到指定位置
     */
    private fun scrollToPosition() {
        if (binding.recyclerContent.layoutManager != null && lastPosition >= 0) {
            (binding.recyclerContent.layoutManager as LinearLayoutManager).scrollToPositionWithOffset(lastPosition, lastOffset)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        ListenerManager.getInstance().unRegisterListener(this)
        ViewControlListenerManager.getInstance().unRegisterListener(this)
    }

    override fun onStop() {
        super.onStop()
        apiCall.cancel()
    }

//    private fun updateSingleGroup() {
//        val paramObject = JsonObject()
//        paramObject.addProperty("user_id", LoginPageActivity.objectLoginFeedback.data.user_id)
//        paramObject.addProperty("group_id", favGroupSelectedPosition + 1)
//        val jsonArray = JsonArray()
//        for (i in LoginPageActivity.objectLoginFeedback.data.stock_group[favGroupSelectedPosition].contain.indices) {
//            jsonArray.add(LoginPageActivity.objectLoginFeedback.data.stock_group[favGroupSelectedPosition].contain[i])
//        }
//        paramObject.add("contain", jsonArray)
//        val call = global.gcAPI.updateSingleGroup(paramObject)
//        call.enqueue(object : Callback<ApiObjectUpdateFeedback?> {
//            override fun onResponse(call: Call<ApiObjectUpdateFeedback?>, response: Response<ApiObjectUpdateFeedback?>) {
//                val objectUpdateFeedback = response.body()
//            }
//
//            override fun onFailure(call: Call<ApiObjectUpdateFeedback?>, t: Throwable) {}
//        })
//    }

    private fun viewVisibility(tvOptionalStockTxtVisibility: Int, swipeRefreshVisibility: Int) {
        binding.tvOptionalStockTxt.visibility = tvOptionalStockTxtVisibility
        binding.swipeRefresh.visibility = swipeRefreshVisibility
    }
}