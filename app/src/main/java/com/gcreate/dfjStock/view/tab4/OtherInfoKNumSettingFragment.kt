package com.gcreate.dfjStock.view.tab4

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.gcreate.dfjStock.Listener.KNumListener
import com.gcreate.dfjStock.Listener.KNumListenerManager
import com.gcreate.dfjStock.R
import com.gcreate.dfjStock.StorageDataController.KNumGroupMaintain
import com.gcreate.dfjStock.adapter.tab4.KNumSettingAdapter
import com.gcreate.dfjStock.databinding.FragmentOtherInfoKnumSettingBinding
import com.gcreate.dfjStock.model.ObjectKNumModel
import java.util.*


class OtherInfoKNumSettingFragment : Fragment(), KNumListener {
    /*  其他資訊 */
    private lateinit var binding: FragmentOtherInfoKnumSettingBinding

    private val list: MutableList<ObjectKNumModel> = ArrayList()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_other_info_knum_setting, container, false)
        KNumListenerManager.getInstance().registerListener(this)

        initView()

        return binding.root
    }

    private fun initView() {
        // ToolBar
        binding.otherInfoKnumToolbar.apply {
            toolbarArrowUp.visibility = View.GONE
            toolbarArrowDown.visibility = View.GONE
            toolbarTitle.text = "K量設定"
            toolbarTitle.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null)
            toolbar.title = ""
            toolbar.setNavigationIcon(R.drawable.arrow_icon_back)
            toolbar.setNavigationOnClickListener { requireActivity().onBackPressed() }
        }

        binding.rvContent.componentRv.apply {
            layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
            val params = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
            params.setMargins(0, 12, 0, 0)
            adapter = KNumSettingAdapter(list)
        }

        kNumData()
    }

    private fun kNumData() {
        if (list.size == 0) {
            list.addAll(KNumGroupMaintain.getKNumModelsList(requireActivity()))
        }
    }

    override fun notifyRefreshView(hasChange: Boolean) {
        if (hasChange) {
            list.clear()
            kNumData()
            binding.rvContent.componentRv.adapter!!.notifyDataSetChanged()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        KNumListenerManager.getInstance().unRegisterListener(this)
    }
}