package com.gcreate.dfjStock.view.dialog

import android.content.DialogInterface
import android.graphics.Color
import com.gcreate.dfjStock.global
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.ViewGroup
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import androidx.fragment.app.DialogFragment
import com.gcreate.dfjStock.R
import com.gcreate.dfjStock.Listener.InterruptCallControlListenerManager

class LoadingDialog : DialogFragment() {
    override fun onStart() {
        super.onStart()
        if (dialog != null) {
            dialog!!.window!!.setLayout(global.windowWidth, global.windowHeigh)
            dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.dialog_loading, container, false)

        //  Loading 過程中. User 手動按下物理返回鍵， 防呆處理。
        dialog!!.setOnKeyListener { dialog: DialogInterface, keyCode: Int, _: KeyEvent? ->
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                InterruptCallControlListenerManager.getInstance().sendBroadCast()
                dialog.cancel()
                return@setOnKeyListener true
            }
            false
        }
        return view
    }

    fun dismissDialog() {
        if (dialog != null) dialog!!.dismiss()
    }
}