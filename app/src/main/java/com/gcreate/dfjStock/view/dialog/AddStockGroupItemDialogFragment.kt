package com.gcreate.dfjStock.view.dialog

import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import com.gcreate.dfjStock.model.ObjectStockGroup
import com.gcreate.dfjStock.R
import com.gcreate.dfjStock.StorageDataController.StockGroupMaintain
import com.gcreate.dfjStock.databinding.ComponentGroupMaintainBinding
import com.gcreate.dfjStock.global
import com.gcreate.dfjStock.view.tab4.OtherInfoGroupMaintainFragment.UpdateGroupListBroadcastReceiver
import java.util.*

class AddStockGroupItemDialogFragment : DialogFragment() {
    /*  新增群組*/
    private lateinit var binding: ComponentGroupMaintainBinding

    private var intent: Intent? = null
    private val mUpdateGroupListBroadcastReceiver = UpdateGroupListBroadcastReceiver()

    override fun onStart() {
        super.onStart()
        //  設定 Dialog 視窗寬高
        if (dialog != null) {
            dialog!!.window!!.setLayout((global.windowWidth * 0.81).toInt(), (global.windowHeigh * 0.25).toInt())
            dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.component_group_maintain, container, false)
        binding.tvAlertTitle.text = "新增群組"

        binding.editGroupName.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_NEXT || actionId == EditorInfo.IME_ACTION_DONE) {
                hideSoftKeyBroad()
                true
            } else {
                false
            }
        }

        binding.btnOk.setOnClickListener {
            if (binding.editGroupName.text.toString().trim { it <= ' ' }.isEmpty()) {
                binding.editGroupName.clearFocus()
                Toast.makeText(activity, "群組名稱不能空白", Toast.LENGTH_SHORT).show()
            } else {
                StockGroupMaintain.addFavStockGroupItem(requireActivity(), ObjectStockGroup(binding.editGroupName.text.toString(), ArrayList()))
                val intentFilter = IntentFilter("updateStockGroupList")
                requireActivity().registerReceiver(mUpdateGroupListBroadcastReceiver, intentFilter)
                intent = Intent()
                intent!!.action = "updateStockGroupList"
                requireActivity().sendBroadcast(intent)
                binding.editGroupName.clearFocus()
                dialog!!.dismiss()
            }
        }
        binding.btnCancel.setOnClickListener { dialog!!.dismiss() }

        return binding.root
    }

    private fun hideSoftKeyBroad() {
        val imm = (requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
        imm.hideSoftInputFromWindow(binding.root.windowToken, 0)
        binding.editGroupName.clearFocus()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (intent != null) {
            requireActivity().unregisterReceiver(mUpdateGroupListBroadcastReceiver)
        }
    }
}