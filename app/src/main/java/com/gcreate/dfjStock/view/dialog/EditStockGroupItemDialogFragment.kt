package com.gcreate.dfjStock.view.dialog

import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import com.gcreate.dfjStock.R
import com.gcreate.dfjStock.StorageDataController.StockGroupMaintain
import com.gcreate.dfjStock.databinding.ComponentGroupMaintainBinding
import com.gcreate.dfjStock.global
import com.gcreate.dfjStock.view.tab4.OtherInfoGroupMaintainFragment

class EditStockGroupItemDialogFragment : DialogFragment() {
    /*  編輯群組*/
    private lateinit var binding: ComponentGroupMaintainBinding
    private var imm: InputMethodManager? = null

    private var mUpdateGroupListBroadcastReceiver = OtherInfoGroupMaintainFragment.UpdateGroupListBroadcastReceiver()
    private var intent: Intent? = null

    override fun onStart() {
        super.onStart()
        if (dialog != null) {
            dialog!!.window!!.setLayout((global.windowWidth * 0.81).toInt(), (global.windowHeigh * 0.25).toInt())
            dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.component_group_maintain, container, false)
        imm = requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

        initView()
        viewAction()

        return binding.root
    }

    private fun initView() {
        binding.tvAlertTitle.text = "更改名稱"
        binding.editGroupName.setText(requireArguments().getString("GroupName"))

        val intentFilter = IntentFilter("updateStockGroupList")
        requireActivity().registerReceiver(mUpdateGroupListBroadcastReceiver, intentFilter)
    }

    private fun viewAction() {
        binding.btnOk.setOnClickListener {
            if (binding.editGroupName.text.toString().trim { it <= ' ' }.isEmpty()) {
                binding.editGroupName.clearFocus()
                Toast.makeText(activity, "群組名稱不能空白", Toast.LENGTH_SHORT).show()
            } else {
                StockGroupMaintain.updateFavStockGroupList(requireArguments().getInt("GroupIndex"), binding.editGroupName.text.toString())
                intent = Intent()
                intent!!.action = "updateStockGroupList"
                requireActivity().sendBroadcast(intent)
                binding.editGroupName.clearFocus()
                dialog!!.dismiss()
            }
        }

        binding.btnCancel.setOnClickListener { dialog!!.dismiss() }

        binding.editGroupName.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_NEXT || actionId == EditorInfo.IME_ACTION_DONE) {
                hideSoftKeyBroad()
                true
            } else {
                false
            }
        }

        binding.editGroupName.onFocusChangeListener = View.OnFocusChangeListener { _: View?, hasFocus: Boolean ->
            if (hasFocus) {
                binding.editGroupName.setText("")
            } else {
                binding.editGroupName.clearFocus()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (intent != null) {
            requireActivity().unregisterReceiver(mUpdateGroupListBroadcastReceiver)
        }
    }

    private fun hideSoftKeyBroad() {
        assert(imm != null)
        imm!!.hideSoftInputFromWindow(binding.root.windowToken, 0)
        binding.editGroupName.clearFocus()
        binding.tvAlertTitle.requestFocus()
    }

}