package com.gcreate.dfjStock.view.tab3

import android.annotation.SuppressLint
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.gcreate.dfjStock.Listener.KViewOrientationListener
import com.gcreate.dfjStock.R
import com.gcreate.dfjStock.StorageDataController.KNumGroupMaintain
import com.gcreate.dfjStock.adapter.tab3.TaiwanIndexTableViewAdapter
import com.gcreate.dfjStock.chart.DataRequest
import com.gcreate.dfjStock.chart.KChartAdapter
import com.gcreate.dfjStock.databinding.FragmentTaiwanIndexBinding
import com.gcreate.dfjStock.kchartlibs.chart.KChartView
import com.gcreate.dfjStock.kchartlibs.chart.formatter.DateFormatter
import com.gcreate.dfjStock.kchartlibs.chart.formatter.TimeFormatter2
import com.gcreate.dfjStock.model.ObjectTrendLineModel
import com.gcreate.dfjStock.model.tableView.TaiwanIndexTableViewModel
import com.gcreate.dfjStock.webAPI.ApiCallback
import com.gcreate.dfjStock.webAPI.RetrofitInstance
import com.gcreate.dfjStock.webAPI.tab1.ApiObjectStockHistoricalData
import com.gcreate.dfjStock.webAPI.tab3.ApiObjectTaiwanIndexLackPoints
import com.gcreate.dfjStock.webAPI.tab3.ApiObjectTaiwanIndexText
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

@SuppressLint("SourceLockedOrientationActivity")
class TaiwanIndexFragment : Fragment(), ApiCallback, KViewOrientationListener {
    /** 台指欠點*/

    private lateinit var binding: FragmentTaiwanIndexBinding
    private var mKChartView: KChartView? = null

    private var mAdapter: KChartAdapter? = null

    //    private var call: Call<ApiObjectStockHistoricalData>? = null
//    private var apiCallFinishedCount = 0
    private val riseDataList: MutableList<ObjectTrendLineModel> = ArrayList()
    private val fallDataList: MutableList<ObjectTrendLineModel> = ArrayList()

    private lateinit var apiCall: Call<ApiObjectStockHistoricalData>

    companion object {
        //--------------------------------------------------------------------------------------------------
        @JvmField
        var objectStockHistoricalData: ApiObjectStockHistoricalData? = null
//        var objectTaiwanIndexHorizontalFallData: ApiObjectTaiwanIndexHorizontalData? = null
//        var objectTaiwanIndexHorizontalRiseData: ApiObjectTaiwanIndexHorizontalData? = null
//        var objectTaiwanIndexArticle: ApiObjectTaiwanIndexArticle? = null

        @JvmField
        var is1MK = true

        @JvmField
        var leftSideDate: String = ""

        @JvmField
        var rightSideData: String = ""
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_taiwan_index, container, false)

        initView()
        viewOnClickAction()


        return binding.root
    }


    private fun initView() {
        requireActivity().requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_SENSOR
        mKChartView = binding.kchartView.kchartView

        //  ToolBar
        binding.taiwanindexToolbar.apply {
            toolbarArrowUp.visibility = View.GONE
            toolbarArrowDown.visibility = View.GONE
            toolbarTitle.text = "台指欠點"
            toolbar.title = ""
        }
        binding.kchartView.KLineArticleTitle.text = "東方靖獨家分析"

        //  Setup Menu
        setHasOptionsMenu(true)
        (requireActivity() as AppCompatActivity?)!!.setSupportActionBar(binding.taiwanindexToolbar.toolbar)
        getStockHistoricalData("min1")
    }


    private fun viewOnClickAction() {
        binding.tv1mK.setOnClickListener {
            refreshUI(true)
            is1MK = true
            getStockHistoricalData("min1")
        }

        binding.tv1dK.setOnClickListener {
            refreshUI(false)
            is1MK = false
            getStockHistoricalData("day1")
        }


    }
    //------------------------------------------台指欠點 Load data and  InitView  ----------------------------------------
    /**
     * @param timeInterval
     *     • "1min" - 一分鐘
     *     • "1day" - 一日
     * 0 ; 台指欠點(一分K)
     * 2 : 台指欠點(日K)
     */
    private fun getStockHistoricalData(timeInterval: String) {

        var kNum = 0
        when (timeInterval) {
            "min1" -> {
                kNum = KNumGroupMaintain.getKNumModelsList(activity)[4].kNumValue
            }
            "day1" -> {
                kNum = KNumGroupMaintain.getKNumModelsList(activity)[5].kNumValue
            }
        }
        apiCall = RetrofitInstance.getDfjApiInstance2().getStockHistoricalData(4468, timeInterval, kNum)

        apiCall.enqueue(object : Callback<ApiObjectStockHistoricalData> {
            override fun onResponse(call: Call<ApiObjectStockHistoricalData>, response: Response<ApiObjectStockHistoricalData>) {
                if (response.isSuccessful) {
                    objectStockHistoricalData = response.body()
                    initKLineView()
                    initData()
                    mKChartView!!.notifyChanged()
                    getTaiwanIndexArticleText()

                } else {
                    try {
                        Toast.makeText(context, "無歷史資料", Toast.LENGTH_LONG).show()
                    } catch (e: Exception) {
                        Toast.makeText(context, e.message, Toast.LENGTH_LONG).show()
                    }
                }
            }

            override fun onFailure(call: Call<ApiObjectStockHistoricalData>, t: Throwable) {
            }
        })
    }

    /**API 23 取得台指欠點 Data  */
    private fun getTaiwanIndexLackPointsData(timeInterval: String, startKIndex: String, endKIndex: String) {
        RetrofitInstance.getDfjApiInstance2().getTaiwanIndexLackPointsData(timeInterval, startKIndex, endKIndex)
            .enqueue(object : Callback<ApiObjectTaiwanIndexLackPoints> {
                override fun onResponse(call: Call<ApiObjectTaiwanIndexLackPoints>, response: Response<ApiObjectTaiwanIndexLackPoints>) {
                    if (response.isSuccessful) {
                        val apiObjectTaiwanIndexLackPoints = response.body()
                        if (apiObjectTaiwanIndexLackPoints!!.status == "success" && apiObjectTaiwanIndexLackPoints.data.isNotEmpty()) {
                            splitUpDownTrendData(apiObjectTaiwanIndexLackPoints)
                        }
                    }
                }

                override fun onFailure(call: Call<ApiObjectTaiwanIndexLackPoints>, t: Throwable) {
                }
            })
    }

    private fun splitUpDownTrendData(apiObjectLackPoint: ApiObjectTaiwanIndexLackPoints) = runBlocking {
        launch {
            for (i in apiObjectLackPoint.data.indices) {
                when (apiObjectLackPoint.data[i].name) {
                    "downtrend-lack-point-1" -> {
                        // 畫綠色水平線
                        val downTrendLackPointList = apiObjectLackPoint.data[i].items
                        for (j in downTrendLackPointList.indices) {
                            fallDataList.add(ObjectTrendLineModel(downTrendLackPointList[j].index,
                                downTrendLackPointList[j].price.toFloat()))
                        }
                    }
                    "uptrend-lack-point-1" -> {
                        // 畫紅色水平線
                        val upTrendLackPointList = apiObjectLackPoint.data[i].items
                        for (j in upTrendLackPointList.indices) {
                            riseDataList.add(ObjectTrendLineModel(upTrendLackPointList[j].index,
                                upTrendLackPointList[j].price.toFloat()))
                        }
                    }
                }
            }
            loadComplete()
        }
    }

    //=======================================================================================================================
    private fun getTaiwanIndexArticleText() {
        RetrofitInstance.getDfjApiInstance2().taiwanIndexText.enqueue(object : Callback<ApiObjectTaiwanIndexText> {
            override fun onResponse(call: Call<ApiObjectTaiwanIndexText>, response: Response<ApiObjectTaiwanIndexText>) {

                if (response.isSuccessful) {
                    val apiObjectTaiwanIndexText = response.body()
                    if (apiObjectTaiwanIndexText!!.status == "success") {
                        initializeTableView(apiObjectTaiwanIndexText)
                    }
                }
            }

            override fun onFailure(call: Call<ApiObjectTaiwanIndexText>, t: Throwable) {
            }
        })

    }

    private fun initializeTableView(response: ApiObjectTaiwanIndexText) {

        // Create TableView View model class  to group view models of TableView
        val tableViewModel = TaiwanIndexTableViewModel(response)

        // Create TableView Adapter
        val tableViewAdapter = TaiwanIndexTableViewAdapter()

        binding.kchartView.tableContent.setHasFixedWidth(false)
        binding.kchartView.tableContent.setAdapter(tableViewAdapter)


        // Load the dummy data to the TableView
        tableViewAdapter.setAllItems(tableViewModel.columnHeaderList, tableViewModel.rowHeaderList, tableViewModel.cellList)

        binding.kchartView.tableContent.setHasFixedWidth(false)

    }

    private fun initKLineView() {
        mAdapter = KChartAdapter()
        mKChartView!!.adapter = mAdapter
        if (is1MK) {
            mKChartView!!.dateTimeFormatter = TimeFormatter2()
        } else {
            mKChartView!!.dateTimeFormatter = DateFormatter()
        }
        mKChartView!!.setGridRows(4) //  設定背景畫面切幾行
        mKChartView!!.setGridColumns(4) //  設定背景畫面切幾列
        mKChartView!!.setIsNeedHorizontalLine(true, riseDataList, fallDataList)
        mKChartView!!.setIsSmartGraph(false)
        mKChartView!!.setIsKChart(false, null, null)


        // 監聽十字線定位
        //mKChartView!!.setOnSelectedChangedListener { view, point, index -> val data = point as KLineEntity }
    }

    private fun initData() {
        mKChartView!!.showLoading() // 加載資料動畫 : 轉圈圈
        Thread {
            val data = DataRequest().getALL(requireActivity().applicationContext, objectStockHistoricalData)
            requireActivity().runOnUiThread {
                mAdapter!!.addFooterData(data)
                mKChartView!!.startAnimation() //  資料加載完 Kline圖產生時間
                mKChartView!!.refreshEnd()
            }
        }.start()
    }

    override fun loadComplete() {
        mKChartView!!.notifyChanged()
    }

    //------------------------------------------Menu function  -----------------------------------------
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.stockgroupmenu, menu)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        val modifyKNumIcon = menu.findItem(R.id.toolbarMenuItem_modifyKNum)
        val addStockIcon = menu.findItem(R.id.toolbarMenuItem_Search)
        modifyKNumIcon.isVisible = false
        addStockIcon.isVisible = false
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        //-----------------------    點擊畫水平線  ----------------------------
        if (item.itemId == R.id.toolbarMenuItem_DFJTrendLine) {
            riseDataList.clear()
            fallDataList.clear()
            if (is1MK) {
                getTaiwanIndexLackPointsData("min1", leftSideDate, rightSideData)
            } else {
                getTaiwanIndexLackPointsData("day1", leftSideDate, rightSideData)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun refreshUI(is1mK: Boolean) {
        if (is1mK) {
            //--------   1分K線圖   --------
            binding.tv1mK.setTextColor(ContextCompat.getColor(requireActivity(), R.color.RedDC0000))
            binding.tv1dK.setTextColor(ContextCompat.getColor(requireActivity(), R.color.WhiteFFFFFF))
            riseDataList.clear()
            fallDataList.clear()
            mKChartView!!.notifyChanged()
        } else {
            //--------   歷史推播   --------
            binding.tv1mK.setTextColor(ContextCompat.getColor(requireActivity(), R.color.WhiteFFFFFF))
            binding.tv1dK.setTextColor(ContextCompat.getColor(requireActivity(), R.color.RedDC0000))
            riseDataList.clear()
            fallDataList.clear()
            mKChartView!!.notifyChanged()
        }
    }

    //------------------------------------------Others  function  -----------------------------------------

    override fun onStop() {
        super.onStop()
        apiCall.cancel()
        requireActivity().requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
    }

    override fun notifyChangeParam(orientation: String) {
        when (orientation) {
            "PORTRAIT" -> {
                mKChartView!!.setPointWidth(10f)
                mKChartView!!.setCandleLineWidth(2f)
                mKChartView!!.setCandleWidth(8f)
                mKChartView!!.setKChildViewPillarWidth(4)
            }
            "LANDSCAPE" -> {
                mKChartView!!.setPointWidth(4f)
                mKChartView!!.setCandleLineWidth(1f)
                mKChartView!!.setCandleWidth(3f)
                mKChartView!!.setKChildViewPillarWidth(1)
            }
        }
        mKChartView!!.notifyChanged()
    }

}