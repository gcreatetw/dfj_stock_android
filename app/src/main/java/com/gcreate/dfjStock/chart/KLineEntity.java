package com.gcreate.dfjStock.chart;

import com.gcreate.dfjStock.kchartlibs.chart.entity.IKLine;

import java.util.ArrayList;
import java.util.List;


/**
 * K线实体
 * Created by tifezh on 2016/5/16.
 */

public class KLineEntity implements IKLine {


    public String getDatetime() {
        return Date;
    }

    @Override
    public float getOpenPrice() {
        return Open;
    }

    @Override
    public float getHighPrice() {
        return High;
    }

    @Override
    public float getLowPrice() {
        return Low;
    }

    @Override
    public float getClosePrice() {
        return Close;
    }

    @Override
    public float getMA5Price() {
        return MA5Price;
    }

    @Override
    public float getMA10Price() {
        return MA10Price;
    }

    @Override
    public float getMA20Price() {
        return MA20Price;
    }

    @Override
    public float getVolume() {
        return Volume;
    }

    @Override
    public float getMA5Volume() {
        return MA5Volume;
    }

    @Override
    public float getMA10Volume() {
        return MA10Volume;
    }


    public String Date;
    public float Open;
    public float High;
    public float Low;
    public float Close;
    public float Volume;

    public float MA5Price;

    public float MA10Price;

    public float MA20Price;


    public float MA5Volume;

    public float MA10Volume;





    //  DFJ 上升均線
    public List<Float> DfjRiseAverageDataList = new ArrayList<>();

    @Override
    public List<Float> getDfjRiseAverageDataList() {
        return DfjRiseAverageDataList;
    }

    //  DFJ 下降均線
    public List<Float> DfjFallverageDataList = new ArrayList<>();

    @Override
    public List<Float> getDfjFallAverageDataList() {
        return DfjFallverageDataList;
    }


    public KLineEntity(double closePrice, String date, double highPrice, double lowPrice, double openPrice, double volume) {

        this.Close = (float) closePrice;
        this.Date = date;
        this.High = (float) highPrice;
        this.Low = (float) lowPrice;
        this.Open = (float) openPrice;
        this.Volume = (float) volume;

    }

}
