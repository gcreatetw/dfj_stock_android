package com.gcreate.dfjStock.chart;


import java.util.List;

/**
 * 数据辅助类 计算
 * Created by tifezh on 2016/11/26.
 */

public class DataHelper {

    /**
     * 计算MA
     *
     * @param datas
     */
    static void calculate(List<KLineEntity> datas) {
        calculateMA(datas);
        calculateVolumeMA(datas);
    }

    /**
     * 计算ma
     *
     * @param datas
     */
    static void calculateMA(List<KLineEntity> datas) {
        float ma5 = 0;
        float ma10 = 0;
        float ma20 = 0;

        for (int i = 0; i < datas.size(); i++) {
            KLineEntity point = datas.get(i);
            final float closePrice = point.getClosePrice();

            ma5 += closePrice;
            ma10 += closePrice;
            ma20 += closePrice;
            if (i >= 5) {
                ma5 -= datas.get(i - 5).getClosePrice();
                point.MA5Price = ma5 / 5f;
            } else {
                point.MA5Price = ma5 / (i + 1f);
            }
            if (i >= 10) {
                ma10 -= datas.get(i - 10).getClosePrice();
                point.MA10Price = ma10 / 10f;
            } else {
                point.MA10Price = ma10 / (i + 1f);
            }
            if (i >= 20) {
                ma20 -= datas.get(i - 20).getClosePrice();
                point.MA20Price = ma20 / 20f;
            } else {
                point.MA20Price = ma20 / (i + 1f);
            }
        }
    }

    private static void calculateVolumeMA(List<KLineEntity> entries) {
        float volumeMa5 = 0;
        float volumeMa10 = 0;

        for (int i = 0; i < entries.size(); i++) {
            KLineEntity entry = entries.get(i);

            volumeMa5 += entry.getVolume();
            volumeMa10 += entry.getVolume();

            if (i >= 5) {

                volumeMa5 -= entries.get(i - 5).getVolume();
                entry.MA5Volume = (volumeMa5 / 5f);
            } else {

                entry.MA5Volume = (volumeMa5 / (i + 1f));
            }

            if (i >= 10) {
                volumeMa10 -= entries.get(i - 10).getVolume();
                entry.MA10Volume = (volumeMa10 / 5f);
            } else {
                entry.MA10Volume = (volumeMa10 / (i + 1f));
            }
        }
    }

}
