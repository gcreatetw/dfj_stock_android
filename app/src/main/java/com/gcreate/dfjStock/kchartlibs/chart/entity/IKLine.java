package com.gcreate.dfjStock.kchartlibs.chart.entity;

/**
 * k线实体接口
 * Created by tifezh on 2016/6/9.
 */

public interface IKLine extends ICandle,IVolume {
}
