package com.gcreate.dfjStock.kchartlibs.chart.draw;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.gcreate.dfjStock.R;
import com.gcreate.dfjStock.kchartlibs.chart.BaseKChartView;
import com.gcreate.dfjStock.kchartlibs.chart.base.IChartDraw;
import com.gcreate.dfjStock.kchartlibs.chart.base.IValueFormatter;
import com.gcreate.dfjStock.kchartlibs.chart.entity.IVolume;
import com.gcreate.dfjStock.kchartlibs.chart.formatter.BigValueFormatter;


/**
 * Created by hjm on 2017/11/14 17:49.
 */

public class VolumeDraw implements IChartDraw<IVolume> {
    /* 成交量柱狀圖*/
    private Paint mRedPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private Paint mGreenPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private Paint ma5Paint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private Paint ma10Paint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private int pillarWidth = 0;
    private final Context context;

    public VolumeDraw(BaseKChartView view, int KChildViewPillarWidth) {
        context = view.getContext();
        mRedPaint.setColor(ContextCompat.getColor(context, R.color.chart_red));
        mGreenPaint.setColor(ContextCompat.getColor(context, R.color.chart_green));
        pillarWidth = KChildViewPillarWidth;
    }

    @Override
    public void drawTranslated(
            @Nullable IVolume lastPoint, @NonNull IVolume curPoint, float lastX, float curX,
            @NonNull Canvas canvas, @NonNull BaseKChartView view, int position, boolean isNeedDrawHorizontalLine) {

        //  畫柱狀圖
        if (isNeedDrawHorizontalLine) {
            drawTaiwanHistogram(canvas, curPoint, lastPoint, curX, view, position);
        } else {
            drawHistogram(canvas, curPoint, lastPoint, curX, view, position);
        }


        /*隱藏 MA5 / MA10 /MA20
        // 折線圖
        // MA5
        view.drawChildLine(canvas, ma5Paint, lastX, lastPoint.getMA5Volume(), curX, curPoint.getMA5Volume());
        //MA10
        view.drawChildLine(canvas, ma10Paint, lastX, lastPoint.getMA10Volume(), curX, curPoint.getMA10Volume());
*/
    }

    private void drawHistogram(Canvas canvas, IVolume curPoint, IVolume lastPoint, float curX, BaseKChartView view, int position) {

        float r = pillarWidth / 2;
        float top = view.getChildY(curPoint.getVolume());
        int bottom = view.getChildRect().bottom;


            if (curPoint.getClosePrice() >= curPoint.getOpenPrice()) {//涨
                canvas.drawRect(curX - r, top, curX + r, bottom, mRedPaint);
            } else {
                canvas.drawRect(curX - r, top, curX + r, bottom, mGreenPaint);
            }


    }

    /** 智慧圖形要限定範圍   */
    private void drawTaiwanHistogram(Canvas canvas, IVolume curPoint, IVolume lastPoint, float curX, BaseKChartView view, int position) {

        float r = pillarWidth / 2;
        float top = view.getChildY(curPoint.getVolume());
        int bottom = view.getChildRect().bottom;

        if (view.getMainY(curPoint.getClosePrice()) < BaseKChartView.mainDrawMATextBottomHeight) {
            if (curPoint.getClosePrice() >= curPoint.getOpenPrice()) {//涨
                canvas.drawRect(curX - r, top, curX + r, bottom, mRedPaint);
            } else {
                canvas.drawRect(curX - r, top, curX + r, bottom, mGreenPaint);
            }

        }
    }

    @Override
    public void drawText(
            @NonNull Canvas canvas, @NonNull BaseKChartView view, int position, float x, float y) {
        IVolume point = (IVolume) view.getItem(position);
        String text = "VOL:" + getValueFormatter().format(point.getVolume()) + " ";
        String textMA5 = "MA5:" + getValueFormatter().format(point.getMA5Volume()) + " ";


        canvas.drawText(text, 0, y, view.getTextPaint());
                /*隱藏 MA5 / MA10 /MA20
        x += view.getTextPaint().measureText(text);

        if (ma5Paint.getColor() != context.getColor(android.R.color.transparent)) {
            canvas.drawText(textMA5, x, y, ma5Paint);
            x += ma5Paint.measureText(textMA5);
        }

        text = "MA10:" + getValueFormatter().format(point.getMA10Volume()) + " ";
        canvas.drawText(text, x, y, ma10Paint);

        */
    }

    @Override
    public float getMaxValue(IVolume point) {
        return Math.max(point.getVolume(), Math.max(point.getMA5Volume(), point.getMA10Volume()));
    }

    @Override
    public float getMinValue(IVolume point) {
        return Math.min(point.getVolume(), Math.min(point.getMA5Volume(), point.getMA10Volume()));
    }

    @Override
    public void drawLevel1TrendLine(Canvas canvas, Paint paint, float startX, double startValue, float stopX, double stopValue, @NonNull BaseKChartView view) {

    }

    @Override
    public IValueFormatter getValueFormatter() {
        return new BigValueFormatter();
    }

    /**
     * 设置 MA5 线的颜色
     *
     * @param color
     */
    public void setMa5Color(int color) {
        this.ma5Paint.setColor(color);
    }

    /**
     * 设置 MA10 线的颜色
     *
     * @param color
     */
    public void setMa10Color(int color) {
        this.ma10Paint.setColor(color);
    }

    public void setLineWidth(float width) {
        this.ma5Paint.setStrokeWidth(width);
        this.ma10Paint.setStrokeWidth(width);
    }

    /**
     * 设置文字大小
     *
     * @param textSize
     */
    public void setTextSize(float textSize) {
        this.ma5Paint.setTextSize(textSize);
        this.ma10Paint.setTextSize(textSize);
    }

}
