package com.gcreate.dfjStock.kchartlibs.chart;

import android.animation.ValueAnimator;
import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.core.view.GestureDetectorCompat;

import com.gcreate.dfjStock.R;
import com.gcreate.dfjStock.kchartlibs.chart.base.IAdapter;
import com.gcreate.dfjStock.kchartlibs.chart.base.IChartDraw;
import com.gcreate.dfjStock.kchartlibs.chart.base.IDateTimeFormatter;
import com.gcreate.dfjStock.kchartlibs.chart.base.IValueFormatter;
import com.gcreate.dfjStock.kchartlibs.chart.draw.MainDraw;
import com.gcreate.dfjStock.kchartlibs.chart.entity.ICandle;
import com.gcreate.dfjStock.kchartlibs.chart.entity.IKLine;
import com.gcreate.dfjStock.kchartlibs.chart.formatter.TimeFormatter;
import com.gcreate.dfjStock.kchartlibs.chart.formatter.ValueFormatter;
import com.gcreate.dfjStock.kchartlibs.utils.DateUtil;
import com.gcreate.dfjStock.model.ObjectTrendLineModel;
import com.gcreate.dfjStock.view.tab1.StockDetailKChartFragment;
import com.gcreate.dfjStock.view.tab1.StockDetailSmartGraphFragment;
import com.gcreate.dfjStock.view.tab3.TaiwanIndexFragment;
import com.gcreate.dfjStock.webAPI.tab1.ApiObjectSmartAnalysisTrendLine;
import com.gcreate.dfjStock.webAPI.tab1.ApiObjectSmartAnalysisTrendLine0;
import com.gcreate.dfjStock.webAPI.tab1.ApiObjectStockHistoricalData;
import com.gcreate.dfjStock.webAPI.tab1.TrendlineData;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * k线图
 * Created by tian on 2016/5/3.
 */
public abstract class BaseKChartView extends ScrollAndScaleView {
    private int mChildDrawPosition = 0;

    private static float mTranslateX = Float.MIN_VALUE;

    private static int mWidth = 0;

    private static int mTopPadding;

    private static int mBottomPadding;

    private static float mMainScaleY = 1;

    private static float mChildScaleY = 1;

    private static float mDataLen = 0;

    private float mMainMaxValue = Float.MAX_VALUE;

    private float mMainMinValue = Float.MIN_VALUE;

    private float mChildMaxValue = Float.MAX_VALUE;

    private float mChildMinValue = Float.MIN_VALUE;

    public static int mStartIndex = 0;

    public static int mStopIndex = 0;

    public float mPointWidth = 6;

    private static int mGridRows = 4;

    private static int mGridColumns = 4;

    private Paint mGridPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    private Paint mTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    private Paint mBackgroundPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    private Paint mSelectedLinePaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    private int mSelectedIndex;

    private IChartDraw mMainDraw;

    private IAdapter mAdapter;

    private DataSetObserver mDataSetObserver = new DataSetObserver() {
        @Override
        public void onChanged() {
            mItemCount = getAdapter().getCount();
            notifyChanged();
        }

        @Override
        public void onInvalidated() {
            mItemCount = getAdapter().getCount();
            notifyChanged();
        }
    };
    //当前点的个数
    private int mItemCount;
    private IChartDraw mChildDraw;
    private static List<IChartDraw> mChildDraws = new ArrayList<>();

    private IValueFormatter mValueFormatter;
    private IDateTimeFormatter mDateTimeFormatter;

    protected static KChartTabView mKChartTabView;

    private ValueAnimator mAnimator;

    private long mAnimationDuration = 500; // KLine 畫圖產生時間

    private float mOverScrollRange = 0;

    private OnSelectedChangedListener mOnSelectedChangedListener = null;

    private Rect mMainRect;

    private Rect mTabRect;

    private Rect mChildRect;

    private float mLineWidth;


    private boolean isNeedDrawHorizontalLine;
    public boolean isSmartGraph;
    public boolean isKChart;
    private List<ObjectTrendLineModel> riseDataList;
    private List<ObjectTrendLineModel> fallDataList;

    private ApiObjectSmartAnalysisTrendLine0 objectSmartAnalysisDownTrendLine;
    private ApiObjectSmartAnalysisTrendLine0 objectSmartAnalysisUpTrendLine;
    private ApiObjectSmartAnalysisTrendLine objectAllTrendLine;
    private List<TrendlineData> UpTrendLineDataList;
    private List<TrendlineData> DownTrendLineDataList;
    private ApiObjectStockHistoricalData klineData;
    public static float mainDrawMATextTopHeight;
    public static float mainDrawMATextBottomHeight;
    public static float mainDrawChildViewTopHeight;
    public static float mainDrawChildViewBottomHeight;


    public BaseKChartView(Context context) {
        super(context);
        init();
    }

    public BaseKChartView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public BaseKChartView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        setWillNotDraw(false);
        mDetector = new GestureDetectorCompat(getContext(), this);
        mScaleDetector = new ScaleGestureDetector(getContext(), this);
        if (StockDetailSmartGraphFragment.isSmartGraph) {
            mTopPadding = 0;
        } else {
            mTopPadding = (int) getResources().getDimension(R.dimen.chart_top_padding);
        }

        mBottomPadding = (int) getResources().getDimension(R.dimen.chart_bottom_padding);


        mKChartTabView = new KChartTabView(getContext());
        addView(mKChartTabView, new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        mKChartTabView.setOnTabSelectListener(new KChartTabView.TabSelectListener() {
            @Override
            public void onTabSelected(int type) {
                setChildDraw(type);
            }
        });

        mAnimator = ValueAnimator.ofFloat(0f, 1f);
        mAnimator.setDuration(mAnimationDuration);
        mAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                invalidate();
            }
        });
    }


    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        // K 圖的 canvas 大小
        //          智慧圖形
        if (isSmartGraph) {
            this.mWidth = w / 2;
        } else {
            this.mWidth = w;
        }
        initRect(w, h);
        mKChartTabView.setTranslationY(mMainRect.bottom);
        setTranslateXFromScrollX(mScrollX);
    }

    private void initRect(int w, int h) {
        int mMainChildSpace = mKChartTabView.getMeasuredHeight();
        int displayHeight = h - mTopPadding - mBottomPadding - mMainChildSpace;
        int mMainHeight = (int) (displayHeight * 0.75f);
        int mChildHeight = (int) (displayHeight * 0.25f);
        mMainRect = new Rect(0, mTopPadding, mWidth, mTopPadding + mMainHeight);
        mTabRect = new Rect(0, mMainRect.bottom, mWidth, mMainRect.bottom + mMainChildSpace);
        mChildRect = new Rect(0, mTabRect.bottom, mWidth, mTabRect.bottom + mChildHeight);

        mainDrawChildViewTopHeight = mChildRect.top;
        mainDrawChildViewBottomHeight = mChildRect.bottom;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawColor(mBackgroundPaint.getColor());
        if (mWidth == 0 || mMainRect.height() == 0 || mItemCount == 0) {
            return;
        }
        calculateValue();
        canvas.save();
        canvas.scale(1, 1);
        drawGird(canvas);
        drawK(canvas, isNeedDrawHorizontalLine);
        drawText(canvas);
        drawValue(canvas, isLongPress ? mSelectedIndex : mStopIndex);


        if (isSmartGraph) {
            if (klineData != null) {
                drawSmartGraphSlashLine(canvas, objectAllTrendLine, klineData);
            }
        } else if (isKChart) {
            // 畫DFJ LEVEL 趨勢線
            drawTrendLine(canvas);
        } else if (isNeedDrawHorizontalLine) {
            drawHorizontalLine(canvas);

        }

        canvas.restore();
    }

    public float getMainY(float value) {
        return (mMainMaxValue - value) * mMainScaleY + mMainRect.top;
    }

    public float getChildY(float value) {
        return (mChildMaxValue - value) * mChildScaleY + mChildRect.top;
    }

    /**
     * 解决text居中的问题
     */
    public float fixTextY(float y) {
        Paint.FontMetrics fontMetrics = mTextPaint.getFontMetrics();
        return (y + (fontMetrics.descent - fontMetrics.ascent) / 2 - fontMetrics.descent);
    }

    /**
     * 画表格
     *
     * @param canvas
     */
    private void drawGird(Canvas canvas) {
        //-----------------------上方k线图------------------------
        //横向的grid
        float rowSpace = mMainRect.height() / mGridRows;
        for (int i = 0; i <= mGridRows; i++) {
            //          智慧圖形
            if (isSmartGraph) {
                canvas.drawLine(0, rowSpace * i + mMainRect.top, mWidth * 2, rowSpace * i + mMainRect.top, mGridPaint);
            } else {
                canvas.drawLine(0, rowSpace * i + mMainRect.top, mWidth, rowSpace * i + mMainRect.top, mGridPaint);
            }
        }
        //-----------------------下方子图------------------------
        if (isSmartGraph) {
            canvas.drawLine(0, mChildRect.top, mWidth * 2, mChildRect.top, mGridPaint);
            canvas.drawLine(0, mChildRect.bottom, mWidth * 2, mChildRect.bottom, mGridPaint);
        } else {
            canvas.drawLine(0, mChildRect.top, mWidth, mChildRect.top, mGridPaint);
            canvas.drawLine(0, mChildRect.bottom, mWidth, mChildRect.bottom, mGridPaint);
        }

        //纵向的grid
        //          智慧圖形
        float columnSpace;
        if (isSmartGraph) {
            columnSpace = mWidth * 2 / mGridColumns;
        } else {
            columnSpace = mWidth / mGridColumns;
        }


        for (int i = 0; i <= mGridColumns; i++) {
            canvas.drawLine(columnSpace * i, mMainRect.top, columnSpace * i, mMainRect.bottom, mGridPaint);
            canvas.drawLine(columnSpace * i, mChildRect.top, columnSpace * i, mChildRect.bottom, mGridPaint);
        }
    }

    /**
     * 画 K線圖
     *
     * @param canvas
     */
    private void drawK(Canvas canvas, Boolean isNeedDrawHorizonLine) {
        //保存之前的平移，缩放
        canvas.save();
        canvas.translate(mTranslateX * mScaleX, 0);
        canvas.scale(mScaleX, 1);
        for (int i = mStartIndex; i <= mStopIndex; i++) {
            Object currentPoint = getItem(i);
            float currentPointX = getX(i);
            Object lastPoint = i == 0 ? currentPoint : getItem(i - 1);
            float lastX = i == 0 ? currentPointX : getX(i - 1);

            if (mMainDraw != null) {
                mMainDraw.drawTranslated(lastPoint, currentPoint, lastX, currentPointX, canvas, this, i, isSmartGraph);
            }
            if (mChildDraw != null) {
                mChildDraw.drawTranslated(lastPoint, currentPoint, lastX, currentPointX, canvas, this, i, isSmartGraph);
            }

        }
        // 畫十字線: 只有線
        if (isLongPress) {
            IKLine point = (IKLine) getItem(mSelectedIndex);
            float x = getX(mSelectedIndex);
            float y = getMainY(point.getClosePrice());
            /** Set color
             mSelectedLinePaint.setColor(getResources().getColor(R.color.chart_ma20));
             */
            /** canvas.drawLine("startX","startY","StopX","StopY","paint");  */
            // 重直線
            canvas.drawLine(x, mMainRect.top, x, mMainRect.bottom, mSelectedLinePaint);
            // 橫線
            canvas.drawLine(-mTranslateX, y, -mTranslateX + mWidth / mScaleX, y, mSelectedLinePaint);
            // 下方成交柱狀圖垂直線
            canvas.drawLine(x, mChildRect.top, x, mChildRect.bottom, mSelectedLinePaint);
        }
        //还原 平移缩放
        canvas.restore();
    }

    /**
     * 画文字
     *
     * @param canvas
     */
    private void drawText(Canvas canvas) {
        Paint.FontMetrics fm = mTextPaint.getFontMetrics();
        float textHeight = fm.descent - fm.ascent;
        float baseLine = (textHeight - fm.bottom - fm.top) / 2;
        //--------------画上方K線圖 Y軸的值-------------
        if (mMainDraw != null) {

            // Y軸最上面的數值
            canvas.drawText(formatValue(mMainMaxValue), 0, baseLine + mMainRect.top, mTextPaint);
            // Y軸最下面的數值
            canvas.drawText(formatValue(mMainMinValue), 0, mMainRect.bottom - textHeight + baseLine, mTextPaint);
            mainDrawMATextBottomHeight = mMainRect.bottom - textHeight + baseLine;
            float rowValue = (mMainMaxValue - mMainMinValue) / mGridRows;
            float rowSpace = mMainRect.height() / mGridRows;
            for (int i = 1; i < mGridRows; i++) {
                String text = formatValue(rowValue * (mGridRows - i) + mMainMinValue);
                // Y軸中間的數值
                canvas.drawText(text, 0, fixTextY(rowSpace * i + mMainRect.top), mTextPaint);
            }


        }
        //--------------画下方子图 左上/左下的值-------------
        /*
                    if (mChildDraw != null) {
                        canvas.drawText(mChildDraw.getValueFormatter().format(mChildMaxValue), 0, mChildRect.top+ baseLine, mTextPaint);
                        canvas.drawText(mChildDraw.getValueFormatter().format(mChildMinValue), 0, mChildRect.bottom, mTextPaint);
                    }
                */
        //--------------画时间---------------------
        float columnSpace = mWidth / mGridColumns;
        float y = mChildRect.bottom + baseLine;

        float startX = getX(mStartIndex) - mPointWidth / 2;
        float stopX = getX(mStopIndex) + mPointWidth / 2;

        // X 軸 中間時間
        if (!isSmartGraph) {
            for (int i = 1; i < mGridColumns; i++) {
                float translateX = xToTranslateX(columnSpace * i);
                if (translateX >= startX && translateX <= stopX) {
                    int index = indexOfTranslateX(translateX);
                    String text = formatDateTime(mAdapter.getDate(index));
                    canvas.drawText(text, columnSpace * i - mTextPaint.measureText(text) / 2, y, mTextPaint);
                }
            }
        }

        // X 軸 最左邊時間
        float translateX = xToTranslateX(0);
        if (translateX >= startX && translateX <= stopX) {
            TaiwanIndexFragment.leftSideDate = DateUtil.FullFormat.format(getAdapter().getDate(mStartIndex));
            canvas.drawText(formatDateTime(getAdapter().getDate(mStartIndex)), 0, y, mTextPaint);
        }
        // X 軸 日期的最右邊時間
        translateX = xToTranslateX(mWidth);
        if (translateX >= startX && translateX <= stopX) {
            String text = formatDateTime(getAdapter().getDate(mStopIndex));
            TaiwanIndexFragment.rightSideData = DateUtil.FullFormat.format(getAdapter().getDate(mStopIndex));
            canvas.drawText(text, mWidth - mTextPaint.measureText(text), y, mTextPaint);
        }
        if (isLongPress) {
            IKLine point = (IKLine) getItem(mSelectedIndex);
            String text = formatValue(point.getClosePrice());
            float r = textHeight / 2;
            y = getMainY(point.getClosePrice());
            float x;
            if (translateXtoX(getX(mSelectedIndex)) < getChartWidth() / 2) {
                x = 0;
                canvas.drawRect(x, y - r, mTextPaint.measureText(text), y + r, mBackgroundPaint);
            } else {
                x = mWidth - mTextPaint.measureText(text);
                canvas.drawRect(x, y - r, mWidth, y + r, mBackgroundPaint);
            }
            canvas.drawText(text, x, fixTextY(y), mTextPaint);
        }
    }

    /**
     * 長壓顯示十字線後，右上 or 左上顯示該點的值
     *
     * @param canvas
     * @param position
     */
    private void drawValue(Canvas canvas, int position) {
        MainDraw.isSmartGraph = isSmartGraph;

        Paint.FontMetrics fm = mTextPaint.getFontMetrics();
        float textHeight = fm.descent - fm.ascent;
        float baseLine = (textHeight - fm.bottom - fm.top) / 2;
        if (position >= 0 && position < mItemCount) {
            if (mMainDraw != null) {
                float y = mMainRect.top + baseLine - textHeight;
                mainDrawMATextTopHeight = y;
                float x = 0;

                mMainDraw.drawText(canvas, this, position, x, y);
            }
            if (mChildDraw != null) {
                float y = mChildRect.top + baseLine;
                float x = mTextPaint.measureText(mChildDraw.getValueFormatter().format(mChildMaxValue) + " ");
                mChildDraw.drawText(canvas, this, position, 0, y);
            }
        }
    }

    public int dp2px(float dp) {
        final float scale = getContext().getResources().getDisplayMetrics().density;
        return (int) (dp * scale + 0.5f);
    }

    public int sp2px(float spValue) {
        final float fontScale = getContext().getResources().getDisplayMetrics().scaledDensity;
        return (int) (spValue * fontScale + 0.5f);
    }

    /**
     * 格式化值
     */
    public String formatValue(float value) {
        if (getValueFormatter() == null) {
            setValueFormatter(new ValueFormatter());
        }
        return getValueFormatter().format(value);
    }

    /**
     * 重新计算并刷新线条
     */
    public void notifyChanged() {
        if (mItemCount != 0) {
            mDataLen = (mItemCount - 1) * mPointWidth;
            checkAndFixScrollX();
            setTranslateXFromScrollX(mScrollX);
        } else {
            setScrollX(0);
        }
        invalidate();
    }

    private void calculateSelectedX(float x) {
        mSelectedIndex = indexOfTranslateX(xToTranslateX(x));
        if (mSelectedIndex < mStartIndex) {
            mSelectedIndex = mStartIndex;
        }
        if (mSelectedIndex > mStopIndex) {
            mSelectedIndex = mStopIndex;
        }
    }

    @Override
    public void onLongPress(MotionEvent e) {
        super.onLongPress(e);
        int lastIndex = mSelectedIndex;
        calculateSelectedX(e.getX());
        if (lastIndex != mSelectedIndex) {
            onSelectedChanged(this, getItem(mSelectedIndex), mSelectedIndex);
        }
        invalidate();
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);
        setTranslateXFromScrollX(mScrollX);
    }

    @Override
    protected void onScaleChanged(float scale, float oldScale) {
        checkAndFixScrollX();
        setTranslateXFromScrollX(mScrollX);
        super.onScaleChanged(scale, oldScale);
    }

    /**
     * 计算当前的显示区域
     */
    private void calculateValue() {
        if (!isLongPress()) {
            mSelectedIndex = -1;
        }
        mMainMaxValue = Float.MIN_VALUE;
        mMainMinValue = Float.MAX_VALUE;
        mChildMaxValue = Float.MIN_VALUE;
        mChildMinValue = Float.MAX_VALUE;
        mStartIndex = indexOfTranslateX(xToTranslateX(0));

        //  note  畫圖區塊寬度
        mStopIndex = indexOfTranslateX(xToTranslateX(mWidth));


        for (int i = mStartIndex; i <= mStopIndex; i++) {
            IKLine point = (IKLine) getItem(i);
            if (mMainDraw != null) {
                mMainMaxValue = Math.max(mMainMaxValue, mMainDraw.getMaxValue(point));
                mMainMinValue = Math.min(mMainMinValue, mMainDraw.getMinValue(point));
            }
            if (mChildDraw != null) {
                mChildMaxValue = Math.max(mChildMaxValue, mChildDraw.getMaxValue(point));
                mChildMinValue = Math.min(mChildMinValue, mChildDraw.getMinValue(point));
            }
        }
        if (mMainMaxValue != mMainMinValue) {
            float padding = (mMainMaxValue - mMainMinValue) * 0.05f;
            mMainMaxValue += padding;
            mMainMinValue -= padding;
        } else {
            //当最大值和最小值都相等的时候 分别增大最大值和 减小最小值
            mMainMaxValue += Math.abs(mMainMaxValue * 0.05f);
            mMainMinValue -= Math.abs(mMainMinValue * 0.05f);
            if (mMainMaxValue == 0) {
                mMainMaxValue = 1;
            }
        }

        if (mChildMaxValue == mChildMinValue) {
            //当最大值和最小值都相等的时候 分别增大最大值和 减小最小值
            mChildMaxValue += Math.abs(mChildMaxValue * 0.05f);
            mChildMinValue -= Math.abs(mChildMinValue * 0.05f);
            if (mChildMaxValue == 0) {
                mChildMaxValue = 1;
            }
        }

        mMainScaleY = mMainRect.height() * 1f / (mMainMaxValue - mMainMinValue);
        mChildScaleY = mChildRect.height() * 1f / (mChildMaxValue - mChildMinValue);

        if (mAnimator.isRunning()) {
            float value = (float) mAnimator.getAnimatedValue();
            mStopIndex = mStartIndex + Math.round(value * (mStopIndex - mStartIndex));
        }
    }

    /**
     * 获取平移的最小值
     *
     * @return
     */
    private float getMinTranslateX() {
        return -mDataLen + mWidth / mScaleX - mPointWidth / 2;
    }

    /**
     * 获取平移的最大值
     *
     * @return
     */
    private float getMaxTranslateX() {
        if (!isFullScreen()) {
            return getMinTranslateX();
        }
        return mPointWidth / 2;
    }

    @Override
    public int getMinScrollX() {
        return (int) -(mOverScrollRange / mScaleX);
    }

    public int getMaxScrollX() {
        return Math.round(getMaxTranslateX() - getMinTranslateX());
    }

    public int indexOfTranslateX(float translateX) {
        return indexOfTranslateX(translateX, 0, mItemCount - 1);
    }

    /**
     * 在主区域画线
     *
     * @param startX     开始点的横坐标
     * @param startValue 开始点的值
     * @param stopX      结束点的横坐标
     * @param stopValue  结束点的值
     */
    public void drawMainLine(Canvas canvas, Paint paint, float startX, float startValue, float stopX, float stopValue) {
        canvas.drawLine(startX, getMainY(startValue), stopX, getMainY(stopValue), paint);
    }

    /**
     * 在主区域画线
     *
     * @param startX    开始点的横坐标
     * @param stopX     开始点的值
     * @param stopX     结束点的横坐标
     * @param stopValue 结束点的值
     */
    public void drawLine(Canvas canvas, float startX, float startValue, float stopX, float stopValue, Paint paint) {
        canvas.drawLine(startX, (float) startValue, stopX, (float) stopValue, paint);
    }

    /**
     * 在子区域画线
     *
     * @param startX     开始点的横坐标
     * @param startValue 开始点的值
     * @param stopX      结束点的横坐标
     * @param stopValue  结束点的值
     */
    public void drawChildLine(Canvas canvas, Paint paint, float startX, float startValue, float stopX, float stopValue) {
        canvas.drawLine(startX, getChildY(startValue), stopX, getChildY(stopValue), paint);
    }

    /**
     * 根据索引获取实体
     *
     * @param position 索引值
     * @return
     */
    public Object getItem(int position) {
        if (mAdapter != null) {
            return mAdapter.getItem(position);
        } else {
            return null;
        }
    }

    /**
     * 根据索引索取x坐标
     *
     * @param position 索引值
     * @return
     */

    public float getX(int position) {
        return position * mPointWidth;
    }

    /**
     * 获取适配器
     *
     * @return
     */
    public IAdapter getAdapter() {
        return mAdapter;
    }

    /**
     * 设置子图的绘制方法
     *
     * @param position
     */
    private void setChildDraw(int position) {
        this.mChildDraw = mChildDraws.get(position);
        mChildDrawPosition = position;
        invalidate();
    }

    /**
     * 给子区域添加画图方法
     *
     * @param name      显示的文字标签
     * @param childDraw IChartDraw
     */
    public void addChildDraw(String name, IChartDraw childDraw) {
        mChildDraws.add(childDraw);
        mKChartTabView.addTab(name);
    }

    /**
     * scrollX 转换为 TranslateX
     *
     * @param scrollX
     */
    private void setTranslateXFromScrollX(int scrollX) {
        mTranslateX = scrollX + getMinTranslateX();
    }

    /**
     * 获取ValueFormatter
     *
     * @return
     */
    public IValueFormatter getValueFormatter() {
        return mValueFormatter;
    }

    /**
     * 设置ValueFormatter
     *
     * @param valueFormatter value格式化器
     */
    public void setValueFormatter(IValueFormatter valueFormatter) {
        this.mValueFormatter = valueFormatter;
    }

    /**
     * 获取DatetimeFormatter
     *
     * @return 时间格式化器
     */
    public IDateTimeFormatter getDateTimeFormatter() {
        return mDateTimeFormatter;
    }

    /**
     * 设置dateTimeFormatter
     *
     * @param dateTimeFormatter 时间格式化器
     */
    public void setDateTimeFormatter(IDateTimeFormatter dateTimeFormatter) {
        mDateTimeFormatter = dateTimeFormatter;
    }

    /**
     * 格式化时间
     *
     * @param date
     */
    public String formatDateTime(Date date) {
        if (getDateTimeFormatter() == null) {
            setDateTimeFormatter(new TimeFormatter());
        }
        return getDateTimeFormatter().format(date);
    }

    /**
     * 获取主区域的 IChartDraw
     *
     * @return IChartDraw
     */
    public IChartDraw getMainDraw() {
        return mMainDraw;
    }

    /**
     * 设置主区域的 IChartDraw
     *
     * @param mainDraw IChartDraw
     */
    public void setMainDraw(IChartDraw mainDraw, boolean isNeedDrawHorizontalLine, List<ObjectTrendLineModel> riseDataList, List<ObjectTrendLineModel> fallDataList) {
        mMainDraw = mainDraw;
        this.isNeedDrawHorizontalLine = isNeedDrawHorizontalLine;
        this.riseDataList = riseDataList;
        this.fallDataList = fallDataList;
    }

    /**
     * 二分查找当前值的index
     *
     * @return
     */
    public int indexOfTranslateX(float translateX, int start, int end) {
        if (end == start) {
            return start;
        }
        if (end - start == 1) {
            float startValue = getX(start);
            float endValue = getX(end);
            return Math.abs(translateX - startValue) < Math.abs(translateX - endValue) ? start : end;
        }
        int mid = start + (end - start) / 2;
        float midValue = getX(mid);
        if (translateX < midValue) {
            return indexOfTranslateX(translateX, start, mid);
        } else if (translateX > midValue) {
            return indexOfTranslateX(translateX, mid, end);
        } else {
            return mid;
        }
    }

    /**
     * 设置数据适配器
     */
    public void setAdapter(IAdapter adapter) {
        // 如果已經有了一個adapter,那麼先登出該Adapter對應的觀察者
        if (mAdapter != null && mDataSetObserver != null) {
            mAdapter.unregisterDataSetObserver(mDataSetObserver);
        }
        mAdapter = adapter;
        if (mAdapter != null) {
            mAdapter.registerDataSetObserver(mDataSetObserver);
            // 獲取資料的數量
            mItemCount = mAdapter.getCount();
        } else {
            mItemCount = 0;
        }
        notifyChanged();
    }

    /**
     * 开始动画
     */
    public void startAnimation() {
        if (mAnimator != null) {
            mAnimator.start();
        }
    }

    /**
     * 设置动画时间
     */
    public void setAnimationDuration(long duration) {
        if (mAnimator != null) {
            mAnimator.setDuration(duration);
        }
    }

    /**
     * 设置表格行数
     */
    public void setGridRows(int gridRows) {
        if (gridRows < 1) {
            gridRows = 1;
        }
        mGridRows = gridRows;
    }

    /**
     * 设置表格列数
     */
    public void setGridColumns(int gridColumns) {
        if (gridColumns < 1) {
            gridColumns = 1;
        }
        mGridColumns = gridColumns;
    }

    /**
     * view中的x转化为TranslateX
     *
     * @param x
     * @return
     */
    public float xToTranslateX(float x) {
        return -mTranslateX + x / mScaleX;
    }

    /**
     * translateX转化为view中的x
     *
     * @param translateX
     * @return
     */
    public float translateXtoX(float translateX) {
        return (translateX + mTranslateX) * mScaleX;
    }

    /**
     * 获取上方padding
     */
    public float getTopPadding() {
        return mTopPadding;
    }

    /**
     * 获取图的宽度
     *
     * @return
     */
    public int getChartWidth() {
        return mWidth;
    }

    /**
     * 是否长按
     */
    public boolean isLongPress() {
        return isLongPress;
    }

    /**
     * 获取选择索引
     */
    public int getSelectedIndex() {
        return mSelectedIndex;
    }

    public Rect getChildRect() {
        return mChildRect;
    }

    /**
     * 设置选择监听
     */
    public void setOnSelectedChangedListener(OnSelectedChangedListener l) {
        this.mOnSelectedChangedListener = l;
    }

    public void onSelectedChanged(BaseKChartView view, Object point, int index) {
        if (this.mOnSelectedChangedListener != null) {
            mOnSelectedChangedListener.onSelectedChanged(view, point, index);
        }
    }

    /**
     * 数据是否充满屏幕
     *
     * @return
     */
    public boolean isFullScreen() {
        return mDataLen >= mWidth / mScaleX;
    }

    /**
     * 设置超出右方后可滑动的范围
     */
    public void setOverScrollRange(float overScrollRange) {
        if (overScrollRange < 0) {
            overScrollRange = 0;
        }
        mOverScrollRange = overScrollRange;
    }

    /**
     * 设置上方padding
     *
     * @param topPadding
     */
    public void setTopPadding(int topPadding) {
        mTopPadding = topPadding;
    }

    /**
     * 设置下方padding
     *
     * @param bottomPadding
     */
    public void setBottomPadding(int bottomPadding) {
        mBottomPadding = bottomPadding;
    }

    /**
     * 设置表格线宽度
     */
    public void setGridLineWidth(float width) {
        mGridPaint.setStrokeWidth(width);
    }

    /**
     * 设置表格线颜色
     */
    public void setGridLineColor(int color) {
        mGridPaint.setColor(color);
    }

    /**
     * 设置选择线宽度
     */
    public void setSelectedLineWidth(float width) {
        mSelectedLinePaint.setStrokeWidth(width);
    }

    /**
     * 设置表格线颜色
     */
    public void setSelectedLineColor(int color) {
        mSelectedLinePaint.setColor(color);
    }

    /**
     * 设置文字颜色
     */
    public void setTextColor(int color) {
        mTextPaint.setColor(color);
    }

    /**
     * 设置文字大小
     */
    public void setTextSize(float textSize) {
        mTextPaint.setTextSize(textSize);
    }

    /**
     * 设置背景颜色
     */
    public void setBackgroundColor(int color) {
        mBackgroundPaint.setColor(color);
    }


    /**
     * 选中点变化时的监听
     */
    public interface OnSelectedChangedListener {
        /**
         * 当选点中变化时
         *
         * @param view  当前view
         * @param point 选中的点
         * @param index 选中点的索引
         */
        void onSelectedChanged(BaseKChartView view, Object point, int index);
    }

    /**
     * 获取文字大小
     */
    public float getTextSize() {
        return mTextPaint.getTextSize();
    }

    /**
     * 获取曲线宽度
     */
    public float getLineWidth() {
        return mLineWidth;
    }

    /**
     * 设置曲线的宽度
     */
    public void setLineWidth(float lineWidth) {
        mLineWidth = lineWidth;
    }

    /**
     * 设置每个点的宽度
     */
    public void setPointWidth(float pointWidth) {
        mPointWidth = pointWidth;
    }

    public Paint getGridPaint() {
        return mGridPaint;
    }

    public Paint getTextPaint() {
        return mTextPaint;
    }

    public Paint getBackgroundPaint() {
        return mBackgroundPaint;
    }

    public Paint getSelectedLinePaint() {
        return mSelectedLinePaint;
    }


    /**
     * ----------------------------------------------------    額外寫的功能     --------------------------------------------------------------
     */

    public void setIsSmartGraph(boolean isSmartGraph) {
        this.isSmartGraph = isSmartGraph;
    }

    public void setIsKChart(boolean isKChart, List<TrendlineData> riseTrendLine, List<TrendlineData> fallTrendLine) {
        this.isKChart = isKChart;
        this.UpTrendLineDataList = riseTrendLine;
        this.DownTrendLineDataList = fallTrendLine;
    }

    //  在智慧圖形畫斜線
    public void setDownSlashLine(ApiObjectSmartAnalysisTrendLine0 objectSmartAnalysisTrendLine, ApiObjectStockHistoricalData objectStockHistoricalData) {
        this.objectSmartAnalysisDownTrendLine = objectSmartAnalysisTrendLine;
        this.klineData = objectStockHistoricalData;
    }

    //  在智慧圖形畫斜線
    public void setSmartGrapghSlashLineData(ApiObjectSmartAnalysisTrendLine objectAllTrendLine, ApiObjectStockHistoricalData objectStockHistoricalData) {
        this.objectAllTrendLine = objectAllTrendLine;
        this.klineData = objectStockHistoricalData;
    }

    //  在智慧圖形畫斜線
    public void setUpSlashLine(ApiObjectSmartAnalysisTrendLine0 objectSmartAnalysisTrendLine, ApiObjectStockHistoricalData objectStockHistoricalData) {
        this.objectSmartAnalysisUpTrendLine = objectSmartAnalysisTrendLine;
        this.klineData = objectStockHistoricalData;
    }

    public void drawDownSlashLine(Canvas canvas, ApiObjectSmartAnalysisTrendLine0 objectSmartAnalysisTrendLine, ApiObjectStockHistoricalData klineData) {
        //保存之前的平移，缩放
        canvas.translate(mTranslateX * mScaleX, 0);
        canvas.scale(mScaleX, 1);

        if (mMainDraw != null) {
            if (objectSmartAnalysisTrendLine != null) {
                // new MainDraw(this).drawDownSlashLine(mStartIndex, mStopIndex, getX(mStartIndex), getX(mStopIndex), canvas, this, objectSmartAnalysisTrendLine, klineData);
            }
        }
    }

    /**
     * mStartIndex 第 K data List 中. 在螢幕最左邊是資料集中第幾根 K
     * mStopIndex 第 K data List 中. 在螢幕最右邊是資料集中第幾根 K. 通常等於 KNUM-1
     */
    public void drawSmartGraphSlashLine(Canvas canvas, ApiObjectSmartAnalysisTrendLine objectAllTrendLine, ApiObjectStockHistoricalData klineData) {
        //保存之前的平移，缩放
        canvas.translate(mTranslateX * mScaleX, 0);
        canvas.scale(mScaleX, 1);
        if (mMainDraw != null) {
            if (objectAllTrendLine != null) {
                new MainDraw(this).drawSmartGraphSlashLine2(mStartIndex, mStopIndex, getX(mStartIndex), getX(mStopIndex), canvas, this, objectAllTrendLine, klineData);
            }
        }
    }

//    public void drawUpSlashLine(Canvas canvas, ApiObjectSmartAnalysisTrendLine0 objectSmartAnalysisTrendLine, ApiObjectStockHistoricalData klineData) {
//        //保存之前的平移，缩放
//        canvas.translate(0, 0);
//        if (mMainDraw != null) {
//            if (objectSmartAnalysisTrendLine != null) {
//                new MainDraw(this).drawUpSlashLine(mStartIndex, mStopIndex, getX(mStartIndex), getX(mStopIndex), canvas, this, objectSmartAnalysisTrendLine, klineData);
//            }
//        }
//    }

    public void drawDFJ_AvgLine(Canvas canvas) {
        //保存之前的平移，缩放
        if (mMainDraw != null) {
            for (int i = mStartIndex; i <= mStopIndex; i++) {
                Object currentPoint = getItem(i);
                float currentPointX = getX(i);
                Object lastPoint = i == 0 ? currentPoint : getItem(i - 1);
                float lastX = i == 0 ? currentPointX : getX(i - 1);
                if (mMainDraw != null) {
                    if (isKChart) {
                        canvas.clipRect(0, BaseKChartView.mainDrawMATextTopHeight, 1000000, BaseKChartView.mainDrawMATextBottomHeight);
                        //  將視窗內每個 K棒的  dfj  上升、下降收盤價平均值連起來
                        new MainDraw(this).drawDfjRiseAverageLine((ICandle) lastPoint, (ICandle) currentPoint, lastX, currentPointX, canvas, this, i);
                        new MainDraw(this).drawDfjFallAverageLine((ICandle) lastPoint, (ICandle) currentPoint, lastX, currentPointX, canvas, this, i);
                    }
                    mMainDraw.drawTranslated(lastPoint, currentPoint, lastX, currentPointX, canvas, this, i, isSmartGraph);
                }
            }
        }
    }

    /**
     * 台指期畫水平線
     * TrendLine Data 畫線
     *
     * @param canvas 主畫布
     */
    private void drawHorizontalLine(Canvas canvas) {
        //保存之前的平移，缩放
        canvas.save();
        canvas.translate(mTranslateX * mScaleX, 0);
        canvas.scale(mScaleX, 1);
        canvas.clipRect(0, BaseKChartView.mainDrawMATextTopHeight, 1000000, BaseKChartView.mainDrawMATextBottomHeight);

        float endIndexX = getX(TaiwanIndexFragment.objectStockHistoricalData.getData().size());

        if (TaiwanIndexFragment.is1MK) {
            // 1分1K圖
            for (int i = 0; i < riseDataList.size(); i++) {
                int positionInArray = riseDataList.get(i).getTrendLinePointIndex() - TaiwanIndexFragment.objectStockHistoricalData.getData().get(0).getIndex();
                new MainDraw(this).drawHorizonRiseLine(getX(positionInArray), endIndexX, canvas, this, riseDataList.get(i).getTrendLinePointPrice());
            }
            for (int i = 0; i < fallDataList.size(); i++) {
                int positionInArray = fallDataList.get(i).getTrendLinePointIndex() - TaiwanIndexFragment.objectStockHistoricalData.getData().get(0).getIndex();
                new MainDraw(this).drawHorizonFallLine(getX(positionInArray), endIndexX, canvas, this, fallDataList.get(i).getTrendLinePointPrice());
            }
        } else {
            // 短線(日K)圖
            for (int i = 0; i < riseDataList.size(); i++) {
                int positionInArray = riseDataList.get(i).getTrendLinePointIndex() - TaiwanIndexFragment.objectStockHistoricalData.getData().get(0).getIndex();
                new MainDraw(this).drawHorizonRiseLine(getX(positionInArray), endIndexX, canvas, this, riseDataList.get(i).getTrendLinePointPrice());
            }
            for (int i = 0; i < fallDataList.size(); i++) {
                int positionInArray = fallDataList.get(i).getTrendLinePointIndex() - TaiwanIndexFragment.objectStockHistoricalData.getData().get(0).getIndex();
                new MainDraw(this).drawHorizonFallLine(getX(positionInArray), endIndexX, canvas, this, fallDataList.get(i).getTrendLinePointPrice());
            }
        }
        //还原 平移缩放
        canvas.restore();
    }

    /**
     * API19 上升下降趨勢線
     */
    private void drawTrendLine(Canvas canvas) {
        //保存之前的平移，缩放
        canvas.save();
        canvas.translate(mTranslateX * mScaleX, 0);
        canvas.scale(mScaleX, 1);
        canvas.clipRect(0, BaseKChartView.mainDrawMATextTopHeight, 1000000, BaseKChartView.mainDrawMATextBottomHeight);
        // 畫DFJ 均線
        drawDFJ_AvgLine(canvas);

        // 畫上升趨勢線
        if (UpTrendLineDataList != null) {
            // 第一個 For  跑多少個   Level
            for (int i = 0; i < UpTrendLineDataList.size(); i++) {
                // 第二個 For  跑該 Level 有多少個小段趨勢線
                for (int j = 0; j < UpTrendLineDataList.get(i).getItems().size(); j++) {
                    // 找每個小段的起始 k 棒的index 在陣列存的位子
                    float startX = 0;
                    float startY = 0;

                    for (int k = 0; k < StockDetailKChartFragment.objectStockHistoricalData.getData().size(); k++) {
                        if (StockDetailKChartFragment.objectStockHistoricalData.getData().get(k).getIndex() == UpTrendLineDataList.get(i).getItems().get(j).get(0).getIndex()) {
                            startX = getX(k);
                            startY = (float) UpTrendLineDataList.get(i).getItems().get(j).get(0).getPrice();
                            break;
                        }
                    }

                    // 找每個小段的結束 k 棒的index 在陣列存的位子
                    float endX = 0;
                    float endY = 0;
                    for (int l = StockDetailKChartFragment.objectStockHistoricalData.getData().size() - 1; l > 0; l--) {
                        if (StockDetailKChartFragment.objectStockHistoricalData.getData().get(l).getIndex() == UpTrendLineDataList.get(i).getItems().get(j).get(1).getIndex()) {
                            endX = getX(l);
                            endY = (float) UpTrendLineDataList.get(i).getItems().get(j).get(1).getPrice();
                            break;
                        }
                    }

                    new MainDraw(this).drawKChartRiseLine(canvas, this, startX, startY, endX, endY);
                }
            }
        }

        // 畫下降趨勢線
        if (DownTrendLineDataList != null) {
            // 第一個 For  跑多少個   Level
            for (int i = 0; i < DownTrendLineDataList.size(); i++) {
                // 第二個 For  跑該Level 有多少個小段趨勢線
                for (int j = 0; j < DownTrendLineDataList.get(i).getItems().size(); j++) {
                    // 找每個小段的起始 k 棒的index 在陣列存的位子
                    float startX = 0;
                    float startY = 0;

                    for (int k = 0; k < StockDetailKChartFragment.objectStockHistoricalData.getData().size(); k++) {
                        if (StockDetailKChartFragment.objectStockHistoricalData.getData().get(k).getIndex() == DownTrendLineDataList.get(i).getItems().get(j).get(0).getIndex()) {
                            startX = getX(k);
                            startY = (float) DownTrendLineDataList.get(i).getItems().get(j).get(0).getPrice();
                            break;
                        }
                    }
                    // 找每個小段的結束 k 棒的index 在陣列存的位子
                    float endX = 0;
                    float endY = 0;
                    for (int l = StockDetailKChartFragment.objectStockHistoricalData.getData().size() - 1; l > 0; l--) {
                        if (StockDetailKChartFragment.objectStockHistoricalData.getData().get(l).getIndex() == DownTrendLineDataList.get(i).getItems().get(j).get(1).getIndex()) {
                            endX = getX(l);
                            endY = (float) DownTrendLineDataList.get(i).getItems().get(j).get(1).getPrice();
                            break;
                        }
                    }

                    new MainDraw(this).drawKChartFallLine(canvas, this, startX, startY, endX, endY);
                }
            }
        }
    }

}
