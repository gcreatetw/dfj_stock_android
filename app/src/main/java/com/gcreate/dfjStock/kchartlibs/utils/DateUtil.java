package com.gcreate.dfjStock.kchartlibs.utils;

import java.text.SimpleDateFormat;

/**
 * 时间工具类
 * Created by tifezh on 2016/4/27.
 */
public class DateUtil {
    public static SimpleDateFormat longTimeFormat = new SimpleDateFormat("MM/dd HH:mm");
    public static SimpleDateFormat shortTimeFormat = new SimpleDateFormat("HH:mm");
    public static SimpleDateFormat DateFormat = new SimpleDateFormat("yyyy/MM/dd");
    public static SimpleDateFormat FullFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:00");
}
