package com.gcreate.dfjStock.kchartlibs.chart.formatter;

import com.gcreate.dfjStock.kchartlibs.chart.base.IDateTimeFormatter;
import com.gcreate.dfjStock.kchartlibs.utils.DateUtil;

import java.util.Date;

/**
 * 时间格式化器
 * Created by tifezh on 2016/6/21.
 */

public class TimeFormatter2 implements IDateTimeFormatter {
    @Override
    public String format(Date date) {
        if (date == null) {
            return "";
        }
        return DateUtil.longTimeFormat.format(date);
    }
}
