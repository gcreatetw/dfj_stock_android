package com.gcreate.dfjStock.kchartlibs.chart;

import android.database.DataSetObservable;
import android.database.DataSetObserver;

import com.gcreate.dfjStock.kchartlibs.chart.base.IAdapter;

/**
 * K 線圖的数据适配器 < 觀察者模式 >
 * Created by tifezh on 2016/6/9.
 */

public abstract class BaseKChartAdapter implements IAdapter {

    private final DataSetObservable mDataSetObservable = new DataSetObservable();


    /**
     * 當資料集用變化時通知所有觀察者
     * Notifies the attached observers that the underlying data has been changed
     * and any View reflecting the data set should refresh itself.
     */
    public void notifyDataSetChanged() {
        if (getCount() > 0) {
            mDataSetObservable.notifyChanged();
        } else {
            mDataSetObservable.notifyInvalidated();
        }
    }


    @Override
    public void registerDataSetObserver(DataSetObserver observer) {
        mDataSetObservable.registerObserver(observer);
    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {
        mDataSetObservable.unregisterObserver(observer);
    }
}
