package com.gcreate.dfjStock.kchartlibs.chart.draw

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.util.Log
import androidx.core.content.ContextCompat
import com.gcreate.dfjStock.R
import com.gcreate.dfjStock.kchartlibs.chart.BaseKChartView
import com.gcreate.dfjStock.kchartlibs.chart.base.IChartDraw
import com.gcreate.dfjStock.kchartlibs.chart.base.IValueFormatter
import com.gcreate.dfjStock.kchartlibs.chart.entity.ICandle
import com.gcreate.dfjStock.kchartlibs.chart.entity.IKLine
import com.gcreate.dfjStock.kchartlibs.chart.formatter.ValueFormatter
import com.gcreate.dfjStock.kchartlibs.utils.ViewUtil
import com.gcreate.dfjStock.webAPI.tab1.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import java.util.*

/**
 * 主图的实现类
 * Created by tifezh on 2016/6/14.
 */
class MainDraw(view: BaseKChartView) : IChartDraw<ICandle?> {
    private var mCandleWidth = 0f
    private var mCandleLineWidth = 0f
    private val mRedPaint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val mGreenPaint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val ma5Paint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val ma10Paint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val ma20Paint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val mSelectorTextPaint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val mSelectorBackgroundPaint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val mContext: Context
    private var mCandleSolid = true
    private val mHorizonLine = Paint(Paint.ANTI_ALIAS_FLAG)

    /**
     * 智慧圖形趨勢線
     */
    fun drawSmartGraphSlashLine2(
        startIndex: Int, stopIndex: Int, startX: Float, stopX: Float, canvas: Canvas,
        view: BaseKChartView, objectSmartAnalysisTrendLine: ApiObjectSmartAnalysisTrendLine,
        objectStockHistoricalData: ApiObjectStockHistoricalData,
    ) {
        val startXindex = objectStockHistoricalData.data[startIndex].index
        val stopXindex = objectStockHistoricalData.data[stopIndex].index


        //設定顯示範圍
        canvas.clipRect(0f, BaseKChartView.mainDrawMATextTopHeight, stopX * 2 - startX, BaseKChartView.mainDrawMATextBottomHeight)
        val min30List: List<Min30> = objectSmartAnalysisTrendLine.data.min30

        runBlocking {

        }

        runBlocking(Dispatchers.IO) {
            if (isShow30minTrendLine) {
                if (min30List.isNotEmpty()) {
                    for (i in min30List.indices) {
                        if (min30List[i].name == "downtrendlines-1-level-1") {
                            for (j in min30List[i].items.indices) {
                                val startY = min30List[i].items[j].a * startXindex + min30List[i].items[j].b
                                val stopY = min30List[i].items[j].a * stopXindex + min30List[i].items[j].b
                                val predictionY = stopY - (startY - stopY)
                                view.drawMainLine(canvas, downSlashLinePaint, startX, startY.toFloat(), stopX * 2 - startX, predictionY.toFloat())
                            }
                        } else if (min30List[i].name == "uptrendlines-1-level-1") {
                            for (j in min30List[i].items.indices) {
                                val startY = min30List[i].items[j].a * startXindex + min30List[i].items[j].b
                                val stopY = min30List[i].items[j].a * stopXindex + min30List[i].items[j].b
                                val predictionY = stopY - (startY - stopY)
                                view.drawMainLine(canvas, upSlashLinePaint, startX, startY.toFloat(), stopX * 2 - startX, predictionY.toFloat())
                            }
                        }
                    }
                }
            }
        }

        runBlocking(Dispatchers.IO) {
            val day1List: List<Day1> = objectSmartAnalysisTrendLine.data.day1
            if (isShow1DTrendLine) {
                if (day1List.isNotEmpty()) {
                    for (i in day1List.indices) {
                        if (day1List[i].name == "downtrendlines-1-level-1") {
                            for (j in day1List[i].items.indices) {
                                val startY = day1List[i].items[j].a * startXindex + day1List[i].items[j].b
                                val stopY = day1List[i].items[j].a * stopXindex + day1List[i].items[j].b
                                val predictionY = stopY - (startY - stopY)
                                view.drawMainLine(canvas, downSlashLinePaint, startX, startY.toFloat(), stopX * 2 - startX, predictionY.toFloat())
                            }
                        } else if (day1List[i].name == "uptrendlines-1-level-1") {
                            for (j in day1List[i].items.indices) {
                                val startY = day1List[i].items[j].a * startXindex + day1List[i].items[j].b
                                val stopY = day1List[i].items[j].a * stopXindex + day1List[i].items[j].b
                                val predictionY = stopY - (startY - stopY)
                                view.drawMainLine(canvas, upSlashLinePaint, startX, startY.toFloat(), stopX * 2 - startX, predictionY.toFloat())
                            }
                        }
                    }
                }
            }
        }
        runBlocking(Dispatchers.IO) {
            val day5List: List<Day5> = objectSmartAnalysisTrendLine.data.day5
            if (isShow5DTrendLine) {
                if (day5List.isNotEmpty()) {
                    for (i in day5List.indices) {
                        if (day5List[i].name == "downtrendlines-1-level-1") {
                            for (j in day5List[i].items.indices) {
                                val startY = day5List[i].items[j].a * startXindex + day5List[i].items[j].b
                                val stopY = day5List[i].items[j].a * stopXindex + day5List[i].items[j].b
                                val predictionY = stopY - (startY - stopY)
                                view.drawMainLine(canvas, downSlashLinePaint, startX, startY.toFloat(), stopX * 2 - startX, predictionY.toFloat())
                            }
                        } else if (day5List[i].name == "uptrendlines-1-level-1") {
                            for (j in day5List[i].items.indices) {
                                val startY = day5List[i].items[j].a * startXindex + day5List[i].items[j].b
                                val stopY = day5List[i].items[j].a * stopXindex + day5List[i].items[j].b
                                val predictionY = stopY - (startY - stopY)
                                view.drawMainLine(canvas, upSlashLinePaint, startX, startY.toFloat(), stopX * 2 - startX, predictionY.toFloat())
                            }
                        }
                    }
                }
            }
        }

        runBlocking(Dispatchers.IO) {
            val day20List: List<Day20> = objectSmartAnalysisTrendLine.data.day20
            if (isShow20DTrendLine) {
                if (day20List.isNotEmpty()) {
                    for (i in day20List.indices) {
                        if (day20List[i].name == "downtrendlines-1-level-1") {
                            for (j in day20List[i].items.indices) {
                                val startY = day20List[i].items[j].a * startXindex + day20List[i].items[j].b
                                val stopY = day20List[i].items[j].a * stopXindex + day20List[i].items[j].b
                                val predictionY = stopY - (startY - stopY)
                                view.drawMainLine(canvas, downSlashLinePaint, startX, startY.toFloat(), stopX * 2 - startX, predictionY.toFloat())
                            }
                        } else if (day20List[i].name == "uptrendlines-1-level-1") {
                            for (j in day20List[i].items.indices) {
                                val startY = day20List[i].items[j].a * startXindex + day20List[i].items[j].b
                                val stopY = day20List[i].items[j].a * stopXindex + day20List[i].items[j].b
                                val predictionY = stopY - (startY - stopY)
                                view.drawMainLine(canvas, upSlashLinePaint, startX, startY.toFloat(), stopX * 2 - startX, predictionY.toFloat())
                            }
                        }
                    }
                }
            }
        }


    }

    /**
     * 量價趨勢線分析
     * DFJ 收盤價均線  - 上升
     */
    fun drawDfjRiseAverageLine(
        lastPoint: ICandle,
        curPoint: ICandle,
        lastX: Float,
        curX: Float,
        canvas: Canvas,
        view: BaseKChartView,
        position: Int,
    ) {
        val p = Paint()
        p.strokeWidth = 2f
        p.color = ContextCompat.getColor(mContext, R.color.YellowFED804)
        canvas.clipRect(0f, BaseKChartView.mainDrawMATextTopHeight, 1000000f, BaseKChartView.mainDrawMATextBottomHeight)
        for (i in lastPoint.dfjRiseAverageDataList.indices) {
            view.drawMainLine(canvas, p, lastX, lastPoint.dfjRiseAverageDataList[i], curX, curPoint.dfjRiseAverageDataList[i])
        }
    }

    /**
     * 量價趨勢線分析
     * DFJ 收盤價均線- 下降
     */
    fun drawDfjFallAverageLine(
        lastPoint: ICandle,
        curPoint: ICandle,
        lastX: Float,
        curX: Float,
        canvas: Canvas,
        view: BaseKChartView,
        position: Int,
    ) {
        val p = Paint()
        p.strokeWidth = 2f
        p.color = ContextCompat.getColor(mContext, R.color.YellowFED804)
        canvas.clipRect(0f, BaseKChartView.mainDrawMATextTopHeight, 1000000f, BaseKChartView.mainDrawMATextBottomHeight)
        for (i in lastPoint.dfjFallAverageDataList.indices) {
            view.drawMainLine(canvas, p, lastX, lastPoint.dfjFallAverageDataList[i], curX, curPoint.dfjFallAverageDataList[i])
        }
    }

    /**
     * 量價趨勢線分析 DFJ趨勢線
     */
    fun drawKChartRiseLine(canvas: Canvas, view: BaseKChartView, startX: Float, startY: Float, endX: Float, endY: Float) {
        // 畫 Rise 線
        view.drawMainLine(canvas, mRedPaint, startX, startY, endX, endY)
    }

    fun drawKChartFallLine(canvas: Canvas, view: BaseKChartView, startX: Float, startY: Float, endX: Float, endY: Float) {
        // 畫 Fall 線
        view.drawMainLine(canvas, mGreenPaint, startX, startY, endX, endY)
    }

    /**
     * 台指期水平線
     */
    fun drawHorizonRiseLine(lastX: Float, curX: Float, canvas: Canvas, view: BaseKChartView, priceValue: Float) {
        // 畫 Rise 水平線
        view.drawMainLine(canvas, mRedPaint, lastX, priceValue, curX, priceValue)
    }

    fun drawHorizonFallLine(lastX: Float, curX: Float, canvas: Canvas, view: BaseKChartView, priceValue: Float) {
        // 畫 Fall 水平線
        view.drawMainLine(canvas, mGreenPaint, lastX, priceValue, curX, priceValue)
    }

    //    @Override
    //    public void drawTranslated(@Nullable ICandle lastPoint, @NonNull ICandle curPoint, float lastX, float curX, @NonNull Canvas canvas, @NonNull BaseKChartView view, int position,Boolean isNeedDrawHorizontalLine) {
    //
    /* 隱藏 MA5 / MA10 /MA20
        //画ma5
        if (lastPoint.getMA5Price() != 0) {
            view.drawMainLine(canvas, ma5Paint, lastX, lastPoint.getMA5Price(), curX, curPoint.getMA5Price());
        }
        //画ma10
        if (lastPoint.getMA10Price() != 0) {
            view.drawMainLine(canvas, ma10Paint, lastX, lastPoint.getMA10Price(), curX, curPoint.getMA10Price());
        }
        //画ma20
        if (lastPoint.getMA20Price() != 0) {
            view.drawMainLine(canvas, ma20Paint, lastX, lastPoint.getMA20Price(), curX, curPoint.getMA20Price());
        }
        */
    //
    //    }
    override fun drawTranslated(
        lastPoint: ICandle?,
        curPoint: ICandle,
        lastX: Float,
        curX: Float,
        canvas: Canvas,
        view: BaseKChartView,
        position: Int,
        isSmartGraph: Boolean,
    ) {
        if (isSmartGraph) {
            drawTaiwanIndexCandle(view, canvas, curX, curPoint.highPrice, curPoint.lowPrice, curPoint.openPrice, curPoint.closePrice)
        } else {
            drawCandle(view, canvas, curX, curPoint.highPrice, curPoint.lowPrice, curPoint.openPrice, curPoint.closePrice)
        }
    }

    override fun drawText(canvas: Canvas, view: BaseKChartView, position: Int, x: Float, y: Float) {
        val point: ICandle = view.getItem(position) as IKLine
        val textMA5 = "MA5:" + view.formatValue(point.mA5Price) + " "
        val textMA10 = "MA10:" + view.formatValue(point.mA10Price) + " "
        val textMA20 = "MA20:" + view.formatValue(point.mA20Price) + " "

        /*   隱藏 MA5 / MA10 /MA20
        if (ma5Paint.getColor() != mContext.getColor(android.R.color.transparent)) {
            canvas.drawText(textMA5, x, y, ma5Paint);
            x += ma5Paint.measureText(textMA5);
        }
        if (ma10Paint.getColor() != mContext.getColor(android.R.color.transparent)) {
            canvas.drawText(textMA10, x, y, ma10Paint);
            x += ma10Paint.measureText(textMA10);
        }
        if (ma20Paint.getColor() != mContext.getColor(android.R.color.transparent)) {
            canvas.drawText(textMA20, x, y, ma20Paint);
        }
        */if (view.isLongPress) {
            drawSelector(view, canvas)
        }
    }

    override fun getMaxValue(point: ICandle?): Float {
        return point!!.highPrice.coerceAtLeast(point.mA20Price)
    }

    override fun getMinValue(point: ICandle?): Float {
        return point!!.mA20Price.coerceAtMost(point.lowPrice)
    }


    override fun drawLevel1TrendLine(
        canvas: Canvas,
        paint: Paint,
        startX: Float,
        startValue: Double,
        stopX: Float,
        stopValue: Double,
        view: BaseKChartView,
    ) {
        canvas.clipRect(0f, BaseKChartView.mainDrawMATextTopHeight, 1000000f, BaseKChartView.mainDrawMATextBottomHeight)
        view.drawLine(canvas, startX, startValue.toFloat(), stopX, stopValue.toFloat(), paint)
    }

    override fun getValueFormatter(): IValueFormatter {
        return ValueFormatter()
    }

    /**
     * 画Candle
     *
     * @param canvas
     * @param x      x轴坐标
     * @param high   最高价
     * @param low    最低价
     * @param open   开盘价
     * @param close  收盘价
     */
    private fun drawCandle(view: BaseKChartView, canvas: Canvas, x: Float, high: Float, low: Float, open: Float, close: Float) {
        var high = high
        var low = low
        var open = open
        var close = close
        high = view.getMainY(high)
        low = view.getMainY(low)
        open = view.getMainY(open)
        close = view.getMainY(close)
        val r = mCandleWidth / 2
        val lineR = mCandleLineWidth / 2
        if (open > close) {
            //实心
            if (mCandleSolid) {
                canvas.drawRect(x - r, close, x + r, open, mRedPaint)
                canvas.drawRect(x - lineR, high, x + lineR, low, mRedPaint)
            } else {
                mRedPaint.strokeWidth = mCandleLineWidth
                canvas.drawLine(x, high, x, close, mRedPaint)
                canvas.drawLine(x, open, x, low, mRedPaint)
                canvas.drawLine(x - r + lineR, open, x - r + lineR, close, mRedPaint)
                canvas.drawLine(x + r - lineR, open, x + r - lineR, close, mRedPaint)
                mRedPaint.strokeWidth = mCandleLineWidth * view.scaleX
                canvas.drawLine(x - r, open, x + r, open, mRedPaint)
                canvas.drawLine(x - r, close, x + r, close, mRedPaint)
            }
        } else if (open < close) {
            // 柱
            canvas.drawRect(x - r, open, x + r, close, mGreenPaint)
            /// 線
            canvas.drawRect(x - lineR, high, x + lineR, low, mGreenPaint)
        } else {
            canvas.drawRect(x - r, open, x + r, close + 1, mRedPaint)
            canvas.drawRect(x - lineR, high, x + lineR, low, mRedPaint)
        }
    }

    /**
     * 智慧圖形要限定畫面範圍
     */
    private fun drawTaiwanIndexCandle(view: BaseKChartView, canvas: Canvas, x: Float, high: Float, low: Float, open: Float, close: Float) {
        var high = high
        var low = low
        var open = open
        var close = close
        high = view.getMainY(high)
        low = view.getMainY(low)
        open = view.getMainY(open)
        close = view.getMainY(close)
        val r = mCandleWidth / 2
        val lineR = mCandleLineWidth / 2
        if (close < BaseKChartView.mainDrawMATextBottomHeight) {
            if (open > close) {
                //实心
                if (mCandleSolid) {
                    canvas.drawRect(x - r, close, x + r, open, mRedPaint)
                    canvas.drawRect(x - lineR, high, x + lineR, low, mRedPaint)
                } else {
                    mRedPaint.strokeWidth = mCandleLineWidth
                    canvas.drawLine(x, high, x, close, mRedPaint)
                    canvas.drawLine(x, open, x, low, mRedPaint)
                    canvas.drawLine(x - r + lineR, open, x - r + lineR, close, mRedPaint)
                    canvas.drawLine(x + r - lineR, open, x + r - lineR, close, mRedPaint)
                    mRedPaint.strokeWidth = mCandleLineWidth * view.scaleX
                    canvas.drawLine(x - r, open, x + r, open, mRedPaint)
                    canvas.drawLine(x - r, close, x + r, close, mRedPaint)
                }
            } else if (open < close) {
                // 柱
                canvas.drawRect(x - r, open, x + r, close, mGreenPaint)
                /// 線
                canvas.drawRect(x - lineR, high, x + lineR, low, mGreenPaint)
            } else {
                canvas.drawRect(x - r, open, x + r, close + 1, mRedPaint)
                canvas.drawRect(x - lineR, high, x + lineR, low, mRedPaint)
            }
        }
    }

    /**
     * draw选择器
     */
    private fun drawSelector(view: BaseKChartView, canvas: Canvas) {
        val metrics = mSelectorTextPaint.fontMetrics
        val textHeight = metrics.descent - metrics.ascent
        val index = view.selectedIndex
        val padding = ViewUtil.Dp2Px(mContext, 5f).toFloat()
        val margin = ViewUtil.Dp2Px(mContext, 5f).toFloat()
        var width = 0f
        val left: Float
        val top = margin + view.topPadding
        val height = padding * 8 + textHeight * 5
        val point = view.getItem(index) as ICandle
        val strings: MutableList<String> = ArrayList()
        strings.add(view.formatDateTime(view.adapter.getDate(index)))
        strings.add("開:" + point.openPrice)
        strings.add("高:" + point.highPrice)
        strings.add("低:" + point.lowPrice)
        strings.add("收:" + point.closePrice)
        for (s in strings) {
            width = Math.max(width, mSelectorTextPaint.measureText(s))
        }
        width += padding * 2
        val x = view.translateXtoX(view.getX(index))
        left = if (isSmartGraph) {
            if (x > view.chartWidth) {
                margin
            } else {
                view.chartWidth * 2 - width - margin
            }
        } else {
            if (x > view.chartWidth / 2) {
                margin
            } else {
                view.chartWidth - width - margin
            }
        }
        val r = RectF(left, top, left + width, top + height)
        canvas.drawRoundRect(r, padding, padding, mSelectorBackgroundPaint)
        var y = top + padding * 2 + (textHeight - metrics.bottom - metrics.top) / 2
        for (s in strings) {
            canvas.drawText(s, left + padding, y, mSelectorTextPaint)
            y += textHeight + padding
        }
    }

    /**
     * 设置蜡烛宽度
     */
    fun setCandleWidth(candleWidth: Float) {
        mCandleWidth = candleWidth
    }

    /**
     * 设置蜡烛线宽度
     */
    fun setCandleLineWidth(candleLineWidth: Float) {
        mCandleLineWidth = candleLineWidth
    }

    /**
     * 设置ma5颜色
     */
    fun setMa5Color(color: Int) {
        ma5Paint.color = color
    }

    /**
     * 设置ma10颜色
     */
    fun setMa10Color(color: Int) {
        ma10Paint.color = color
    }

    /**
     * 设置ma20颜色
     */
    fun setMa20Color(color: Int) {
        ma20Paint.color = color
    }

    /**
     * 设置选择器文字颜色
     */
    fun setSelectorTextColor(color: Int) {
        mSelectorTextPaint.color = color
    }

    /**
     * 设置选择器文字大小
     */
    fun setSelectorTextSize(textSize: Float) {
        mSelectorTextPaint.textSize = textSize
    }

    /**
     * 设置选择器背景
     */
    fun setSelectorBackgroundColor(color: Int) {
        mSelectorBackgroundPaint.color = color
    }

    /**
     * 设置曲线宽度
     */
    fun setLineWidth(width: Float) {
        ma20Paint.strokeWidth = width
        ma10Paint.strokeWidth = width
        ma5Paint.strokeWidth = width
    }

    /**
     * 设置文字大小
     */
    fun setTextSize(textSize: Float) {
        ma20Paint.textSize = textSize
        ma10Paint.textSize = textSize
        ma5Paint.textSize = textSize
    }

    /**
     * 蜡烛是否实心
     */
    fun setCandleSolid(candleSolid: Boolean) {
        mCandleSolid = candleSolid
    }

    /**
     * 設置下降趨勢線顏色颜色
     */
    fun setDownSlashLineParams(
        color: Int,
        Show30minTrendLine: Boolean,
        Show1DayTrendLine: Boolean,
        Show5DayTrendLine: Boolean,
        Show20DayTrendLine: Boolean,
    ) {
        downSlashLinePaint.color = color
        downSlashLinePaint.strokeWidth = 2f
        isShow30minTrendLine = Show30minTrendLine
        isShow1DTrendLine = Show1DayTrendLine
        isShow5DTrendLine = Show5DayTrendLine
        isShow20DTrendLine = Show20DayTrendLine
    }

    /**
     * 設置上升趨勢線顏色颜色
     */
    fun setUpSlashLineParams(
        color: Int,
        Show30minTrendLine: Boolean,
        Show1DayTrendLine: Boolean,
        Show5DayTrendLine: Boolean,
        Show20DayTrendLine: Boolean,
    ) {
        upSlashLinePaint.color = color
        upSlashLinePaint.strokeWidth = 2f
        isShow30minTrendLine = Show30minTrendLine
        isShow1DTrendLine = Show1DayTrendLine
        isShow5DTrendLine = Show5DayTrendLine
        isShow20DTrendLine = Show20DayTrendLine
    }

    companion object {
        private val downSlashLinePaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
            color = Color.GREEN
        }
        private val upSlashLinePaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
            color = Color.RED
        }
        private var isShow30minTrendLine = false
        private var isShow1DTrendLine = false
        private var isShow5DTrendLine = false
        private var isShow20DTrendLine = false

        @JvmField
        var isSmartGraph = false
    }

    init {
        val context = view.context
        mContext = context
        mRedPaint.color = ContextCompat.getColor(context, R.color.chart_red)
        mGreenPaint.color = ContextCompat.getColor(context, R.color.chart_green)
        mHorizonLine.color = ContextCompat.getColor(context, R.color.YellowFFFF00)
    }


}