package com.gcreate.dfjStock.server

import android.util.Log
import com.gcreate.dfjStock.MainActivity.Companion.sendNotification
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class FirebaseNotificationService : FirebaseMessagingService() {

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        if (remoteMessage.notification != null) {
            sendNotification(this, remoteMessage.notification!!.title, remoteMessage.notification!!.body)
        }
    }

    override fun onNewToken(s: String) {
        super.onNewToken(s)
        Log.i("MyFirebaseService", "token $s")
    }

}